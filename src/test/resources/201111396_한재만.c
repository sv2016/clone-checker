#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct node
{
	struct node* link;
	char word[32];
};

struct index
{
	struct node* first[26];
};

int insert_word(char* word, struct index* header);
int delete_word(char* word, struct index* header);
int search_word(char* word, struct index* header);
void print_word(struct index* header);
void freeList(struct index*  header);
int check_word(char* word, int length);
void init_word(char* word, int length);
int open_dic(struct index* header);
int open_voc(struct index* header);
int store_voc(struct index*  header, char mode[3]);


int main(void)
{
	int length;
	struct index* dic;
	struct index* voc;
	int menu;
	char word[1024]={0};
	int i=0;

	dic=(struct index*)malloc(sizeof(struct index));
	voc=(struct index*)malloc(sizeof(struct index));
	for(i=0;i<26;i++)
	{
		dic->first[i]=NULL;
		voc->first[i]=NULL;
	}

	if(open_dic(dic)==1)
	{
		printf("->sample.txt를 여는데 실패 했습니다.\n");
		return 0;
	}
	else
		printf("->sample.txt를 불러왔습니다.\n");

	if(open_voc(voc)==1)
		printf("->voc.txt가 없습니다.\n");
	else
		printf("->voc.txt를 불러왔습니다.\n");

	printf("기능을 선택하세요\n[1] Search Dic\t[2] Search Voc\t[3] Insert\t[4] Delete\n[5] Reload\t[6] Binary\t[7] Ascii\t[8] Quit\n");
	while(1)
	{		
		printf(": ");
		scanf("%d",&menu);
		if (menu==1)
		{
			printf("사전에서 찾을 단어를 입력하세요: ");
			scanf("%s",word);

			if(check_word(word, strlen(word))==0)
			{
				if(search_word(word,dic)==0)
				{
					printf("->사전에 있습니다.\n");
					
					if(insert_word(word,voc)==0)
						printf("->단어장에 추가 되었습니다.\n");
					else
						printf("->단어장에 이미 존재합니다.\n");
				}
				else
				{
					printf("->사전에 없습니다.\n");
				}
			}
			else
			{
				printf("->잘못된 단어입니다.\n");
			}
		}
		else if (menu==2)
		{
			printf("단어장에서 찾을 단어를 입력하세요: ");
			scanf("%s",word);

			if(check_word(word, strlen(word))==0)
			{
				if(search_word(word,voc)==0)
				{
					printf("->단어장에 있습니다.\n");
				}
				else
				{
					printf("->단어장에 없습니다.\n");
				}
			}
			else
			{
				printf("->잘못된 단어입니다.\n");
			}
		}
		else if(menu==3)
		{
			printf("추가할 단어를 입력하세요: ");
			scanf("%s",word);
			if(check_word(word, strlen(word))==0)
			{
				if(insert_word(word, voc)==0)
				{
					printf("->단어장에 추가되었습니다.\n");
				}
				else
				{
					printf("->중복된 단어입니다.\n");
				}
			}
			else
			{
				printf("->잘못된 단어입니다.\n");
			}
		}
		else if(menu==4)
		{
			printf("삭제할 단어를 입력하세요: ");
			scanf("%s",word);

			if(check_word(word, strlen(word))==0)
			{
				if(delete_word(word,voc)==0)
				{
					printf("->삭제되었습니다.\n");
				}
				else
				{
					printf("->단어장에 없습니다.\n");
				}
			}
			else
			{
				printf("->잘못된 단어입니다.\n");
			}
		}
		else if(menu==5)
		{
			freeList(voc);
			freeList(dic);
			dic=(struct index*)malloc(sizeof(struct index));
			voc=(struct index*)malloc(sizeof(struct index));
			for(i=0;i<26;i++)
			{
				dic->first[i]=NULL;
				voc->first[i]=NULL;
			}
			if(open_dic(dic)==1)
			{
				printf("->sample.txt를 여는데 실패 했습니다.\n");
				return 0;
			}
			else
				printf("->sample.txt를 불러왔습니다.\n");

			if(open_voc(voc)==1)
				printf("->voc.txt가 없습니다.\n");
			else
				printf("->voc.txt를 불러왔습니다.\n");
		}
		else if(menu==6)
		{
			if(store_voc(voc,"wb")==0)
				printf("->Binary로 저장 되었습니다.\n");
		}
		else if(menu==7)
		{
			if(store_voc(voc,"wt")==0)
				printf("->Ascii로 저장 되었습니다.\n");
		}
		else if(menu==8)
		{
			break;
		}
		else if(menu==2011)
		{
			printf("===============dic.txt===============\n");
			print_word(dic);
			printf("===============voc.txt===============\n");
			print_word(voc);
		}
		else
		{
			printf("->잘못된 입력입니다.\n");
		}
		getchar();
	}
	freeList(voc);
	freeList(dic);
	return 0;
}

int insert_word(char* word, struct index* header)
{
	if(search_word(word,header)==1)
	{
		int word_order;
		struct node* nodeNew;

		if(isupper(word[0]))
			word_order=word[0]-65;
		else
			word_order=word[0]-97;

		nodeNew=(struct node*)malloc(sizeof(struct node));
		strcpy(nodeNew->word,word);
		nodeNew->link=header->first[word_order];
		header->first[word_order]=nodeNew;
		return 0;
	}
	return 1;
}

void print_word(struct index* header)
{
	int i=0;
	int line=0;
	struct node* current;

	for(i=0;i<26;i++)
	{
		line=0;
		printf("[%c]\n",i+65);
		current=header->first[i];
		while(current!=NULL)
		{
			printf("<%s> ",current->word);
			current=current->link;
			line++;
			if(line==10)
			{	
				printf("\n");
				line=0;
			}
		}
		if(line!=0)
			printf("\n");
	}
}

int delete_word(char* word, struct index* header)
{
	int word_order;
	struct node* current=NULL;
	struct node* prev=NULL;

	if(isupper(word[0]))
		word_order=word[0]-65;
	else
		word_order=word[0]-97;

	current=header->first[word_order];
	prev=NULL;
	while(current!=NULL)
	{
		if(strcmp(word,current->word)==0)
		{
			if(prev==NULL)
			{
				header->first[word_order]=current->link;
				free(current);
			}
			else if(prev!=NULL)
			{	
				prev->link=current->link;
				free(current);
			}
			return 0;
		}
		prev=current;
		current=current->link;	
	}
	return 1;
}

int search_word(char* word, struct index* header)
{
	int word_order=0;
	struct node* current;

	if(isupper(word[0]))
		word_order=word[0]-65;
	else
		word_order=word[0]-97;

	current=header->first[word_order];

	while(current!=NULL)
	{
		if(strcmp(word,current->word)==0)
		{
			return 0;
		}
		else
		{
			current=current->link;
		}
	}
	return 1;	
}

void freeList(struct index* header)
{
	struct node* current;
	struct node* next;
	int i = 0;

	for(i=0;i<26;i++)
	{
		current=header->first[i];
		while(current!=NULL)
		{
			next=current->link;
			free(current);
			current=next;
		}
	}
	free(header);
}

int check_word(char* word, int length)
{
	int i = 0;
	char target[1024];
	strcpy(target,word);

	if
	(!isalpha(target[0]) || !isalpha(target[length-1]))
		return 1;

	for(i=0;i<length;i++)
	{
		if(target[i]!='-' && target[i]!=39 && !isalpha(target[i])) // ascii:39 = '
			return 1;
	}
	
	for(i=0;i<length-1;i++)
	{
		if(!isalpha(target[i]) && !isalpha(target[i+1]))
		{
			return 1;
		}
	}

	return 0;
}

void init_word(char* word, int length)
{
	int i;
	for(i=0; i<length; i++)
	{
		word[i]='\0';
	}
}

int open_dic(struct index* header)
{
	char word[1024]={0};
	char buffer[1024]={0};
	char* token;
	int i=0;
	int j=0;
	int length=0;
	FILE *file;

	file=fopen("sample.txt", "r");

	if(file==NULL)
	{
		return 1;   
	}

	while(fgets(buffer, 1024, file)!=NULL)
	{
		length=strlen(buffer);


		for(i=0;i<length-1;i++)
		{
			if( (!isdigit(buffer[i]) && !isalpha(buffer[i]) && buffer[i]!=' ') && (!isdigit(buffer[i+1]) && !isalpha(buffer[i+1]) && buffer[i+1]!=' ') )
				buffer[i+1]=' ';
		}

		token=strtok(buffer, " ");

		if(!isalpha(buffer[0]))
			token=strtok(NULL, " ");//장:절 처리

		while(token!=NULL)
		{
			strcpy(word,token);
			
			length=strlen(word);
			i=0;
			j=length-1;
			
			while(i<length && !isalpha(word[i]) && !isdigit(word[i]))
			{
				i++;
			}
			
			while(j>=0 && !isalpha(word[j]) && !isdigit(word[j]))
			{
				word[j]='\0';
				j--;
			}

			if(i<length)
			{
				insert_word(word+i, header);
			}
			
			token=strtok(NULL, " ");
			init_word(word,1024);
		}
	}
	fclose(file);
	return 0;
}

int open_voc(struct index* header)
{
	int i=0;
	int length=0;
	char word[1024]={0};
	FILE *file;

	file=fopen("voc.txt", "rb");

	if(file==NULL)
	{
		return 1;
	}

	while(fgets(word, 1024, file)!=NULL)
	{
		length=strlen(word);

		i=1;
		while(!isalpha(word[length-i]) && !isdigit(word[length-i]))
		{
			word[length-i]='\0';//개행문자 처리.
			i++;
		}

		insert_word(word,header);
		init_word(word,1024);
	}

	fclose(file);
	return 0;
}

int store_voc(struct index* header, char mode[3])
{
	FILE *file;	
	struct node* current;
	int i;

	file=fopen("voc.txt", mode);

	for(i=0;i<26;i++)
	{
		current=header->first[i];

		while(current!=NULL)
		{
			fputs(current->word,file);
			fputs("\n",file);
			current=current->link;
		}
	}

	fclose(file);
	return 0;
}