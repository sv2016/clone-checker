#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<string.h>

typedef struct Node
{
	char *data;
	struct Node *next;
	struct Node *prev;
}Node;

Node *NewNode(char *data)
{
	Node *now = (Node *)malloc(sizeof(Node));
	now->data = data;
	now->prev = now->next = NULL;
	return now;
}

typedef struct List
{
	Node *head;
	Node *tail;
	int count;
}List;

void InitList(List *list);
void AddData(List *list, char *data);
void Remove(List *list, char *data);
int Find(List *list, char *data);

int main(void)
{
        FILE *fp = NULL;
	FILE *Afp = NULL;
	FILE *Bfp = NULL;
	size_t i, size, count;
        char buffer[256];
        char seps[] =" ,.:\'\"1234567890?";
        char *word;
        int menu = 0;
        char searchword[256];

        List list[26];
        for(int i = 0; i < 26; i++)
                InitList(&list[i]);

        fp = fopen("40-Matthew.txt", "r");
        while(fgets(buffer, 256, fp))
        {
                word = strtok(buffer, seps);
                fpurge(stdin);
                while(word != NULL)
                {
                        for(int j = 0; j < 27; j++)
                        {
                                if(isupper(word[0]))
                                {
                                        if(tolower(word[0]) == j+97)
                                        {
                                                AddData(&(list[j]), word);
                                        }
                                }
                                else
                                {
                                        if(word[0] == j+97)
                                        {
                                                AddData(&(list[j]), word);
                                        }
                                }
                        }
                        word = strtok(NULL, seps);
                }
        }

	while(menu != 6)
	{
		printf("기능을 선택하세요\n[1] Search [2] Insert [3] Delete\n[4] Store in ASCII [5] Store int binary [6] Quit\n");
		scanf("%d", &menu);

		switch(menu)
		{
		case 1:
		{
                        printf("찾을 단어를 입력하세요 : ");
                        scanf("%s", searchword);
                        if(Find(&(list[searchword[0]-97]), searchword) == 1)
                        {
                                printf("단어장에 있습니다.\n");
                        }
                        else
                                printf("단어장에 없습니다.\n");
			break;
		}
		case 2:
		{
                        printf("추가할 단어를 입력하세요 : ");
                        scanf("%s", searchword);
                        if(list[toascii(searchword[0])-97].count < 5)
                                AddData(&(list[searchword[0]-97]), searchword);
                        else
                                AddData(&(list[26]), searchword);
			break;
		}
		case 3:
		{
                        printf("삭제할 단어를 입력하세요 : ");
                        scanf("%s", searchword);
                        if(Find(&(list[searchword[0]-97]), searchword) == 1)
                        {
                                Remove(&(list[searchword[0]-97]), searchword);
                                printf("삭제되었습니다.\n"); 
                        }
                        else if(Find(&(list[26]), searchword) == 1)
                        {
                                Remove(&(list[26]), searchword);
                                printf("삭제되었습니다.\n");
                        }
                        else
                                printf("단어장에 없습니다.\n");
			break;
		}
		case 4:
		{
			Afp = fopen("Dict(A).txt", "w");
			for(int i = 0; i < 26; i++)
				fscanf(Afp, "%s", list[i]);
			printf("ASCII로 저장에 성공하였습니다.\n");
			break;
		}
		case 5:
		{
			Bfp = fopen("Dict(B).txt", "w");
			for(int i = 0; i < 26; i++)
			{
				size = sizeof(list[i]);
				count = sizeof(list[i])/sizeof(list[i]);
				i = fwrite(&list[i], size, count, Bfp);
			}
			printf("binary로 저장에 성공하였습니다.\n");
			break;
		}
		case 6:
			break;
		}
	}

	return 0;
}

void InitList(List *list)
{
	list->head = NewNode(0);
	list->tail = NewNode(0);
	list->head->next = list->tail;
	list->tail->prev = list->head;
	list->count = 0;
}

void AddData(List *list, char *data)
{
	Node *now = NewNode(data);
	now->prev = list->tail->prev;
	now->next = list->tail;
	list->tail->prev->next = now;
	list->tail->prev = now;
	list->count++;
}

void Remove(List *list, char *data)
{
	Node *now = list->head->next;
	while(now != list->tail)
	{
		if(strcmp(now->data, data))
		{
			now->prev->next = now->next;
			now->next->prev = now->prev;
			free(now);
			list->count--;
			break;
		}
		now = now->next;
	}
}

int Find(List *list, char *data)
{
	Node *seek = list->head->next;
	while(seek != list->tail)
	{
		if(strcmp(seek->data, data) == 0)
			return 1;
		seek = seek->next;
	}
	return 0;
}
