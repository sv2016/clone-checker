#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

struct Node
{
	char* word;
	struct Node* next;
	struct Node* prev;
};

int ii = 0; int jj = 0; int kk = 0; int mm = 1; // find의 변수
int z1, z2, z3, z5; // z2: contain 함수는 파일을 불러들일 때와 단어장에 단어를 추가할 때 둘다 쓰이는데, 파일을 불러들일 때는 이미 있는 단어라고 알릴 필요가 없으므로, 이때를 구분하기 위한 것이 z2 변수 
		
struct Node** contain(struct Node** set, int* i, int* j, int* k, int* m, char* p); 
struct Node** file_contain(FILE* fp, struct Node** set, int* i, int* j, int* k, int* m, int check);
char* preprocessing(char* word);
int redundancy(char* p, struct Node* set);
struct Node* InitList(char* word);
int voc_search(struct Node* set, char* word);
int search(struct Node* set, struct Node** find, char* word);
struct Node* insert(struct Node* old, char* newword);
struct Node* delete(struct Node* set, char* word, int* z3);
void print(struct Node* set, FILE* fp, int z4); // z2: contain 함수를 파일을 불러들일 때와 단어장에 단어를 추가할 때 둘다 쓰이는데, 파일을 불러들일 때는 이미 있는 단어라고 알릴 필요가 없으므로, 이때를 구분하기 위한 것이 z2 변수 
void print_(struct Node* set);
void Free(struct Node** a, int m);

int main(void)
{
	char ans[200];
	int a = 0; int b = 1; int c = 1;
	int i = 0; int j = 0; int k = 0; int m = 1; // i: set과 find가 몇 번째 배열까지 만들어졌는지 체크하는 변수 
												// j: for문 돌리는 변수
												// k: 기존 단어장에 해당 알파벳으로 시작하는 것이 있으면, insert 함수를 호출하고, 해당 알파벳으로 시작하는 것이 없으면, Init 함수를 호출하여 새로 만들어야 하는데, 이를 구분하기 위한 변수 
												// m: redundancy 체크하는 변수
	FILE *dic = fopen("sample.txt", "r");
	FILE *voca = fopen("voc.txt", "rb");
	FILE *store; // 단어장 저장

	struct Node** set = (struct Node**)malloc(sizeof(struct Node*));
	struct Node** find = (struct Node**)malloc(sizeof(struct Node*));
	
	if(dic == NULL)
	{
	  printf("no sample file\n");
	}

	if(voca == NULL)
	{
		printf("no voca file\n");
	}

	else
	{
		find = file_contain(voca, find, &ii, &jj, &kk, &mm, 1);
	}


	set = file_contain(dic, set, &i, &j, &k, &m, 0);

	fclose(dic);
	fclose(voca);

	z1 = 0; z2 = 0; z3 = 0; z5 = 0;

	while(b)
	{
		printf("\n=======================\n");
		printf("[1] Search in Dictionary  [2] Search in Vocabulary\n");
		printf("[3] Insert                [4] Delete              \n");
		printf("[5] Store in ASCII        [6] Store in Binary     \n");
		printf("[7] Quit                                          \n");
		printf("기능을 선택하세요: ");
		scanf("%d", &a);
	    printf("\n");

		switch(a)
		{
			case 1:
				printf("사전에서 찾을 단어를 입력하세요: ");
				scanf("%s", ans);

				for(j = 0; j < i; j++)
				{	
			  		if((*(ans) == *(set[j] -> word))) //첫번째알파벳확인
					{
			  			z1 = 1;
						search(set[j], find, ans);
		  	  		}
				}

				if(z1 != 1)
				{
					printf("사전에 없습니다.\n");
				}

				z1 = 0; // 초기화

				break;

			case 2:
				printf("단어장에서 찾을 단어를 입력하세요: ");
				scanf("%s", ans);

				for(jj = 0; jj < ii; jj++)
				{	
			  		if((*(ans) == *(find[jj] -> word))) //첫번째알파벳확인
					{
			  			z1 = 1;
						voc_search(find[jj], ans);
		  	  		}
				}

				if(z1 != 1)
				{
					printf("단어장에 없습니다.\n");
				}

				z1 = 0; //초기화

				break;

			case 3:
				printf("단어장에 추가할 단어를 입력하세요: ");
				scanf("%s", ans);

				z2 = 1;
				find = contain(find, &ii, &jj, &kk, &mm, ans);
				z2 = 0;
			
				break;

			case 4:
				printf("단어장에서 삭제할 단어를 입력하세요: ");
				scanf("%s", ans);

				for(jj = 0; jj < ii; jj++)
				{
			  		if((*ans == *(find[jj] -> word))) // ans의 첫번째 알파벳 확인
					{
						find[jj] = delete(find[jj], ans, &z3);

						if(z5) // delete의 첫번째 조건문을 만족할 경우
						{
							for(jj; jj < ii; jj++) // 단어장에서 알파벳 하나가 사라지면, find[jj]는 사라졌지만, find 변수내에선 빈자리로 남아있어서 이를 조정해줌.
							{
								find[jj] = find[jj+1];
							}

							z5 = 0;
						}
		  			}
				}

				if(z3 != 1)
				{
					printf("단어장에 없는 단어입니다.\n");
				}

				z3 = 0;

				break;

			case 5: ;
				store = fopen("voc_ASCII.txt", "w");

				for(jj = 0; jj < ii; jj++)
				{
					print(find[jj], store, 0);
				}

				fclose(store);

				printf("저장 완료!\n");

				break;

			case 6: ;
				store = fopen("voc.txt", "wb");

				for(jj = 0; jj < ii; jj++)
				{
					print(find[jj], store, 1);
				}

				fclose(store);

				printf("저장 완료!\n");

				break;

			case 7:
				printf("종료\n");
				b = 0;
	  	
				break;

			default:
				printf("잘못된 입력\n");

				break;
		}	

  	}

  	Free(set, i);
  	Free(find, ii);  

	return 0;
}

char* preprocessing(char* word) // 문서를 단어로
{
	int i;
	for(i = 0; i < strlen(word); i++)
	{
		if((int)word[i] == '\'' && (int)word[i+1] == 's')
	    {
			memmove(&word[i], &word[i+1], strlen(word)-i);
			memmove(&word[i], &word[i+1], strlen(word)-i);
		}
			
	  	else if(!((64 < (int)word[i] && (int)word[i] < 91 ) || (96 < (int)word[i] && (int)word[i] < 123)))
		{
	   		memmove(&word[i], &word[i+1], strlen(word)-i);
			preprocessing(word);
		}	
	}

	for(i = 0; word[i]; i++)
	{
	  word[i] = tolower(word[i]);
  	}

	return word;
}

struct Node* InitList(char* word)
{
	struct Node* body = (struct Node*)malloc(sizeof(struct Node));

	body -> prev = NULL;
	body -> next = NULL;
	body -> word= malloc(sizeof(word));
	strcpy(body -> word, word);

	return body;
}

struct Node* insert(struct Node* old, char* newword)
{
	struct Node* new = (struct Node*)malloc(sizeof(struct Node));
  
	old -> next = new;
	new -> prev = old;
	new -> word= malloc(sizeof(newword));
	strcpy(new -> word, newword);
	new -> next = NULL;

	return new;
}

struct Node* delete(struct Node* set, char* word, int* z3)
{
	struct Node* del = (struct Node*)malloc(sizeof(struct Node));
	struct Node* check1 = (struct Node*)malloc(sizeof(struct Node));
	struct Node* check2 = (struct Node*)malloc(sizeof(struct Node));

	del = set;

	while(del != NULL)
	{
		if(strcmp(del -> word, word) == 0)
	    {
			*z3 = 1;

			check1 = del -> prev;
			check2 = del -> next;

			// 단어가 하나밖에 없을 때
			if(check1 == NULL && check2 == NULL) 
			{
				ii--; // 알파벳이 하나 줄어든다
				z5 = 1;
			}

			// 지울 단어가 단어장의 맨 앞에 있을 때 
			else if(check1 == NULL)
			{
				check2 -> prev = check1;
			}

			// 지울 단어가 단어장의 맨 뒤에 있을 때
			else if(check2 == NULL)
			{
				check1 -> next = check2;
			}

			// 지울 단어가 단어장의 가운데에 있을 때 
			else
			{
				check1 -> next = check2;
				check2 -> prev = check1;
			}

			del -> prev = NULL;
			del -> next = NULL;
			del -> word = NULL;

			free(del);

			printf("삭제되었습니다.\n");

			return set;
		}

	    del = del -> prev;
    }
	
	free(del);

	return set;
}

int search(struct Node* set, struct Node** find, char* word)
{
	struct Node* check = (struct Node*)malloc(sizeof(struct Node));

	check = set;

	while(check != NULL)
	{
		if(strcmp(check -> word, word) == 0)
	  	{
	  		find = contain(find, &ii, &jj, &kk, &mm, word);
			printf("사전에 있습니다: %s\n", word);
			
			return 0;
		}
		
	  	check = check -> prev;
  	}

	printf("사전에 없습니다.\n");

	free(check);

	return 0;
}

int voc_search(struct Node* set, char* word)
{
	struct Node* check = (struct Node*)malloc(sizeof(struct Node));

	check = set;

	while(check != NULL)
	{
		if(strcmp(check -> word, word) == 0)
	  	{
			printf("단어장에 있습니다\n");
			
			return 0;
		}
		
	  	check = check -> prev;
  	}

	printf("단어장에 없습니다.\n");

	free(check);

	return 0;
}

void print(struct Node* set, FILE* fp, int z4)
{
	struct Node* start = (struct Node*)malloc(sizeof(struct Node));

	start = set;

	while(start -> prev != NULL)
	{
	  start = start -> prev;
  	}

  	char c = *(start -> word); // 시작 알파벳

  	if(z4 == 0) // ASCII file로 저장할 때
  	{
	  	fprintf(fp, "< %c >\r\n", c);

		while(start != NULL)
		{
			fprintf(fp, "%s ", start -> word);
			start = start -> next;
	  	}

	  	fprintf(fp, "\r\n");
  	}

  	else // binary file로 저장할 때
  	{
		while(start != NULL)
		{
			fwrite(start -> word, 1, strlen(start -> word), fp);
			fwrite(" ", 1, strlen(" "), fp);
			start = start -> next;
	  	}

	  	fwrite("\r\n", 1, strlen("\r\n"), fp);
  	}

	free(start);
}

void print_(struct Node* set)
{
	struct Node* start = (struct Node*)malloc(sizeof(struct Node));

	start = set;

	while(start -> prev != NULL)
	{
	  start = start -> prev;
  	}

	while(start != NULL)
	{
		printf("%s ", start -> word);
		start = start -> next;
  	}

	free(start);
}

int redundancy(char* p, struct Node* set)
{
	struct Node* start = (struct Node*)malloc(sizeof(struct Node));

	start = set;

	while(start -> prev != NULL)
	{
	  start = start -> prev;
  	}

	while(start != NULL)
	{
		if(strcmp(p, start -> word) == 0)
	  	{
			return 0;
		}

		start = start -> next;
  	}

	free(start);

	return 1;
}

struct Node** contain(struct Node** set, int* i, int* j, int* k, int* m, char* p) // 단어장에 파일의 단어 담기
{
	if((*i == 0) && p != NULL) // 맨처음
	{
	  	(*i)++;
	  	set[(*i)-1] = InitList(p);
  	}

	else
	{
		for(*j = 0; *j < *i; (*j)++) // 기존 단어장에 있을 때
		{
			if((*p == *(set[*j] -> word)) && p != NULL)
			{
			  	if(*m = redundancy(p, set[*j]))
				{
					set[*j] = insert(set[*j], p);
					*k = 1;
				}

				break;
			}
		}

		if((*k != 1) && p != NULL && *m != 0) // 기존 단어장에 없을 때 
		{
			(*i)++;
			set = (struct Node**)realloc(set, (*i)*sizeof(struct Node*));
			set[(*i)-1] = InitList(p);
		}
	}

	if(!*m && z2 ==1) // 파일로 contain 함수를 호출할 땐, 이 문장이 프린트 되지 않도록 z2로 구분
	{
		printf("단어장에 있는 단어 입니다.\n");
	}

	*k = 0; // 초기화
	*m = 1;

	return set;
}

struct Node** file_contain(FILE* fp, struct Node** set, int* i, int* j, int* k, int* m, int check)
{
	char* line = NULL; size_t len = 0; char* p = NULL;

	if(check == 0)
	{
		while(getline(&line, &len, fp) != -1)
		{
			for(p = strtok(line, " -"); p != NULL; p = strtok(NULL, " -"))
			{
				p = preprocessing(p);
				set = contain(set, i, j, k, m, p);
		    }
	  	}
	}

	else
	{
		char c;
		char* str;

	  	while(!feof(fp))
	  	{
	  		fread(&c, 1, 1, fp);

	  		if(c == ' ' || c == '\n')
	  		{
	  			set = contain(set, i, j, k, m, str);
	  			strcpy(str, "");
	  		}

	  		else
	  		{
	  			strcat(str, &c);
	  		}
	  	}
	}

  	return set;
}

void Free(struct Node** a, int m) 
{
    int i;
    for (i = 0; i < m; ++i) 
    {
        free(a[i]);
    }

    free(a);
}