//
//  main.c
//  Task3
//
//  Created by 이종빈 on 2016. 5. 18..
//  Copyright © 2016년 이종빈. All rights reserved.
//
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct linkedList{
    int count;
    char str[256];
    
    struct linkedList *next_node;
    struct linkedList *prev_node;
    
} linkedList;


linkedList* search(linkedList s[],char* str);
linkedList* insert(linkedList s[]);
void delete(linkedList s[]);
linkedList* initialize(FILE* fp,linkedList s[]);
void mkDictionary(FILE* fp, linkedList s);


void store_in_ASCII(FILE* fp);


int main()
{
    
    FILE *fp;
    FILE *fp2;
    FILE *fp3=NULL;
    fp = fopen("/Users/Empty_Paper/PP/task2/40-Matthew.txt","r+");
    fp2 = fopen("/Users/Empty_Paper/PP/task2/Dictionary.txt","rb");
    fp3 = fopen("/Users/Empty_Paper/PP/task2/Dictionary.txt","w");
    
    int choice;
    int i=0;
    char str[256];
    linkedList* store;
    linkedList* store_temp;
    linkedList* search_node;
    linkedList* free1=NULL;
    linkedList* free2=NULL;
    linkedList* free3=NULL;
    
    linkedList alpha[27];//0~25까지 a~z 26 = ohters
    
    for(i=0;i<26;i++)
    {
        alpha[i].count = 0;
        strcpy(alpha[i].str,"HEADER");
    }
    free1=initialize(fp,alpha);
    store_temp=&alpha[26];
    if(fp==NULL)
    {
        printf("error\n");
    
            
    }
    
    if(fp2==NULL){
        fp2 = fopen("/Users/Empty_Paper/PP/task2/Dictionary.bin","wb");
        fp3 = fopen("/Users/Empty_Paper/PP/task2/Dictionary.txt","w");
    }
    
    
    while(1){
        store = (linkedList*)malloc(sizeof(linkedList));
        choice =0;
        printf("######기능을 선택하세요#####\n");
        printf("[1] SEARCH [2] INSERT\n");
        printf("[3] DELETE [4] STORE_IN_ASCII\n");
        printf("[5] STORE_IN_BINARY [6] QUIT\n");
        
        printf(": ");
        //show_menu();
        scanf("%d",&choice);
        if(choice == 1){
            printf("찾을 단어를 입력하세요 : ");
            scanf("%s",str);
            search_node=search(alpha,str);
            if(search_node==NULL)
                printf("단어장에 없습니다!\n");
            else{
                strcpy(store->str, search_node->str);
                printf("단어장에 있습니다.\n");
            }
        }
        else if(choice == 2)
            free2=insert(alpha);
        else if(choice == 3)
            delete(alpha);
        else if(choice == 4){
            fp3= fopen("/Users/Empty_Paper/PP/task2/Dictionary.txt","w");
            mkDictionary(fp3, alpha[26]);
        }
        else if(choice == 5){
            fp2= fopen("/Users/Empty_Paper/PP/task2/Dictionary.txt","wb");
            mkDictionary(fp2, alpha[26]);
        }
        else if(choice == 6)
            break;
        
        store_temp->next_node = store;
        store_temp = store_temp->next_node;
    }
    free(free1);
    free(free2);
    free(free3);
    fclose(fp);
    fclose(fp2);
    fclose(fp3);
    
    return 0;
}

linkedList* initialize(FILE* fp,linkedList s[])
{
    char seps[] = ".,\t\r\n\" ";
    char *token;
    int i=0,j=0;
    int count;
    //	char c;
    char buffer[256];
    //	char temp_buffer[256];
    //	char count_buffer[10];
    linkedList* temp=NULL;
    linkedList* temp2;
    while(fgets(buffer,256,fp))
    {
        
        token = strtok(buffer,seps);
        
        while(token != NULL)
        {
            for(i=0;i<26;i++){
                temp2 = &s[i];
                count = s[i].count;
                for(j=0;j<count;j++)
                {
                    temp2=temp2->next_node;
                }
                if(toupper(token[0])==i+65){
                    temp = (linkedList*)malloc(sizeof(linkedList)*1);
                    strcpy(temp->str,token);
                    temp2->next_node = temp;
                    temp->prev_node = temp2;
                    temp->next_node = NULL;
                    //printf("%s %d\n",temp->str,s[i].count);
                    s[i].count++;
                }
            }
            token = strtok(NULL,seps);
        }
    }
    return temp;
    
}

linkedList* search(linkedList s[],char* str)
{
    //  char str[256];
    int i,j;
    int flag=0;
    char* char_flag = NULL;
    linkedList* temp=NULL;
    linkedList* result=NULL;
    
    for(i=0;i<26;i++)
    {
        if(toupper(str[0])==i+65){
            temp=s[i].next_node;
            while(temp!=NULL)
            {
                //printf("%s\n",temp->str);
                char_flag = strstr(temp->str, str);
                if (char_flag!=NULL) {
                    result = temp;
                    //printf("단어장에 있습니다\n");
                    flag++;
                    break;
                }
                temp = temp->next_node;
                
            }
            // if(flag==0)
            //     printf("단어장에 없습니다!\n");
            break;
        }
        /*
        else if(i==26)
        {
            temp = s[i].next_node;
            for(j=0;j<s[i].count;j++)
            {
                char_flag = strstr(temp->str, str);
                if (char_flag!=NULL) {
                    result = temp;
                    //printf("단어장에 있습니다\n");
                    flag++;
                    break;
                }
                temp = temp->next_node;
            }
            //   if(flag==0)
            //     printf("단어장에 없습니다!\n");
            break;
        }
         */
    }
    return result;
}

linkedList* insert(linkedList s[])
{
    char str[256];
    int count,j;
    linkedList* temp =NULL;
    linkedList* temp2;
    
    temp2 = &s[26];
    count = s[26].count;
    for(j=0;j<count;j++)
    {
        temp2=temp2->next_node;
    }
    temp = (linkedList*)malloc(sizeof(linkedList)*1);
    
    
    printf("삽입할 단어를 입력하세요 : ");
    scanf("%s",str);
    strcpy(temp->str,str);
    temp->prev_node = temp2;
    temp->next_node = NULL;
    temp2->next_node = temp;
    s[26].count++;
    return temp;
    
}

void delete(linkedList s[])
{
    char str[256];
    linkedList* temp;
    linkedList* search_node;
    printf("삭제할 단어를 입력하세요 : ");
    scanf("%s",str);
    temp=search(s,str);
    search_node = temp;
    
    if(temp!=NULL){
        (temp->prev_node)->next_node = temp->next_node;
        if(temp->next_node!=NULL)
            (temp->next_node)->prev_node = temp->prev_node;
        while(strcmp(search_node->str,"HEADER")!=0)
        {
            search_node = search_node->prev_node;
        }
        search_node->count--;
    }
    
    else{
        printf("%s 를 찾을 수 없습니다\n",str);
    }
    
}

void mkDictionary(FILE *fp, linkedList s){
    
    linkedList* temp=&s;
    while(temp!=NULL){
        fprintf(fp, "%s ",temp->str);
        temp= temp->next_node;
    }
    
}