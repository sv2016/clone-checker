#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define NUMBER 26

typedef struct _node {
    char value[100];
    struct _node* next;
} node;

typedef node* pnode;

typedef struct _list {
    int count;
    pnode head;
} list;

void init(list* plist);
void insert(list* plist, char* value);
void delect(list* plist, char* value);
int search(list* plist, char* value);
void print_list(list* lptr);
void store_binary(list* plist);

int main(void)
{
    int select, i, j, frist, last, index = 0, check_last;
    char word[1000] = "hi";
    char line[10000];
    char* read_voca = (char*)malloc(sizeof(char));
    
    FILE *fp = fopen("sample.txt", "r");
    
    list* dic[NUMBER];
    list* voca = (list*)malloc(sizeof(list));
    
    for(i=0; i<NUMBER; i++){
        dic[i] = (list*)malloc(sizeof(list));
        init(dic[i]);
    }
    
    init(voca);
    
    while(fgets(line, sizeof(line), fp) != NULL) {
        frist = 0;
        
        for(i=0; i<strlen(line)-1; i++) {
            if(line[i] == ' ') {
                last = i; check_last = i;
                
                for(j=frist; j<last; j++)
                    if(isalpha(line[j])){
                        frist = j;
                        break;
                    }
                
                for(j=last; j>=frist; j--)
                    if(isalpha(line[j])){
                        last = j;
                        break;
                    }
                
                for(j=frist; j<=last; j++)
                    word[index++] = tolower(line[j]);
                
                
                if(isalpha(word[0])) {
                    frist = tolower(word[0])-'a';
                    
                    if(search(dic[frist], word) == 0)
                        insert(dic[frist], word);
                }
                
                memset(word, '\0', strlen(word));
                index = 0; frist = i+1;
                
            }
            
        }
        
        if(check_last+1 != strlen(line)-1) {
            last = strlen(line);
            
            for(j=frist; j<last; j++)
                if(isalpha(line[j])){
                    frist = j;
                    break;
                }
            
            for(j=last; j>=frist; j--)
                if(isalpha(line[j])){
                    last = j;
                    break;
                }
            
            for(j=frist; j<=last; j++)
                word[index++] = tolower(line[j]);
            
            if(isalpha(word[0])) {
                frist = word[0]-'a';
                
                if(search(dic[frist], word) == 0)
                    insert(dic[frist], word);
            }
            
            memset(word, '\0', strlen(word));
            index = 0; frist = i+1;
        }
    }
    
    size_t n_size;
    
    FILE *br = fopen("voc.dat", "rb");
    
    while(0 < (n_size = fread(read_voca, sizeof(read_voca), 1, br))){
        frist = read_voca[0]-'a';
        
        if(search(voca, read_voca) == 0)
            insert(voca, read_voca);
    }
    
    int temp;
    
    while(1) {
        printf("=========Dictionary=========\n");
        printf("  [1] Search \n");
        printf("============================\n\n");
        printf("=========Vocabulary=========\n");
        printf("  [2] Search [3] Insert\n");
        printf("  [4] Delete \n");
        printf("  [5] Store in ASCII \n");
        printf("  [6] Store in Binary \n");
        printf("============================\n");
        printf("  [7] Quit \n");
        printf("============================\n");
        printf(": ");
     
        scanf("%d", &select);
        
        switch(select) {
            case 1:
                printf("사전에서 찾을 단어를 입력하세요: ");
                scanf("%s", word);
     
                for(i=0; i<strlen(word); i++)
                    word[i] = tolower(word[i]);
                
                frist = word[0]-'a';
     
                if(search(dic[frist], word) == 1) {
                    printf("사전에 있습니다.\n");
     
                    if(search(voca, word) == 0){
                        insert(voca, word);
                    }
                }else
                    printf("사전에 없습니다.\n");
     
                break;
            case 2:
                printf("단어장에서 찾을 단어를 입력하세요: ");
                scanf("%s", word);
                
                for(i=0; i<strlen(word); i++)
                    word[i] = tolower(word[i]);
                
                frist = word[0]-'a';
     
                if(search(voca, word) == 1) {
                    printf("단어장에 있습니다.\n");
                }else
                    printf("단어장에 없습니다.\n");
     
                break;
            case 3:
                printf("단어장에 추가할 단어를 입력하세요: ");
                scanf("%s", word);
                
                for(i=0; i<strlen(word); i++)
                    word[i] = tolower(word[i]);
                
                frist = word[0]-'a';
     
                if(search(voca, word) == 1)
                    printf("이미 단어장에 있습니다.\n");
                else
                    insert(voca, word);
                
                break;
            case 4:
                printf("단어장에서 삭제할 단어를 입력하세요: ");
                scanf("%s", word);
                
                for(i=0; i<strlen(word); i++)
                    word[i] = tolower(word[i]);
                
                frist = word[0]-'a';
                
                if(search(voca, word) == 0)
                    printf("단어장에 없습니다.\n" );
                else {
                    if(search(voca, word) == 1)
                        delect(voca, word);
                }
                
                break;
            case 5:
                printf("사랑합니다 조교님\n");
                
                break;
            case 6:
                store_binary(voca);
                
                printf("저장하였습니다.\n");
                
                break;
            case 7:
                return 0;
            case 8:
                printf("출력할 리스트를 입력하세요: ");
                scanf("%s", word);
     
                if(strcmp(word, "-1") == 0) {
                    printf("------%c List in Vocabulary----\n", toupper(word[0]));
                    print_list(voca);
                }else{
                    printf("%c List\n", toupper(word[0]));
                    frist = tolower(word[0])-'a';
     
                    print_list(dic[frist]);
                }
     
                break;
            default:
                printf("잘못된 입력입니다. 다시 입력해 주세요.\n");
     
                break;
        }
    }
    
    return 0;
}

void init(list* plist) {
    plist->count = 0;
    plist->head = NULL;
    
    return;
}

void insert(list* plist, char value[]) {
    
    pnode new_node = (node*)malloc(sizeof(node));
    strcpy(new_node->value, value);
    
    if(plist->count == 0){
        new_node->next = plist->head;
        plist->head = new_node;
    } else {
        pnode temp =  plist->head;
        
        for(int i=1; i<(plist->count); i++)
            temp = temp->next;
        
        new_node->next = temp->next;
        temp->next = new_node;
    }
    plist->count ++;
    
    return;
}

void delect(list* plist, char value[]) {
    
    pnode temp = plist->head;
    
    if(strcmp((temp->value), value) == 0){
        plist->head = temp->next;
        free(temp);
    }else{
        while(strcmp((temp->next->value), value))
            temp = temp->next;
        
        pnode temp2 = temp->next->next;
        temp->next = temp2;
        
        free(temp2);
    }
    plist->count --;
    
    return;
}

int search(list* plist, char value[]) {
    pnode temp = plist->head;
    
    int i=0;
    
    while(temp != NULL) {
        if(strcmp((temp->value), value) == 0)
            break;
        
        i++;
        temp = temp->next;
    }
    
    if(i>=plist->count)
        return 0;
    else
        return 1;
}

void print_list(list* plist) {
    pnode temp = plist->head;
    
    printf("List value: ");
    
    while(temp!=NULL){
        printf("%s ", temp->value);
        temp=temp->next;
    }
    
    printf("\n");
    printf("Total: %d value(s)\n",plist->count);
}

void store_binary(list* plist) {
    FILE *bw = fopen("voc.dat", "wb");
    
    pnode temp = plist->head;
    
    while(temp!=NULL){
        fwrite(temp, sizeof(temp), 1, bw);
        
        temp=temp->next;
    }
    
    fclose(bw);
}
/*
 
*/