#include<stdio.h>
#include<string.h>
#include<ctype.h>

#define BUF_SIZE 10000
#define SIZE 24
#define NODESIZEINDEX 25
//알파벳 갯수가 26개인데 0부터 세야해서 25


typedef struct Node{
	char data[SIZE];
	struct Node *fp;
	//forward pointer
	struct Node *bk;
	//back pointer
}Node;

void make_relation(Node *this, Node *next, Node *prev);
Node *nalloc();
void set_data(Node *n, char* d);
void del_node(Node *n);
Node *make_node(char *data);
void add_last(Node *this, Node *tail);
void add_next(Node *this, Node *curr);
void init_array(Node *n[]);
void init_array_int(int n[]);
int split_word(char str[]);
int find_data(char string[]); //return 0->not found 1->found index alpha 2->found the others
int return_index(char string[]);
void input_data(char string[]);
void print_nodes(int index);
void del_word(char string[]);
void compact_nodes(int index);


//########################################### global
Node *head[NODESIZEINDEX], *tail[NODESIZEINDEX]; //리스트 생성

/*
################################################
                 Node comment
################################################

[Node] -> [Node] -> [Node]   (fp)
[Node]    [Node]    [Node]
[Node] <- [Node] <- [Node]   (bk)

################################################
*/

int file_size(char *filename){
	FILE *temp;
	int ret;
	if(access(filename, 00)==0){
		//file exist
		temp=fopen(filename, "rb");
		fseek(temp, 0, SEEK_END);
		ret=ftell(temp);
		fclose(temp);
	}
	else{
		ret=0;
	}
	return ret;
}

//########################################### function
int main(){
	FILE *fp, *voc_bin_r, *voc_ascii_r, *voc_bin_w, *voc_ascii_w;
	Node *mid;
	int i, user_choice;
	char oneline[1024];
	char *str;
	char user_input[128];
	char front_space[128]=" ";
	char bin_file_buffer[BUF_SIZE], temp_buffer[BUF_SIZE];
	char *voc_bin="voc.txt", *voc_ascii="voc_ascii.txt";
	int voc_size, bk=0;
	
	init_array(head);
	init_array(tail);
	//init nodex heads, tails array

	for(i=0; i<=NODESIZEINDEX; i++){
		head[i]=make_node("head");
		tail[i]=make_node("tail");
		make_relation(head[i], tail[i], NULL);
		make_relation(tail[i], NULL, head[i]);
		//initialize Nodelist
	}

	fp=fopen("sample.txt", "r");
	if(fp!=NULL){
		while(!feof(fp)){
			str=fgets(oneline, sizeof(oneline), fp);
			str[strlen(str)-1]='\0';
			//파일 읽어서 한 줄로 읽고 개행문자 삭제
			split_word(str);
			//한 줄을 각 단어별로 나누고 노드리스트에 추가
		}
		fclose(fp);
	}
	else{
		printf("No file sample.txt");
	}
	
	voc_size=file_size(voc_bin);

	if(voc_size>0){
		voc_bin_r=fopen(voc_bin, "rb");
	
		fread((void *)bin_file_buffer, voc_size+1, 1, voc_bin_r);
		strncpy(temp_buffer, bin_file_buffer, voc_size);
		//printf("%s\n", bin_file_buffer);
		split_word(bin_file_buffer);
		
		fclose(voc_bin_r);
	}
	else{
		strcpy(temp_buffer, "");
	}
	
	printf("Choice Function:\n[1]Search [2]Insert\n[3]Delete [4]Quit\n[5]Store in ASCII\n[6]Store in Binary\n");
	while(1){
		printf("Select: ");
		scanf("%d", &user_choice);
		switch(user_choice){
			case 1:
				printf("Search what: ");
				scanf("%s", user_input);
				strlwr(user_input);
				
				if(find_data(user_input)!=0){
					printf("-> It is in dictionary\n");
				}
				else{
					printf("-> Not in dictionary\n");
				}
				
				strcat(user_input, " ");
				strcat(front_space, user_input);
				if(strstr(temp_buffer, front_space)==NULL){
					strcat(temp_buffer, front_space);
				}
				strcpy(front_space, " ");
				//단어장에 없으면 select 동작을 할 때 voc파일에 저장할 수 있게 문자열 더함
				break;
			case 2:
				printf("Insert what: ");
				scanf("%s", user_input);
				strlwr(user_input);
				if(find_data(user_input)!=0){
					printf("-> Already exist\n");
				}
				else{
					input_data(user_input);
					printf("-> Insert complete\n");
				}
				break;
			case 3:
				printf("Delete what: ");
				scanf("%s", user_input);
				strlwr(user_input);
				if(find_data(user_input)!=0){
					del_word(user_input);
					printf("-> Delete complete\n");
				}
				else{
					printf("-> Not exist\n");
				}
				break;
			case 4:
				printf("==Exit==\n");
				bk=1;
				break;
			case 5:
				voc_ascii_w=fopen(voc_ascii, "w");
				fputs(temp_buffer, voc_ascii_w);
				fclose(voc_ascii_w);
				printf("Stored with ASCII mode\n");
				break;
			case 6:
				voc_bin_w=fopen(voc_bin, "wb");
				fwrite(temp_buffer, strlen(temp_buffer), 1, voc_bin_w);
				fclose(voc_bin_w);
				printf("Stored with Binary mode\n");
				break;
			default:
				printf("Wrong command\n");
				continue;
		}
		if(bk==1){
			break;
		}
	}
	return 0;
}
int split_word(char str[]){
	//여기에서 linkedlist작업함
	char *strTok;
	char temp[128];
	strTok=strtok(str, " ");
	while(strTok!=NULL){
		if(!isalpha(strTok[0])){
			strTok=strtok(NULL, " ");
			continue;
		}
		strcpy(temp, strTok);
		strlwr(temp);
		input_data(temp);
		strTok=strtok(NULL, " ");
	}
	return 0;
}

void print_nodes(int index){
	//index array의 모든 노드를 출력 head[index]이후부터 tail[index]전까지
	Node *temp;
	printf("Nodes %c : ", index+'a');
	temp=head[index]->fp;
	while(strcmp(temp->data, tail[index]->data)!=0){
		printf("%s ", temp->data);
		temp=temp->fp;
	}
	printf("\n");
}

int return_index(char string[]){
	//단어배열의 0번째 문자에서 'a'를 빼서 노드계산에 쓸 index를 리턴
	char alphabet=string[0];
	int index;
	
	if(alphabet>='A' && alphabet<='Z'){
		alphabet=alphabet-'A'+'a';
	}
	index=alphabet-'a';
	return index;
}
void input_data(char string[]){
	//5개까지는 각 노드로, 5개를 넘는 경우 theothers로
	Node *temp;
	int index;
	
	if(find_data(string)==0){
		index=return_index(string);
		temp=make_node((char*)string);
		add_last(temp, tail[index]);
		//갯수 제한없이 노드리스트에 추가
	}
}

void del_word(char string[]){
	Node *temp;
	int index, found;
	found=find_data(string);
	if(found==1){
		index=return_index(string);
	}
	else{
		printf("No word found\n");
		return ;
	}
	temp=head[index]->fp;
	while(strcmp(temp->data, tail[index]->data)!=0){
		if(strcmp(temp->data, string)==0){
			del_node(temp);
		}
		temp=temp->fp;
	}
}

int find_data(char string[]){
	Node *temp;
	int ret=0;
	//0 -> not found  1 -> found in node alphabet
	char alphabet=string[0];
	int index;
	
	index=return_index(string);
	temp=head[index]->fp;
	while(strcmp(temp->data, tail[index]->data)!=0){
		if(strcmp(temp->data, string)==0){
			ret=1;
			break;
		}
		temp=temp->fp;
	}
	return ret;
}




void init_array(Node *n[]){
	//init all element of array
	int i;
	for(i=0; i<sizeof(n); i++){
		n[i]=NULL;
	}
}


//################################-fragment func
Node *nalloc(){
	//node alloc
	return (Node *)malloc(sizeof(Node));
}
void set_data(Node *n, char* d){
	//set data
	strcpy(n->data, d);
}

void add_next(Node *this, Node *curr){
	//curr Node를 기준으로 this 뒤를 curr 뒤의 Node로 this 앞을 curr로 설정 후 curr의 앞의 뒤를 this로 함
	if(curr->fp!=NULL){
		this->fp=curr->fp;
		this->bk=curr;
		
		curr->fp->bk=this;
		curr->fp=this;
	}
}

void add_last(Node *this, Node *tail){
	//tail을 기준으로 tail뒤에 있는 Node(현재 가장 마지막 데이터)를 새 Node의 뒤로 tail 앞을 새 Node로 연결함
	this->bk=tail->bk;
	this->fp=tail;
	
	tail->bk->fp=this;
	tail->bk=this;
}

void del_node(Node *n){
	//앞 뒤 연결을 각자로 연결시키고 (지울 노드는 free해서 그냥 없앰)
	if(n==NULL){
		return;
	}
	n->bk->fp=n->fp;
	n->fp->bk=n->bk;
	free(n);
}

Node *make_node(char *data){
	//Node 구조체로 하나 만듬
	Node *new=nalloc();
	set_data(new, data);
	return new;
}

void make_relation(Node *this, Node *next, Node *prev){
	//앞 뒤 연결 끌어와서 관계를 새로 만듬
	if(next!=NULL){
		next->bk=this;
	}
	if(prev!=NULL){
		prev->fp=this;
	}
	
	this->fp=next;
	this->bk=prev;
}
