#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct node {
	struct node* next;
	struct node* prev;
	char* value;
}NODE;

typedef struct list {
	NODE* header;
	NODE* temp;
	int* number;
}LIST;

LIST add_List(char* word, LIST list) {
	if (*list.number == 0) {
		list.header = (NODE*)malloc(sizeof(NODE));
		list.header->value = (char*)malloc(strlen(word) * sizeof(char));
		strcpy(list.header->value, word);
		list.header->next = NULL;
		list.header->prev = NULL;
		*list.number += 1;
	}
	else if (*list.number == 1) {
		NODE *newnode = (NODE*)malloc(sizeof(NODE));
		list.header->next = newnode;
		newnode->value = (char*)malloc(strlen(word) * sizeof(char));
		newnode->next = NULL;
		newnode->prev = list.header;
		strcpy(newnode->value, word);
		list.temp = newnode;
		*list.number += 1;
	}
	else {
		NODE *newnode = (NODE*)malloc(sizeof(NODE));
		newnode->value = (char*)malloc(strlen(word) * sizeof(char));
		newnode->next = NULL;
		newnode->prev = list.temp;
		strcpy(newnode->value, word);
		list.temp->next = newnode;
		list.temp = newnode;
		*list.number += 1;
	}

	return list;
}

void add(char* word, LIST* list) {
	NODE *tmp;
	for (int i = 0; i < 26; i++) {
		if (word[0] == ('a' + i)) {
			tmp = list[i].header;
			while (1) {
				if (tmp == NULL) break;
				if (strcmp(word, tmp->value) == 0) return;
				tmp = tmp->next;
			}
			list[i] = add_List(word, list[i]);
		}
	}
}

void storage(char* line, LIST* list) {
	char* word;
	int index = 0;

	for (int i = 0; i < strlen(line); i++) {
		if (isalpha(line[i])) {
			if (isupper(line[i])) {
				line[i] = tolower(line[i]);
			}
			word[index] = line[i];
			index++;
		}
		if (line[i] == ' ' || line[i] == '\n') {
			word[index] = '\0';
			add(word, list);
			index = 0;
		}
	}
}

void select_Insert(char* input, LIST* list) {
	NODE *tmp;
	tmp = list->header;
	for (int i = 0; i < *(list->number); i++) {
		if (strcmp(input, tmp->value) == 0) {
			printf("-> 이미 단어장에 있습니다.\n");
			return;
		}
		tmp = tmp->next;
	}
	*list = add_List(input, *list);
	printf("-> 추가되었습니다.\n");
}

void select_Search1(char* input, LIST* list1, LIST* list2) {
	NODE *tmp;
	
	tmp = list2->header;
	for (int j = 0; j < *(list2->number); j++) {
		if (strcmp(input, tmp->value) == 0) {
			printf("-> 단어장에 있습니다.\n");
			return;
		}
		tmp = tmp->next;
	}

	for (int i = 0; i < 26; i++) {
		tmp = list1[i].header;
		for (int j = 0; j < *(list1[i].number); j++) {
			if (strcmp(input, tmp->value) == 0) {
				printf("-> 사전에 있습니다.\n");
				*list2 = add_List(input, *list2);
				printf("-> 단어장에 추가되었습니다.\n");
				return;
			}
			tmp = tmp->next;
		}
	}
	printf("-> 사전에 없습니다.\n");
}

void select_Search2(char* input, LIST* list) {
	NODE *tmp;
	tmp = list->header;
	for (int j = 0; j < *(list->number); j++) {
		if (strcmp(input, tmp->value) == 0) {
			printf("-> 단어장에 있습니다.\n");
			return;
		}
		tmp = tmp->next;
	}
	printf("-> 단어장에 없습니다.\n");
}

void select_Delete(char* input, LIST* list) {
	NODE *tmp;
	tmp = list->header;
	
	for (int i = 0; i < *(list->number); i++) {
		if (strcmp(input, tmp->value) == 0) {
			if (list->header == list->temp) {
				list->header = NULL;
			}
			if (tmp->prev == NULL) {
				list->header = list->header->next;
			}
			else if (tmp->next == NULL) {
				list->temp = list->temp->prev;
			}
			else {
				tmp->next->prev = tmp->prev;
				tmp->prev->next = tmp->next;
			}
			free(tmp);
			*list->number -= 1;
			printf("-> 삭제되었습니다.\n");
			return;
		}
		tmp = tmp->next;
	}

	printf("-> 단어장에 없습니다.\n");
}

void select_StoreASCII(FILE* bar, LIST* list) {
	printf("조교님 화이팅!\n");
}

void select_StoreBinary(FILE* bar, LIST* list) {
	bar = fopen("voc.txt", "wb");

	NODE *tmp;
	char word[20];
	tmp = list->header;

	while (tmp != NULL) {
		strcpy(word, tmp->value);
		strcat(word, " ");
		fwrite(word, strlen(word), 1, bar);
		tmp = tmp->next;
	}
}

int main(void) {
	FILE *fp = fopen("sample.txt", "rt");
	FILE *bar = fopen("voc.txt", "rb");
	int select = 0;
	LIST *list1;
	LIST *list2;
	char inbuf;
	int i = 0;
	char line[400];
	int count[27];
	char word[30];
	char input[100];

	list1 = (LIST*)malloc(26 * sizeof(LIST));
	list2 = (LIST*)malloc(sizeof(LIST));

	if (fp == NULL) {
		printf("파일을 열 수 없습니다.\n");
		return 1;
	}

	for (int j = 0; j < 26; j++) {
		count[j] = 0;
		list1[j].header = NULL;
		list1[j].number = &count[j];
	}
	count[27] = 0;
	list2->header = NULL;
	list2->number = &count[27];

	while (fgets(line, sizeof(line), fp) != NULL) {
		storage(line, list1);
	}
	
	while (fread(&inbuf, sizeof(char), 1, bar)) {
		word[i] = inbuf;
		i++;
		if (inbuf == ' ') {
			word[i-1] = '\0';
			*list2 = add_List(word, *list2);
			word[0] = '\0';
			i = 0;
		}
	}
	fclose(bar);

	printf("기능을 선택하세요.\n");
	printf("[1] Search1 [2] Search2\n");
	printf("[3] Insert  [4] Delete\n");
	printf("[5] Store in binary\n");
	printf("[6] store in  ASCII\n");
	printf("[7] Quit\n");

	while (select != 7) {
		printf(": ");
		scanf("%d", &select);
		while (getchar() != '\n');

		switch (select) {
		case 1:
			printf("사전에서 찾을 단어를 입력하세요: ");
			scanf("%s", input);
			select_Search1(input, list1, list2);
			break;
		case 2:
			printf("단어장에서 찾을 단어를 입력하세요: ");
			scanf("%s", input);
			select_Search2(input, list2);
			break;
		case 3:
			printf("추가할 단어를 입력하세요: ");
			scanf("%s", input);
			select_Insert(input, list2);
			break;
		case 4:
			printf("삭제할 단어를 입력하세요: ");
			scanf("%s", input);
			select_Delete(input, list2);
			break;
		case 5:
			select_StoreBinary(bar, list2);
			break;
		case 6:
			select_StoreASCII(bar, list2);
			break;
		case 7:
			break;
		}
	}

	fclose(fp);
	fclose(bar);
	return 0;
}
