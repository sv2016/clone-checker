#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define SIZE 256
#define W_SIZE 20

	const char *s = str;
	const char *p = pattern;
	const char *q = 0;
	const char *r = 0;
	int state = 0;

	bool match = true;
	while(match && *p)
	{
		if(*p == '*')
		{
			state = star;
			q = p + 1;
		}
		else if(*p == '?')
		{
			state = ques;
			r = p + 1;
		}
		else state = value;

		if(*s == 0) break;

		switch(state)
		{
			case value:
				match = (*s == *p);
				s++;
				p++;
				break;

			case ques:
			{
				if(*s == *r)
				{
					match = true;
					p++;
				}
				else
				{
					match = true;
					s++;
					p++;
				}
				break;
			}

			case star:
			{
				if(*s == *q)
				{
					match = true;
					p++;
				}
				else
				{
					match = true;
					s++;
					if(*s == *q) p++;
				}
				break;
			}
		}//switch
	}//while
	return match && (*s == *p);
}


void init(LIST *lpt)
{
	lpt->count = 0; //초기화
	lpt->head = NULL;
}
void insert(LIST *lpt, char *word)
{
	NODE *new_node = (NODE*)malloc(sizeof(NODE));
	strcpy(new_node->word, word);
	if(lpt->count==0)
	{
		new_node->link = lpt->head;
		lpt->head = new_node;
	}
	else
	{
		NODE *tmp = lpt->head;
		int i;
		for(i=1; i<lpt->count; i++)
		{
			tmp = tmp->link;
		}
		new_node->link= tmp->link;
		tmp->link = new_node;
	}
	lpt -> count++;
}
void delete(LIST *lpt, char *word)
{
	NODE *tmp = lpt->head;
	NODE *tmp2 = tmp;//템프 전노드 기억
	if(lpt->head == NULL)
		printf("error\n");
	else
	{
		if(string_match(tmp->word, word))
		{
			lpt->head = tmp->link;
			free(tmp);
		}
		else
		{
			int i;
			for(i=1; i < lpt->count; i++) // the others 에서도.
			{
				tmp2 = tmp;
				tmp = tmp->link;
				if(string_match(tmp->word, word))
					break;
			}
			NODE *tmp3 =tmp2->link;
			tmp2->link = tmp3->link;
			free(tmp3);
		}
		lpt ->count--;
	}
}
bool search(LIST *lpt, char *word)
{
	NODE *tmp = lpt->head;
	int i = 1;
	while(tmp!=NULL)
	{
		if(string_match(tmp->word, word))
			break;
		i++;
		tmp = tmp->link;
	}
	if(i>lpt->count)
		return false;//존재 x
	else
		return true; //존재 o
	
}
void print_list(LIST *lpt)
{
	NODE *tmp = lpt->head;
	printf("List value : ");
	while(tmp!=NULL)
	{
		printf("%s ", tmp->word);
		tmp = tmp ->link;
	}
	printf("\n");
	printf("Total = %d value(s)\n", lpt->count);
}
int main(void)
{
	FILE *fp = NULL;
	char buffer[SIZE]; // 파일에서 입력받는 버퍼
	char buffer2[SIZE]; //파일에서 받은 버퍼 뒷부분을 잘라서 2에 복사
	int t, u = 0; // 반복문
	int c = 0; //temp카운트용
	int d = 0; //삭제할 리스트 기억
	int select = 0; //메인에서 사용자 선택
	char I_word[W_SIZE];
	char s_w[3];//a*형태로 검색하기 위한용도
	char *temp[SIZE];//영어 인지 확인용
	char *token = NULL;
	char str[] = " ,.:;\t\n\"'!?-";
	LIST *mylist[26];
	LIST *diclist[26];
	NODE tmp;
	NODE *ptmp;
	for(t = 0; t <26; t++)
	{
		mylist[t] = (LIST*)malloc(sizeof(LIST));
		init(mylist[t]);
	}
	for(t = 0; t <26; t++)
	{
		diclist[t] = (LIST*)malloc(sizeof(LIST));
		init(diclist[t]);
	}
	if((fp = fopen("voc.txt", "rb"))==NULL)
	{
		fprintf(stderr,"단어장 파일이 없습니다.\n");
	}
	else
	{
		while(fread(&tmp, sizeof(NODE), 1, fp))
		{
			t = tmp.word[0] - 97;
			insert(mylist[t],tmp.word);//읽은 파일로 리스트 작성
			//printf("%s\n",tmp.word);
		}
		
		fclose(fp);
	}
	fp = fopen("sample.txt", "r");
	while(fgets(buffer, 256, fp))
	{
		//printf("%s",buffer);
		for(t = 0; t < SIZE; t++)//복사받을 버퍼 2 초기화
			buffer2[t] = '\0';
		strncpy(buffer2, buffer, strlen(buffer)-2);
		token = strtok(buffer2, str);
		c = 0;
		while(token != NULL)
		{
			t = 0;
			temp[c] = token;
			if(isalpha(temp[c][0]))//영문자인지 확인
				t = 1;
			if(t!=0)
			{
				if(isupper(temp[c][0]))
				{	
					for(t=0; t<strlen(temp[c]); t++)
						temp[c][t] = tolower(temp[c][t]);//다 소문자로 바꾸기
				}
				for(t=0; t<26; t++)
				{
					if(temp[c][0]==(t+97))
					{
						if(diclist[t]==NULL) //없으면 첫자리
							insert(diclist[t], token);
						else
						{
							if(search(diclist[t], token)==false) //앞에 리스트에 있는단어인지 확인
								insert(diclist[t], token);
						}
					}
				}
				//printf("token = %s\n",token);
				token = strtok(NULL, str);
			}
			else
				token = strtok(NULL, str);
			c++;
		}
	}
	//print_list(mylist[0]);
	//print_list(mylist[26]);
	while(1)
	{
		c = 0; //처음 리스트 생성후 안쓰이는 변수 재활용
		d = 0;
		for(t=0;t<sizeof(I_word);t++)
			I_word[t] = '\0';
		printf("기능을 선택하세요\n");
		printf("  [1] Search          [2] Insert\n");
		printf("  [3] Delete          [4] Store in ASCII\n");
		printf("  [5] Store in Binary [6] Quit\n");
		printf(":");
		scanf("%d",&select);
		fflush(stdin);//scanf연속 입력받을경우 버퍼 비워줘야함
		if(select == 1 )
		{
			//print_list(mylist[0]); //test용
			//print_list(mylist[1]);
			//print_list(mylist[26]);
			printf("  [1] 사전에서 찾기 [2] 단어장에서 찾기\n");
			printf(":");
			scanf("%d", &select);
			fflush(stdin);
			if(select == 1 )
			{
				printf("찾을 단어를 입력하세요: ");
				scanf("%s", I_word);
				for(t=0;t<26;t++)//검색
				{
					if(search(diclist[t], I_word))
					{
						c++;
						d = t;
					}
				}
				if(c>0)
					printf("-> 사전에 있습니다\n");
				else
					printf("-> 사전에 없습니다\n");
				c = 0;
				for(t=0;t<26;t++)//검색
				{
					if(search(mylist[t], I_word))
						c++;
				}
				if(c>0)
					printf("-> 단어장에 있어 추가하지 않습니다.\n");
				else
					insert(mylist[d], I_word);//없으면 단어장에 추가
				select = 0;
			}
			if(select == 2)
			{
				printf("찾을 단어를 입력하세요: ");
				scanf("%s", I_word);
				for(t=0;t<26;t++)//검색
				{
					if(search(mylist[t], I_word))
						c++;
				}
				if(c>0)
					printf("-> 단어장에 있습니다\n");
				else
					printf("-> 단어장에 없습니다\n");
				select = 0;
			}
		}
		if(select == 2)
		{
			printf("추가할 단어를 입력하세요: ");
			scanf("%s", I_word);
			for(t=0;t<26;t++)//있으면 추가 안함;
			{
				if(search(mylist[t], I_word))
				{
					c++;
					d = t;
				}
			}
			if(c>0)
				printf("이미 있는 단어입니다.\n");
			else
			{
				printf("->단어장에 추가했습니다.\n");
					insert(mylist[d], I_word);
			}
		}
		if(select == 3)
		{
			printf("삭제할 단어를 입력하세요: ");
			scanf("%s", I_word);
			for(t=0;t<26;t++)//검색 있어야 지움
			{
				if(search(mylist[t], I_word))
				{
					c++;
					d = t;
				}
			}
				if(c>0)
				{
					delete(mylist[d], I_word);
					printf("->삭제 되었습니다\n");
				}
				else
				{
					printf("->없는 단어입니다.\n");
					printf("->삭제 할 수 없습니다.\n");
				}
		}
		if(select == 4)
		{
			fp = fopen("voc2.txt","w");//voc.txt로하면 읽을때 이진파일로 읽기때문에 리스트를 생성하지 않고, 이진파일로만 읽기때문에 파일명을 다르게함
			for(t=0;t<26;t++)
			{	
				ptmp = mylist[t]->head;
			
				while(ptmp!=NULL)
				{
					fputs(ptmp->word,fp);
					fputs("\r\n",fp);
					//printf("%s\n",ptmp->word);
					ptmp = ptmp->link;
				}
			}
			fclose(fp);
		}
		if(select == 5)
		{
			fp = fopen("voc.txt", "wb");
			for(t=0;t<26;t++)
			{	
				ptmp = mylist[t]->head;
			
				while(ptmp!=NULL)
				{
					fwrite(ptmp,sizeof(NODE),1,fp);
					//printf("%s\n",ptmp->word);
					ptmp = ptmp->link;
				}
			}
			fclose(fp);
		}
		if(select == 6)
			break;
	}
	return 0;
}
