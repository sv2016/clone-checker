#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct data{
	char word[20];	
}DATA;

typedef struct NODE{
	DATA data;
	struct NODE* link;
}NODE;


NODE* insert_node(NODE* plist,NODE* pprev,DATA item);
void search();
void insert();
void delete();
void store_in_ASCII(char search[]);
void store_in_binary(char search[]);
int wordsize(char check,int size);
void wordreturn(char check,DATA* pdata,int size); 

char s[101];
char seps[]=" ,\t\n";
char* token;

int main(void)
{
	NODE* p=NULL;	
	DATA* pdata=NULL;
	int inputNum;	
	int wordNum;	
//	char check='a';
	int size;
	int i;
	int j;
	int nodeaddress[26];
	for(i=0;i<26;i++) //a~z에 대해 단어 숫자를 세고 그에대한 단어NODE 생성
	{
		size=0;	
		size=wordsize('a'+i,size)+1;
		p=(NODE*)malloc(size*sizeof(NODE));
//		nodeaddress[i]=p;
//		printf("%d\n",nodeaddress[i]);
		pdata=(DATA*)malloc(size*sizeof(DATA));
		wordreturn('a'+i,pdata,size);	
		for(j=0;j<size;j++)
		{
			if(j<size-1)
				(p+j)->link=(p+j+1);
			(p+j)->data=*(pdata+j);
		}
	}


//	printf("---------NODE생성까지-----------\n");

	while(1)
	{	
		printf("기능을 선택하세요.\n");
		printf("[1]Search [2]Insert\n");
		printf("[3]Delete [4]Quit\n");	


//		printf("---------기능 선택------------\n");
		scanf("%d",&inputNum);

		switch(inputNum)
		{
			case 1:search(); break;
			case 2:insert(); break;
			case 3:delete(); break;
			case 4:exit(1); break; 
			default:printf("1~4의 숫자를 입력하세요.\n");
		}
	}	

	free(pdata);
	free(p);
	return 0;
}

void search()
{
	char search[50];
	char replyword;
	int replynum;
	printf("찾을 단어를 입력하세요. :");
	scanf("%s",search);	
	
	FILE* fp;
	fp=fopen("sample.txt","r");

	while(!feof(fp))
	{
		fgets(s,100,fp);
		token=strtok(s,seps);

		while(token!=NULL)
		{
			if(strcasecmp(search,token)==0)
			{
				printf("단어가 존재합니다.\n");
				printf("저장하시겠습니까? Y/N\n\n");	
				scanf("%c",&replyword);

				if(replyword=='Y'||replyword=='y')
				{	
					printf("[1]store in ASCII [2]store in binary\n"); 	

					while(!(replynum==1||replynum==2))
					{
						scanf("%d",&replynum);
						if(!(replynum==1 || replynum==2))
							printf("1또는 2를 입력하세요\n");
					}

					if(replynum==1)
						store_in_ASCII(search);	
					else if(replynum==2)
						store_in_binary(search);
				}	

				else if(replyword=='N'||replyword=='n')
				{
					exit(1);
				}
				
			}
		
			token=strtok(NULL,seps);
			
		}
	}	
}

void insert()
{
}

void delete()
{
}

void store_in_ASCII(char search[])
{
	FILE* fp;
	fp=fopen("voc.txt","a");

	fprintf(fp,"%s\n",search);
	fclose(fp);
}

void store_in_binary(char search[])
{
	FILE* fp;
	fp=fopen("voc.txt","ab");
	size_t size,count;	
	size=sizeof(search[0]);
	count=sizeof(search)/sizeof(search[0]);	

	fwrite(search,size,count,fp);
	fclose(fp);
}

int wordsize(char check,int size)
{
	FILE* fp;
	fp=fopen("sample.txt","r");

	while(!feof(fp))
	{
		fgets(s,100,fp);
		token=strtok(s,seps);

		while(token!=NULL)
		{
			if(token[0]==check||token[0]==check-32)
			{
				size++;
			}
		
			token=strtok(NULL,seps);
			
		}
	}	
	return size;
}  

void wordreturn(char check,DATA* pdata,int size)
{
	FILE* fp;
	int j;
	fp=fopen("sample.txt","r");
	
	while(!feof(fp))
	{
		fgets(s,100,fp);
		token=strtok(s,seps);

		while(token!=NULL)
		{
			if(token[0]==check||token[0]==check-32)
			{
				for(j=0;j<size;j++)
					strcpy((pdata+j)->word,token);	
			}
		
			token=strtok(NULL,seps);	
		}
	}	
}

NODE* insert_node(NODE* plist,NODE* pprev,DATA data)
{
	NODE* pnew=NULL;

	if(!(pnew=(NODE*)malloc(sizeof(NODE))))
	{
		printf("메모리 동적 할당 오류\n");
		exit(1);
	}

	pnew->data=data;
	if(pprev==NULL)
	{
		pnew->link=plist;
		plist=pnew;
	}
	else
	{
		if((pprev->link)!=NULL)
		{
			pnew->link=pprev->link;
			pprev->link=pnew;
		}
		else
		{
			pprev->link=pnew;
		}
	}
	return plist;
}
