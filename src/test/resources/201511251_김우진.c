#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

char word_d[] = "_ - \t \" \' . , ? / : \;";
char line_t[512];
int num=0;
int delete_time = 0;

typedef struct node{
	char element[32];
	struct node *prev;
	struct node *next;
}NODE;

typedef struct list{
	NODE *header;
	NODE *trailer;
	int size; 
} LIST;

void setList(LIST* list){
	list->header = (NODE*)malloc(sizeof(NODE));
	list->trailer = (NODE*)malloc(sizeof(NODE));

	list->header->next = list->trailer;
	list->trailer->prev = list->header;
	list->header->prev = NULL;
	list->trailer->next = NULL;
	list->size = 0;
}

void addFirst(LIST* list, char name[]){
	NODE *temp = (NODE*)malloc(sizeof(NODE));
	NODE *temp2 = list->header->next;
	strcpy(temp->element, name);
	
	temp2->prev = temp;
	temp->next = list->header->next;
	list->header->next = temp;
	temp->prev = list->header;

	(list->size)++;
}

void addLast(LIST* list, char name[]){
	NODE *temp = (NODE*)malloc(sizeof(NODE));
	NODE *temp2 = list->trailer->prev;

	strcpy(temp->element, name);
	temp2->next = temp;
	temp->prev = list->trailer->prev;
	temp->next = list->trailer;
	list->trailer->prev = temp;

	(list->size)++;
}

void printList(LIST* list){
	NODE *temp = list->header;

	while (temp != NULL){
		printf("%s ", temp->element);
		temp = temp->next;
	}
	printf("\n");
}

int find_Node(LIST* list,char str[]){
	NODE * temp = list->header;
	while (temp != NULL){
		if (!strcmp(temp->element, str))
			return 1;
		else
			temp = temp->next;
	}
	return 0;
}

void delete_Node(LIST* list,char str[]){
	delete_time = 0;
	NODE * temp = list->header;
	while (temp != NULL){
		if (!strcmp(temp->element, str)){
			NODE * temp_p = temp->prev;
			NODE * temp_n = temp->next;

			temp_p->next = temp_n;
			temp_n->prev = temp_p;
			
			temp = temp_n;
			delete_time++;
		}
		else{
			temp = temp->next;
		}
	}
}

int getSize(LIST *list){
	return list->size;
}

void my_tolower(char word[]){
	int i;
	for (i = 0; i < strlen(word); i++){
		if (isupper(word[i]))
			word[i] = tolower(word[i]);
	}
}


void main(){
	LIST list[27];
	int i;

	for (i = 0; i < 26; i++){
		setList(&list[i]);
		list[i].header->element[0] = '#';
		list[i].header->element[1] = '65+i';
		list[i].header->element[2] = '\0';
		strcpy(list[i].trailer->element, " ");
	}

	LIST voca;

	setList(&voca);
	voca.header->element[0] = '\0';
	voca.trailer->element[0] = '\0';

	FILE *Dic = fopen("sample.txt", "r");
	if (Dic != NULL){
		while (!feof(Dic)){
			fgets(line_t, 512, Dic);
			char *word_t = strtok(line_t, word_d);
			my_tolower(word_t);

			while (word_t != NULL){
				for (i = 0; i < 26; i++){
					if (word_t[0] == 97 + i){
						if (!find_Node(&list[i], word_t))
							addLast(&list[i], word_t);
					}
				}
				word_t = strtok(NULL, word_d);
			}
		}
		fclose(Dic);
	}

	FILE *Voc = fopen("binary.bin", "rb");
	if (Voc != NULL){
		char buffer[32];

		while (0 < fread(buffer, sizeof(voca.header->element), 1, Voc)){
			addLast(&voca, buffer);
		}

		fclose(Voc);
	}
	while (num != 7){
		printf("============================= \n");
		printf("Dictionary \n");
		printf("[1] Search \n");
		printf("Vocabulary \n");
		printf("[2] Search [3] Insert \n");
		printf("[4] Delete [5] Store in ASCII \n");
		printf("[6] Store in binary \n");
		printf("[7] Quit \n");
		printf("============================= \n");
		scanf("%d", &num);

		if (num == 1){
			int i;
			char f_word[64];
			printf("찾을 단어를 입력하세요 : ");
			scanf("%s", f_word);
			my_tolower(f_word);
			for (i = 0; i < 26; i++){
				if (f_word[0] == 97 + i){
					if (find_Node(&list[i], f_word)){
						printf("->사전에 있습니다.\n");

						if (find_Node(&voca, f_word)){
							printf("->단어장에 이미 존재하여 추가하지 않습니다. \n");
						}
						else{
							addLast(&voca, f_word);
							printf("->단어장에 추가하였습니다.\n");
						}
					}
					else
						printf("->사전에 없습니다.\n");
				}
			}
		}

		if (num == 2){
			char f_word[64];
			printf("찾을 단어를 입력하세요 : ");
			scanf("%s", f_word);
			my_tolower(f_word);
			if (find_Node(&voca, f_word))
				printf("->단어장에 있습니다.\n");
			else
				printf("->단어장에 없습니다.\n");
		}

		else if (num == 3){
			int i;
			char i_word[64];
			printf("삽입할 단어를 입력하세요 : ");
			scanf("%s", i_word);
			my_tolower(i_word);
			
			if (find_Node(&voca, i_word)){
				printf("->단어장에 이미 존재합니다.\n");
			}
			else{
				addLast(&voca, i_word);
				printf("->입력되었습니다.\n");
			}
		}
		else if (num == 4){
			int i;
			char d_word[64];
			printf("삭제할 단어를 입력하세요 : ");
			scanf("%s", d_word);
			my_tolower(d_word);
		
			if (find_Node(&voca, d_word)){
				delete_Node(&voca, d_word);
				printf("->삭제 하였습니다 \n");
			}
			else{
				printf("->단어장에 존재하지 않는 단어입니다 \n");
			}
		}
		else if (num == 5){
			FILE *Asc = fopen("ascii.txt", "w");

			NODE *temp = voca.header;

			while (temp != NULL){
				strcat(temp->element, " ");
				fputs(temp->element, Asc);

				temp = temp->next;
			}

			fclose(Asc);
			printf("->ASCII 저장 완료 \n");
		}

		else if (num == 6){
			FILE *Bin = fopen("binary.bin", "wb");

			NODE* temp = voca.header->next;
			
			while (temp != voca.trailer){
				fwrite(temp->element, sizeof(temp->element) , 1, Bin);
				temp = temp->next;
			}

			fclose(Bin);
			printf("->BINARY 저장 완료 \n");
		}

		else if (num == 8){
			printList(&voca);
		}
		else{
			continue;
		}
	}
}
