#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

typedef struct _node{
	char value[256];
	struct _node* next;
}node;

//자주 사용될 node 포인터 변수
typedef node* nptr;

typedef struct _list{
	//list에 몇 개의 노드가 있는지 알려준다
	int count;
	nptr head;
}list;

struct words{
	char word[256];
	char target[256];
	int wordIndex;
	int targetIndex;
	int queMark;
	int tmp; //*이 시작된 것으로 인정되는 위치(Index)
	int tmp2; //이전에 ?이었나?
};

//linkedList.c
void init(list* lptr);
void insert(list* lptr, char* value, int position);
void delete(list* lptr, int position);
int search(list* lptr, char* value);
char* getList(list* lptr);

//voca.c
void voca(FILE* fp, FILE* fp2);
void mainFunction();
int storeInASCII();
int storeInBinary();
int searchVocList(char* str);
int insertToVocList(char* str);
void writeToVoc(char* str);
int searchFunc();
int insertFunc();
void deleteFunc();
void divideLine(FILE* fp, FILE* fp2);
void token(char* s);
void token2(char* s);
void storeToken(char* token);
void storeToken2(char* token);
void viewList(list* l[26]);

//FileManaber.c
FILE* opendic();
FILE* openvoc();

//WordFind.c
int searchDic();
struct words searchLogic(struct words w);
struct words analysisLetter(struct words w);
int compareAlpha(struct words w);
char target[256];
char firstFinded[256]; //찾은 단어 중 첫 번째 단어
char listValue[1024];

list* dic[26];
list* vocab[26];

int main(void)
{
	FILE *fp = NULL;
	FILE *fp2 = NULL;
	fp = opendic();
	fp2 = openvoc();
	voca(fp, fp2);
	fclose(fp);
	
	return 0;
}

void init(list* lptr)
{
	//리스트를 초기화한다
	lptr->count = 0;
	lptr->head =NULL;
}

void insert(list* lptr, char* value, int position)
{
	//맞는 위치에 값을 삽입한다
	if(position<1||position>(lptr->count)+1)
	{
		printf("Position Out of Bound\n");
		return;
	}
	nptr new_nptr=(node*)malloc(sizeof(node));
	strcpy(new_nptr->value, value);

	if(position==1)
	{
		//단순히 새로운 노드에게 head역할을 위임
		//그 후, 새 노드가 이전의 head를 가리키게 한다
		new_nptr->next=lptr->head;
		lptr->head=new_nptr;
	}
	else
	{
		nptr tmp = lptr->head;
		int i;
		//바꾸고자 하는 노드까지 찾아간다
		for(i=1; i<position-1; i++)
		{
			tmp = tmp->next;
		}
		//노드를 삽입한다
		new_nptr->next=tmp->next;
		tmp->next=new_nptr;
	}
	lptr->count++;
}

void delete(list* lptr, int position)
{
	if(position<1||position>(lptr->count))
	{
		printf("Position Out of Bound\n");
		return;
	}
	nptr tmp=lptr->head;

	//첫 번째 노드를 삭제할 때는 단순히 head를 삭제한다
	if(position==1)
	{
		lptr->head=tmp->next;
		free(tmp);
	}
	else
	{
		int i;
		for(i=1; i<position-1; i++)
		{
			tmp=tmp->next;
		}
		nptr tmp2=tmp->next;
		tmp->next=tmp2->next;
		free(tmp2);
	}
	lptr->count--;
}

int search(list* lptr, char* value)
{
	//리스트를 순회하며 같은 값이 있는지 알아본다
	//값을 찾으면 처음 나온 position을 리턴한다
	//없으면 0을 리턴한다
	int i = 1;
	nptr tmp=lptr->head;
	while(tmp!=NULL)
	{
		if(!strcmp(value, tmp->value))
			break;
		i++;
		tmp=tmp->next;
	}
	if(i>lptr->count)
		return 0;
	else
		return i;
}

char* getList(list* lptr)
{
	//리스트 내의 모든 노드를 char배열로 반환한다.
	nptr tmp=lptr->head;
	if(tmp!=NULL)
	{	
		strcpy(listValue, tmp->value);
		strcat(listValue, " ");
		tmp=tmp->next;
	}
	else
		return "\0";
	if(tmp!=NULL)
	{
		while(tmp!=NULL)
		{
			strcat(listValue, tmp->value);
			strcat(listValue, " ");
			tmp=tmp->next;
		}
	}
	return listValue;
}
FILE* opendic()
{
	FILE *fp = NULL;
	fp = fopen("sample.txt", "r");
	if(fp == NULL)
	{
		printf("sample.txt가 없습니다!\n");
		return NULL;
	}
	else	
	{
		return fp;
	}
}

FILE* openvoc()
{
	FILE *fp = NULL;
	fp = fopen("voc.txt", "rb");
	if(fp == NULL)
	{
		printf("voc.txt가 없습니다! 새로 만들어 엽니다.\n");
		fp = fopen("voc.txt", "wb");
		fclose(fp);
		fp = fopen("voc.txt", "rb");
		return fp;
	}
	else
	{
		return fp;
	}
}

void voca(FILE* fp, FILE* fp2)
{
	int i = 0;

	//동적 메모리 할당&초기화
	for(i=0; i<27; i++)
	{
		dic[i] = (list *)malloc(sizeof(list));
		init(dic[i]);
		vocab[i] = (list *)malloc(sizeof(list));
		init(vocab[i]);
        	if(vocab[i] == NULL||dic[i] == NULL)
        	{
                	printf("동적 메모리 할당 오류\n");
                	exit(1);
       		}
	};
	divideLine(fp, fp2);
	fclose(fp);
	fclose(fp2);
	mainFunction();
}

void mainFunction()
{
	//메뉴를 띄우고, 이 프로그램의 주요 기능을 사용자가 선택하여 이용하게 하는 함수
	while(1)
	{
		int plag=0;
	        printf("기능을 선택하세요\n[0] 사전에서 검색\n[1] Search [2] Insert [3] Delete [4] 리스트 보기: Voc List\n[5] Store in ASCII [6] Store in binary: Voc List 저장\n[7] Quit\n: ");
		scanf("%d", &plag);
		switch(plag)
		{
			case 0:
			{
				if(searchDic())
					if(!searchVocList(firstFinded))
						insertToVocList(firstFinded);
					else
						printf("이미 있는 단어는 추가되지 않습니다.\n");
				break;
			}
			case 1:searchFunc(); break;
			case 2:insertFunc(); break;
			case 3:deleteFunc(); break;
			case 4:viewList(vocab); break;
			case 5:storeInASCII(); break;
			case 6:storeInBinary(); break;
			case 7:break;
			default: printf("잘못 입력하셨습니다.\n"); break;
		}
		printf("\n");
		if(plag == 7)
			break;
	}
}

int storeInASCII()
{
	FILE *fp = NULL;
	char fileName[256];
	int i;
	int select;
	while(1)
	{
		printf("[1] 저장 [2] 다른 이름으로 저장\n");
		scanf("%d", &select);
		if(select == 1)
		{
			fp = fopen("voc-ascii.txt", "w");
	                for(i=0; i<26; i++)
	                {
        	                fprintf(fp, "%s\n", getList(vocab[i]));
        	        }
        	        fclose(fp);
        	        return 1;
		}
		else if(select == 2)
		{
			printf("새 파일 이름(확장자 미포함): ");
			scanf("%s", fileName);
			strcat(fileName, ".txt");
			fp = fopen(fileName, "w");
               		for(i=0; i<26; i++)
               		{
               		        fprintf(fp, "%s\n", getList(vocab[i]));
               		}
        	        fclose(fp);
	                return 1;

		}
		else
		{
			printf("잘못 입력하셨습니다.\n");
		}
		
		if(fp == NULL)
		{
			printf("파일을 열 수 없습니다.");
			break;
		}
	}
	//파일을 열 수 없으면 0 반환
	return 0;
}

int storeInBinary()
{
	FILE *fp = NULL;
	char fileName[256];
	char value[1024];
	int i;
	int select;
	while(1)
	{
		printf("[1] 저장 [2] 다른 이름으로 저장\n");
		scanf("%d", &select);
		if(select == 1)
		{
			fp = fopen("voc.txt", "wb");
			for(i=0; i<26; i++)
			{
				strcpy(value, getList(vocab[i]));
				strcat(value, "\n");	
				fwrite(value, strlen(value), 1, fp);
			}
			fclose(fp);
			return 1;
		}
		else if(select == 2)
                {
                        printf("새 파일 이름(확장자 미포함): ");
                        scanf("%s", fileName);
                        strcat(fileName, ".txt");
                        fp = fopen(fileName, "wb");
                        for(i=0; i<26; i++)
                        {
                                
                        }
                        fclose(fp);
                        return 1;

                }
                else
                {
                        printf("잘못 입력하셨습니다.\n");
                }

                if(fp == NULL)
                {
                        printf("파일을 열 수 없습니다.");
                        break;
                }
        }
        //파일을 열 수 없으면 0 반환
	return 0;
}

int searchVocList(char* str)
{
        int num;
        if(isalpha(str[0]))
        {
                if(isupper(str[0]))
                        str[0] = tolower(str[0]);
                num = toascii(str[0])-97;
                if(search(vocab[num], str))
                {
                        printf("단어장에 있습니다.\n");
                        return 1;
                }
                else
                {
                        printf("단어장에 없습니다.\n");
                        return 0;
                }
        }
        else printf("영단어만 입력해 주세요.\n");
        return 0;
}


int insertToVocList(char* str)
{
	int num;
	if(isalpha(str[0]))
	{
		if(isupper(str[0]))
			str[0] = tolower(str[0]);
		num = toascii(str[0])-97;
		insert(vocab[num], str, vocab[num]->count+1);
		printf("'%s' List에 추가되었습니다.\n", str);
		return 1;
	}
	else
	{
	printf("영단어만 입력해 주세요.\n");
	return 0;
	}
}

void writeToVoc(char* str) //Voc.txt에 바로 저장하는 함수. 결국 사용되지 않았지만 남겨 놓았습니다.
{
	FILE *fp = NULL;
	fp = fopen("voc.txt", "ab");
	if(fp == NULL)
		printf("파일을 열 수 없습니다.");
	strcat(str, " ");
	fputs(str, fp);
	fclose(fp);
}

int searchFunc()
{
	int num; //단어 첫 글자의 알파벳 순서
	char word[256];
	printf("찾을 단어를 입력하세요: ");
	scanf("%s", word);
	return searchVocList(word);
}

int insertFunc()
{
	char word[256];
	printf("추가할 단어를 입력하세요: ");
	scanf("%s", word);
	return insertToVocList(word);
}

void deleteFunc()
{
	int num;
	int position; //찾은 단어의 위치
	char word[256];
	printf("삭제할 단어를 입력하세요: ");
	scanf("%s", word);
        if(isalpha(word[0]))
        {
                if(isupper(word[0]))
                        word[0] = tolower(word[0]);
                num = toascii(word[0])-97;
                if(search(vocab[num], word))
                {
                        position = search(vocab[num], word);
                        delete(vocab[num], position);
                        printf("삭제되었습니다.\n");
                }
                else
                {
                	printf("단어장에 없습니다.\n");
                }
        }
        else printf("영단어만 입력해 주세요.\n");
}

void divideLine(FILE* fp, FILE* fp2)
{
	char buffer[1024];
	//샂전
	while(fgets(buffer, 1024, fp))
	{
		token(buffer);
	}
	//단어장
	fread(buffer, 1024, 1, fp2);
	token2(buffer);
}
 
void token(char* s)
{
	char seps[] = " =-'\",:\t\n";
	char *token;

	token = strtok(s, seps);
	
	while(token!=NULL)
	{
		storeToken(token); 
		token=strtok(NULL, seps);
	}
}

void token2(char* s)
{
        char seps[] = " =-'\",:\t\n";
        char *token;
	
	token = strtok(s, seps);

        while(token!=NULL)
        {
                storeToken2(token);
		token=NULL;
                token=strtok(NULL, seps);
        }
}


void storeToken(char* token)
{
	int num;
	if(isalpha(token[0]))
	{
		if(isupper(token[0]))
		{
			token[0] = tolower(token[0]);
		}
		num = toascii(token[0])-97;
		insert(dic[num], token, dic[num]->count+1);
	}
}

void storeToken2(char* token)
{
        int num;
        if(isalpha(token[0]))
        {
                if(isupper(token[0]))
                {
                        token[0] = tolower(token[0]);
                }
                num = toascii(token[0])-97;
                insert(vocab[num], token, vocab[num]->count+1);
        }
}

void viewList(list* l[26])
{
	int i;
	for(i=0; i<26; i++)
	{
		
		printf("%c: %s\n", i+65, getList(l[i]));
	}
}

