#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
	char* value;
	struct node* next;
}node;
typedef node* nptr;
typedef struct list
{
	int count;
	nptr head;
}list;

void init(list* lptr);
void insert(list* lptr, char* value, int position);
void Delete(list* lptr, int position);
int search(list* lptr, char* value);
void print_list(list* lptr);

int main(void)
{
	FILE* fp = fopen("voc_sample.txt","r");
	FILE* fp2 = fopen("voc.txt","rb+");
        char str[200];
        list* initial[26];
        char* token;
        int choice;
        int i;
	list* mylist=(list*)malloc(sizeof(list));
        init(mylist);
        if(fp==NULL)
		printf("failed to open file!\n");
	else
	{
		printf("기능을 선택하세요\n[1]Search [2]Insert\n[3]Delete[4]store in ASCII\n[5]store in binary:");
		scanf("%d",&choice);
		
		switch(choice)
		{
			case 1:
				break;
			case 2:
				while(fgets(str,200,fp))
				{
					token = strtok(str," ");
					for(i=0;i<26;i++)
					{
						if(token[0] == i + 'a')
							insert(initial[i],token,mylist->count);
					}
					while(token != NULL)
					{
						token = strtok(NULL," ");
						for(i=0;i<26;i++)
						{
							if(token[0] == i+'a')
								insert(initial[i],token,mylist->count);
						}
					}
				}
			case 3:
				break;
			case 4:
				printf("ASCII 저장은 안 해도 된다 하셔서 하지 않았습니다! 조교님 수고하세요!!");
				break;
			case 5:
				if(fp2 == NULL)
					printf("failed to open binary file");
				else
					fwrite(mylist,sizeof(struct node),sizeof(struct list),fp2);

				break;
			default:
				break;
		}
		fclose(fp);
		fclose(fp2);
		fclose(fp3);
		return 0;
       }
}
void init(list* lptr)
{
	lptr->count = 0;
        lptr->head = NULL;
}
void insert(list* lptr, char* value, int position)
{
	nptr new_nptr= (node*)malloc(sizeof(node));
	strcpy(new_nptr-> value , value);
	if(position == 1)
	{
		new_nptr -> next=lptr -> head;
		lptr -> head = new_nptr;
	}
	else
	{
		nptr tmp = lptr->head;
		int i;
		for(i=1;i<position-1;i++)
		{
			tmp = tmp->next;
		}
		new_nptr -> next= tmp->next;
		tmp->next=new_nptr;
        }
        lptr->count++;
}
void Delete(list* lptr, int position)
{
	nptr tmp;
	nptr tmp2;
	if(position<1||position>(lptr->count))
	{
		printf("Position Out of bound\n");
		return;
	}
	tmp = lptr->head;
	if(position==1)
	{
		lptr->head=tmp->next;
		free(tmp);
	}
	else
	{
		int i;
		for(i=1;i<position-1;i++)
		{
			tmp=tmp->next;
			tmp2 = tmp->next;
			tmp ->next=tmp2->next;
			free(tmp2);
		}
	}
	lptr->count--;
}
int search(list* lptr, char* value)
{
	nptr tmp = lptr->head;
	int i=1;
	while(tmp!=NULL)
	{
		if(strcmp(value,tmp->value)==0)
			break;
		i++;
		tmp=tmp->next;
	}
	if(i>lptr->count)
	{
		printf("The word %s is NOT in the list!\n",value);
		return 0;
	}
	else
	{
		printf("The word %d is at position %d in the list\n",value,i);
		return i;
	}
}
