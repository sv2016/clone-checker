#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define SIZE 128*128
#define MAX 1024

struct node
{
	char data[20];
	struct node * link;
};

struct node *dic[26];
struct node *voca[26];//단어장의 노드

int insert(char *,struct node **);
int search(char *,struct node **);
int delete(char *);
void removes();
void fileopen();
int token_save(char *out[],char *);
void print(int,int);
int store_bin();
int store_asc();
int search_bin(char *str);//search

void main()
{
	int flag,data;
	char word[20];
	int ch, ch1;
	int i,j,t,a;
	
	FILE *fp;
	FILE *fdbr;//binary file read pointer
	
	char *out[MAX];
	char *input;//for dic read
	
	int buffer[1000];
	size_t size;
	
	for(j=0;j<27;j++) //initialize dic
	{
		dic[j] = ( struct node * ) malloc ( sizeof ( struct node ) );
		//memset(dic[j],0,sizeof(dic[j]));
		dic[j]->link = NULL;
	}
	for(j=0;j<27;j++)//initialize voca
	{
		voca[j] = ( struct node * ) malloc ( sizeof ( struct node ) );
		memset(voca[j],0,sizeof(voca[j]));
		voca[j]->link = NULL;
	}
	
	fp = fopen("./40-Matthew.txt","r"); //dic file open
	input=malloc(SIZE);
	while(!feof(fp)) //make nodes from dic
	{
		memset(input,'\0',SIZE);
		fgets(input, SIZE, fp);
		if(strlen(input)!=0)
		{
			if(input[strlen(input)-1]=='\n')
			{
				input[strlen(input)-1] = '\0';
			}
			flag = token_save(out, input);
			for(data=0;data<flag;data++)
			{
				insert(out[data],dic);
			}
		}
		
	}
	free(input);
	fclose(fp);//close dictionary file
	a=1;
	fdbr=fopen("./voc.txt","rb");//only binary read
	if( fdbr == NULL )//err case
	{
		fprintf(stderr, "binary.txt 파일을 열 수 없습니다.");
	}
	else
	{
		char tempchar;
		input=malloc(SIZE);
		while(!feof(fdbr))
		{
			printf("fw\t");
			i =0;
			while(1){
				fread(&tempchar, 1, 1, fdbr);
				if(tempchar==32)
				{
					break;
				}
				input[i]=tempchar;
				printf("%s\n",input);
				i++;
			}
			//printf("%s",input);
			insert(input, voca);
			//printf("%s",input);
		}
		free(input);
	}
	fclose(fdbr);//binary voca close
	
		while(1)
		{	
			printf("\n\n\t\tDictionary : 0, Word : 1, Exit : 2\n");
			scanf("%d",&ch);
			switch(ch)
			{
				case 0:
					printf("\n\n\t\tDictionary\n" );
					printf("\n\t\[1] Search." );
					printf("\n\t\[2] Exit." );
					printf("\n\tSELECT : ");
					scanf("%d",&ch1);
					switch(ch1){
						case 1:
							printf("\n\tInput word : ");
							scanf("%s",word);
							if(search(word, dic)){
								if(insert(word, voca)){
									printf("Inserted in VOCA! : %s", word);
								}
							}else{
								printf("Not Found!\n");
							}
							break;
						case 2:
							break;
					}
					break;
				case 1:
					printf("\n\n\t\tWord\n" );
					printf("\n\t\[1] Search." );
					printf("\t[2] Insert.\n" );
					printf("\t[3] Delete." );
					printf("\t[4] Store in binary.\n" );
					printf("\t[5] Store in ASCII.\t[6] Exit\n" );
					scanf("%d",&ch1);
					switch(ch1){
						case 1:
							printf("\n\tInput word : ");
							scanf("%s",word);
							if(search(word, voca)){
								printf("Found! %s\n", word);
							}else{
								printf("Not Found %s in Voca\n", word);
							}
							break;
						case 2:
							printf("\n\tInput word : ");
							scanf("%s",word);
							if(insert(word, voca)){
								printf("Inserted in VOCA! : %s\n", word);
							}
							break;
						case 3:
							printf("\n\tInput word : ");
							scanf("%s",word);
							if(delete(word)){
								printf("Deleted in VOCA! : %s\n", word);
							}else{
								printf("Not Found %s in Voca\n", word);
							}
							break;
						case 4:
							if(store_bin())
							{
								printf("Saved in BINARY FILE NAME : voc.txt\n");
							}
							break;
						case 5:
							if(store_asc())
							{
								printf("Saved in ASCII FILE NAME : voc_asc.txt\n");
							}
							break;
						case 6:
							break;
					}
					break;
				case 2:
					return;
			}
	}
}

int store_bin()
{
	FILE *fdbw;
	fdbw=fopen("./voc.txt","wb");// writeable pointer
	struct node *temp;
	char ws = 32;
	int i=0;
	for(i=0;i<26;i++)
	{
		temp=voca[i];		
		while(temp->link!=NULL)
		{
			temp = temp->link; //toward next word
			fwrite(temp->data,strlen(temp->data),1,fdbw);
			fwrite(&ws,sizeof(ws),1,fdbw);//seperate words
		}
	}
	fclose(fdbw);//binary write close
	return 1;
}

int store_asc()
{
	FILE *fda;
	fda=fopen("./voc_asc.txt","w");// writeable pointer
	struct node *temp;
	
	int i=0;
	for(i=0;i<26;i++)
	{
		temp=voca[i];
		while(temp->link!=NULL)
		{	
			temp = temp->link;
			fprintf(fda,"%s\n",temp->data);
		}
	}
	fclose(fda);
	return 1;
}

int search(char *str, struct node **list)//search
{
	char temp1[20];
	char temp2[20];
	int i,j;
	j=toupper(str[0])-65;
	struct node *temp = list[j];

	
	strncpy(temp2,str,sizeof(str));
	strupr(temp2);
	while(temp->link!=NULL)
	{
		temp = temp->link;
		strncpy(temp1,temp->data,sizeof(temp->data));
		if(strcmp(strupr(temp1),temp2)==0)
		{
			return 1;
		}
	}
	return 0;
}

int insert(char *str,struct node **list)//insert
{
	int j;
	struct node *temp;
	j = toupper(str[0])-65;
	temp = list[j];
	if (j < 0 || 26 < j)
	{
		return 0;
	}
	if(search(str,list))
	{
		return 0;
	}	


	
	while(temp->link != NULL)
	{
		temp = temp -> link;
	}//to the last
	
	//(dic[j]->count)++;//through count++ ==> the other seperates
	temp->link = malloc(sizeof(struct node));//???
	temp = temp->link;//connecting
	strcpy(temp->data, str);
	temp->link = NULL;//connecting end.

	return 1;
}

int delete(char *str)//delete
{
	struct node *pre;
	struct node *temp;
	char temp1[20];
	char temp2[20];
	memset(temp1, 0, sizeof(temp1));
	memset(temp2, 0, sizeof(temp2));
	int i, j;
	j = toupper(str[0])-65;
	temp = voca[j];
	
	strcpy(temp2,str);
	strupr(temp2);
	while(temp->link!=NULL)
	{
		pre=temp;
		temp=temp->link;
		strcpy(temp1,temp->data);
		if(strcmp(strupr(temp1),temp2)==0)
		{
			pre->link=temp->link;
			temp->link=NULL;
			free(temp);
			return 1;
		}
	}
	return 0;
}
	
void removes( )//remove *****err still in at final closes
{
	struct node *n, *t;
	int i;

	for(i= 0;i<28;i++)
	{
		n = dic[i];
		if(n->link==NULL)
		{
			free(n);
		}
		else
		{
			n = n->link;
			while(n->link!=NULL)
			{
				t = n->link;
				free(n);
				n = t;
			}
		}
	}
	i=0;
	for(i= 0;i<27;i++)
	{
		n = voca[i];
		if(n->link==NULL)
		{
			free(n);
		}
		else
		{
			n = n->link;
			while(n->link!=NULL)
			{
				t = n->link;
				free(n);
				n = t;
			}
		}
	}
}

int token_save(char *out[], char *input)//token save
{
	int i=0;
	char *ptr = strtok(input," ");
	while(ptr!=NULL)
	{
		out[i]=ptr;
		ptr=strtok(NULL," ");
		i++;
	}
	return i;
}
int token_save_binary(char *out[], char *input)//token save
{
	int i=0;
	char *ptr = strtok(input,"\0");
	while(ptr!=NULL)
	{
		out[i]=ptr;
		ptr=strtok(NULL,"\0");
		i++;
	}
	return i;
}

void print(int i,int ch)//print
{
	if(i==0)
	{
		switch(ch)
		{
			case 1:
				printf("\tNot found");
				break;
			case 2:
				printf("\tAlready exist");
				break;
			case 3:
				printf("\tNot found");
				break;
		}
	}
	else if(i==1)
	{
		switch(ch)
		{
			case 1:
				printf("\tFound");
				break;
			case 2:
				printf("\tInserted");
				break;
			case 3:
				printf("\tDeleted");
				break;
		}
	}
}


