#include <stdio.h>
#include <stdlib.h>
#include <String.h>
#include <ctype.h>

#define size 27
#define MAX 1000

typedef struct Node {
	struct Node *prevNode;
	struct Node *nextNode;
	char data[256];
} node;

typedef struct LinkedList {
	node *header;
	node *trailer;
} List;

void init(List *list) {
	for(int i=0; i<size; i++) {
		(list+i)->header = (node *)malloc(sizeof(node));
		(list+i)->trailer = (node *)malloc(sizeof(node));
		(list+i)->header->prevNode = NULL;
		(list+i)->header->nextNode = (list+i)->trailer;
		(list+i)->trailer->prevNode = (list+i)->header;
		(list+i)->trailer->nextNode = NULL;
	}
}

void insert(List *list, char data[]) {
	node *newNode = (node *)malloc(sizeof(node));
	newNode->prevNode = list->trailer->prevNode;
	newNode->nextNode = list->trailer;
	strcpy(newNode->data, data);
	list->trailer->prevNode->nextNode = newNode;
	list->trailer->prevNode = newNode;
}

void menu() {
	printf("기능을 선택하세요\n");
	printf("[1] Search [2] Insert\n");
	printf("[3] Delete [4] Quit\n");
}

node* search(List *list, char input_data[]) {
	int index = input_data[0] - 97;
	node *tmp = (list+index)->header->nextNode;
	while(tmp != (list+index)->trailer) {
		if(strcmp(input_data, tmp->data) == 0) {
			printf("-> 단어장에 있습니다.\n");
			break;
		}
		tmp = tmp->nextNode;
	}
	return tmp;
}

void delete(List *list, char input_data[]) {
	node *tmp = search(list, input_data);
	tmp->nextNode->prevNode = tmp->prevNode;
	tmp->prevNode->nextNode = tmp->nextNode;
	printf("-> 삭제되었습니다.");
	free(tmp);
}

void loadfile(List *list) {
	char Line[1024];
	char word[256];
	char *tmp;
	char m_data[256];
	File *f;
	f = fopen("test.txt", "r");

	while(fgets(line, 1-24, f)) {
		tmp = strtok(line, " \n");
		while(tmp != NULL) {
		getf_word(tmp);
		insert(&list[getf_word(tmp)], tmp);
		printf("%s\n", tmp);
		tmp = strtok(NULL, " \n");
		}
		break;
	}
}

int main() {
	int s_num;
	char m_data[256];
	List *list = (List *)malloc(sizeof(List)*size);
	init(list);
	loadfile(list);
	while(1) {
		menu();
		scanf("%d", &s_num);
		if(s_num == 1) {
			printf("찾을 단어를 입력하세요 :");
			scanf("%s", &m_data);
			search(list, m_data);
		}
		else if(s_num == 2) {
			printf("입력할 단어를 입력하세요 :");
			scanf("%s", &m_data);
			insert(list, m_data);
		}
		else if(s_num == 3) {
			printf("삭제할 단어를 입력하세요 :");
			scanf("%s", &m_data);
			delete(list, m_data);
		}
		else {
			printf("종료되었습니다.\n");
			break;
		}
	}
}
