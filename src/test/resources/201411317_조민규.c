#pragma warning (disable:4996)
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<ctype.h>

typedef struct __node {
	char word[20];
	struct __node* next;
} node;

typedef node* nptr;

typedef struct _list{
	int count;
	nptr head;
}list;

void init(list* lptr);
void insert(list* lptr, char w[], int position1);
void insert_result(char w[]);
void delete(list* lptr, char w[], int position);
void delete_total(list* lptr, char w[], int position);
int search(list* lptr, char w[]);
int search_total(list* lptr, char w[]);

void print_list(list* lptr);

void init(list* lptr){
	lptr->count = 0;
	lptr->head = NULL;
}

void insert(list* lptr, char w[], int position1){
	nptr new_nptr = (node*)malloc(sizeof(node));
	strcpy(new_nptr->word, w);
	if (position1 == 0){
		new_nptr->next = lptr->head;
		lptr->head = new_nptr;
		lptr->count++;
	}	
	else
	{
		nptr tmp = lptr->head;
		int i;
		for (i = 0; i < position1 - 1; i++){
			tmp = tmp->next;
		}
		new_nptr->next = tmp->next;
		tmp->next = new_nptr;
		lptr->count++;
	}
}

void delete(list* lptr,char w[], int position)
{
	nptr tmp1 = lptr->head;
	char* str1 = tmp1->word;

	if (position == 0)
	{
		return;
	}
	else
		if (position == 1)	{
		if (strcmp(str1, w)==0)
		{
			lptr->head = tmp1->next;
			lptr->count--;
			printf("--->   삭제되었습니다\n");
		}
		
	}
	else
	{
		int i;
		for (i = 0; i < position-2; i++){
			tmp1 = tmp1->next;
		}
		str1 = tmp1->next->word;
		if (strcmp(str1, w) == 0)
		{
			nptr temp = tmp1->next;
			tmp1->next = temp->next;
			lptr->count--;free(temp);
			printf("--->  삭제되었습니다\n");
		}
	}
}



int search(list* lptr,char w[]){
	nptr tmp = lptr->head;
	int i = 0;
	while (tmp != NULL){
		if (strcmp(w,tmp->word)==0) break;
		i++;
		tmp = tmp->next;
	}
	if (i < lptr->count){
		return i+1;
	}

	printf("---> 단어%s가 [사전]에 존재 하지 않습니다! \n", w);
	return 0;
}

void delete_total(list* lptr, char w[], int position)
{
	nptr tmp1 = lptr->head;
	char* str1 = tmp1->word;

	if (position == 0)
	{
		return;
	}
	else if (position == 1)	{
		if (strcmp(str1, w) == 0)
		{
			lptr->head = tmp1->next;
			lptr->count--;
		}

	}
	else
	{
		int i;
		for (i = 0; i < position - 2; i++){
			tmp1 = tmp1->next;
		}
		str1 = tmp1->next->word;
		if (strcmp(str1, w) == 0)
		{
			nptr temp = tmp1->next;
			tmp1->next = temp->next;
			lptr->count--; free(temp);
		}

	}

}

int search_total (list* lptr, char w[]){
	nptr tmp = lptr->head;
	int i = 0;
	while (tmp != NULL){
		if (strcmp(w, tmp->word) == 0) break;
		i++;
		tmp = tmp->next;
	}
	if (i < lptr->count){
		return i + 1;
	}

	return 0;
}


void print_list(list* lptr){
	nptr tmp = lptr->head;
	if (tmp == NULL){ printf("[단어 목록]에 아무것도 존재하지 않습니다\n"); return; }
	else{ printf("[ %c의 배열]", tmp->word[0]); }
	while (tmp != NULL){
		printf(" -> ");
		printf("%s", tmp);
		tmp = tmp->next;
	}
	printf("\n");
}

void print_dic(list* lptr){
	nptr tmp = lptr->head;
	if (tmp == NULL){ printf("[사전 목록]에 아무것도 존재하지 않습니다\n"); return; }
	else{ printf("[Total 목록]", tmp->word[0]); }
	while (tmp != NULL){
		printf(" -> ");
		printf("%s", tmp);
		tmp = tmp->next;
	}
	printf("\n");
}

int main(void)
{
	char* token = NULL;
	char seps[] = ", \r\n\"=\t:.\"-";
	char line[400];
	int choice;
	char word[20];

	FILE* fp = fopen("sample.txt", "rt");
	if (fp == NULL){							   //파일이 없을경우 에러발생!
		printf("파일을 열 수 없습니다!\n");
		return -1;
	}

	FILE* voc = fopen("voc.txt", "rb");
	if (voc == NULL) { 
		puts("voc.txt 파일을열수없습니다."); 
		exit(1); 
	}

	list* a = (list*)malloc(sizeof(list)); init(a); list* b = (list*)malloc(sizeof(list)); init(b);
	list* c = (list*)malloc(sizeof(list)); init(c); list* d = (list*)malloc(sizeof(list)); init(d);
	list* e = (list*)malloc(sizeof(list)); init(e); list* f = (list*)malloc(sizeof(list)); init(f);
	list* g = (list*)malloc(sizeof(list)); init(g); list* h = (list*)malloc(sizeof(list)); init(h);
	list* i = (list*)malloc(sizeof(list)); init(i); list* j = (list*)malloc(sizeof(list)); init(j);
	list* k = (list*)malloc(sizeof(list)); init(k); list* l = (list*)malloc(sizeof(list)); init(l);
	list* m = (list*)malloc(sizeof(list)); init(m); list* n = (list*)malloc(sizeof(list)); init(n);
	list* o = (list*)malloc(sizeof(list)); init(o); list* p = (list*)malloc(sizeof(list)); init(p);
	list* q = (list*)malloc(sizeof(list)); init(q); list* r = (list*)malloc(sizeof(list)); init(r);
	list* s = (list*)malloc(sizeof(list)); init(s); list* t = (list*)malloc(sizeof(list)); init(t);
	list* u = (list*)malloc(sizeof(list)); init(u); list* v = (list*)malloc(sizeof(list)); init(v);
	list* w = (list*)malloc(sizeof(list)); init(w); list* x = (list*)malloc(sizeof(list)); init(x);
	list* y = (list*)malloc(sizeof(list)); init(y); list* z = (list*)malloc(sizeof(list)); init(z);
	list* dic = (list*)malloc(sizeof(list)); init(dic);
	list* total = (list*)malloc(sizeof(list)); init(total);
	
	int i_ = 0;
	char ch;

	while (!feof(voc)) {
		
		fread(&ch, sizeof(ch), 1, voc);
		char word[20];

		if (isalpha(ch))
		{
			word[i_] = ch;
			i_++;
		}
		else {
			word[i_] = '\0';
			i_ = 0;

			if (word[0] == 'a'){ insert(a, word, a->count); }		if (word[0] == 'b'){ insert(b, word, b->count); }
			if (word[0] == 'c'){ insert(c, word, c->count); }		if (word[0] == 'd'){ insert(d, word, d->count); }
			if (word[0] == 'e'){ insert(e, word, e->count); }		if (word[0] == 'f'){ insert(f, word, f->count); }
			if (word[0] == 'g'){ insert(g, word, g->count); }		if (word[0] == 'h'){ insert(h, word, h->count); }
			if (word[0] == 'i'){ insert(i, word, i->count); }		if (word[0] == 'j'){ insert(j, word, j->count); }
			if (word[0] == 'k'){ insert(k, word, k->count); }		if (word[0] == 'l'){ insert(l, word, l->count); }
			if (word[0] == 'm'){ insert(m, word, m->count); }		if (word[0] == 'n'){ insert(n, word, n->count); }
			if (word[0] == 'o'){ insert(o, word, o->count); }		if (word[0] == 'p'){ insert(p, word, p->count); }
			if (word[0] == 'q'){ insert(q, word, q->count); }		if (word[0] == 'r'){ insert(r, word, r->count); }
			if (word[0] == 's'){ insert(s, word, s->count); }		if (word[0] == 't'){ insert(t, word, t->count); }
			if (word[0] == 'u'){ insert(u, word, u->count); }		if (word[0] == 'v'){ insert(v, word, v->count); }
			if (word[0] == 'w'){ insert(w, word, w->count); }		if (word[0] == 'x'){ insert(x, word, x->count); }
			if (word[0] == 'y'){ insert(y, word, y->count); }		if (word[0] == 'z'){ insert(z, word, z->count); }
			insert(total, word, total->count);
		}
	}

	fclose(voc);

	while (fgets(line, 400, fp) != NULL){
		token = strtok(line, seps);
		while (token != NULL)
		{
			if (isupper(token[0])){token[0] = tolower(token[0]); }
			insert(dic, token, dic->count);
			token = strtok(NULL, seps);
		}
	}

	do{
		printf("■■■■■■■■■■■■■■■■■■■■■■■\n");
		printf("[1]Search Voca in Dict  \n");
		printf("[2]Insert Voca in List  \n");
		printf("[3]Delete Voca in List  \n"); 
		printf("[4]Show List of Voca    \n");
		printf("[5]Store List in File   \n");
		printf("[6]Quit\n");
		printf("■■■■■■■■■■■■■■■■■■■■■■■\n");
		printf("기능을 선택하세요: 선택---> ");
		scanf("%d", &choice);
		if (choice == 1)
		{
			printf("찾을 [단어]를 입력하세요:"); scanf("%s", word);
			int result = search(dic, word);
			if (result != 0)
			{
				printf("---> 단어%s가 [사전]에 존재합니다!\n", word);
				printf("---> 단어%s를 [단어 목록] 과 [단어장 파일]에 저장합니다!\n", word);
				if (word[0] == 'a'){ insert(a, word, a->count); }		if (word[0] == 'b'){ insert(b, word, b->count); }
				if (word[0] == 'c'){ insert(c, word, c->count); }		if (word[0] == 'd'){ insert(d, word, d->count); }
				if (word[0] == 'e'){ insert(e, word, e->count); }		if (word[0] == 'f'){ insert(f, word, f->count); }
				if (word[0] == 'g'){ insert(g, word, g->count); }		if (word[0] == 'h'){ insert(h, word, h->count); }
				if (word[0] == 'i'){ insert(i, word, i->count); }		if (word[0] == 'j'){ insert(j, word, j->count); }
				if (word[0] == 'k'){ insert(k, word, k->count); }		if (word[0] == 'l'){ insert(l, word, l->count); }
				if (word[0] == 'm'){ insert(m, word, m->count); }		if (word[0] == 'n'){ insert(n, word, n->count); }
				if (word[0] == 'o'){ insert(o, word, o->count); }		if (word[0] == 'p'){ insert(p, word, p->count); }
				if (word[0] == 'q'){ insert(q, word, q->count); }		if (word[0] == 'r'){ insert(r, word, r->count); }
				if (word[0] == 's'){ insert(s, word, s->count); }		if (word[0] == 't'){ insert(t, word, t->count); }
				if (word[0] == 'u'){ insert(u, word, u->count); }		if (word[0] == 'v'){ insert(v, word, v->count); }
				if (word[0] == 'w'){ insert(w, word, w->count); }		if (word[0] == 'x'){ insert(x, word, x->count); }
				if (word[0] == 'y'){ insert(y, word, y->count); }		if (word[0] == 'z'){ insert(z, word, z->count); }
				insert(total, word, total->count);
				voc = fopen("voc.txt", "wb");
				fwrite(word, strlen(word), 1, voc);
			}
		}

		if (choice == 2)
		{
			printf("[단어 목록]에 넣을 단어를 입력하세요:"); scanf("%s", word);
			if (word[0] == 'a'){ insert(a, word, a->count); }
			if (word[0] == 'b'){ insert(b, word, b->count); }
			if (word[0] == 'c'){ insert(c, word, c->count); }
			if (word[0] == 'd'){ insert(d, word, d->count); }
			if (word[0] == 'e'){ insert(e, word, e->count); }
			if (word[0] == 'f'){ insert(f, word, f->count); }
			if (word[0] == 'g'){ insert(g, word, g->count); }
			if (word[0] == 'h'){ insert(h, word, h->count); }
			if (word[0] == 'i'){ insert(i, word, i->count); }
			if (word[0] == 'j'){ insert(j, word, j->count); }
			if (word[0] == 'k'){ insert(k, word, k->count); }
			if (word[0] == 'l'){ insert(l, word, l->count); }
			if (word[0] == 'm'){ insert(m, word, m->count); }
			if (word[0] == 'n'){ insert(n, word, n->count); }
			if (word[0] == 'o'){ insert(o, word, o->count); }
			if (word[0] == 'p'){ insert(p, word, p->count); }
			if (word[0] == 'q'){ insert(q, word, q->count); }
			if (word[0] == 'r'){ insert(r, word, r->count); }
			if (word[0] == 's'){ insert(s, word, s->count); }
			if (word[0] == 't'){ insert(t, word, t->count); }
			if (word[0] == 'u'){ insert(u, word, u->count); }
			if (word[0] == 'v'){ insert(v, word, v->count); }
			if (word[0] == 'w'){ insert(w, word, w->count); }
			if (word[0] == 'x'){ insert(x, word, x->count); }
			if (word[0] == 'y'){ insert(y, word, y->count); }
			if (word[0] == 'z'){ insert(z, word, z->count); }
			insert(total, word, total->count);

			printf("-->  입력되었습니다\n");
		}
		if (choice == 3)
		{
			printf("[단어 목록]에서 삭제할 단어를 입력하세요:"); scanf("%s", word);
			if (word[0] == 'a'){ delete(a, word, search(a, word)); }
			if (word[0] == 'b'){ delete(b, word, search(b, word)); }
			if (word[0] == 'c'){ delete(c, word, search(c, word)); }
			if (word[0] == 'd'){ delete(d, word, search(d, word)); }
			if (word[0] == 'e'){ delete(e, word, search(e, word)); }
			if (word[0] == 'f'){ delete(f, word, search(f, word)); }
			if (word[0] == 'g'){ delete(g, word, search(g, word)); }
			if (word[0] == 'h'){ delete(h, word, search(h, word)); }
			if (word[0] == 'i'){ delete(i, word, search(i, word)); }
			if (word[0] == 'j'){ delete(j, word, search(j, word)); }
			if (word[0] == 'k'){ delete(k, word, search(k, word)); }
			if (word[0] == 'l'){ delete(l, word, search(l, word)); }
			if (word[0] == 'm'){ delete(m, word, search(m, word)); }
			if (word[0] == 'n'){ delete(n, word, search(n, word)); }
			if (word[0] == 'o'){ delete(o, word, search(o, word)); }
			if (word[0] == 'p'){ delete(p, word, search(p, word)); }
			if (word[0] == 'q'){ delete(q, word, search(q, word)); }
			if (word[0] == 'r'){ delete(r, word, search(r, word)); }
			if (word[0] == 's'){ delete(s, word, search(s, word)); }
			if (word[0] == 't'){ delete(t, word, search(t, word)); }
			if (word[0] == 'u'){ delete(u, word, search(u, word)); }
			if (word[0] == 'v'){ delete(v, word, search(v, word)); }
			if (word[0] == 'w'){ delete(w, word, search(w, word)); }
			if (word[0] == 'x'){ delete(x, word, search(x, word)); }
			if (word[0] == 'y'){ delete(y, word, search(y, word)); }
			if (word[0] == 'z'){ delete(z, word, search(z, word)); }
			delete_total(total, word, search_total(total,word));

		}
		if (choice == 4){
			printf("\n");
			printf("<  단어장의 리스트입니다  >\n");
			print_list(a); print_list(b); print_list(c); print_list(d);
			print_list(e); print_list(f); print_list(g); print_list(h);
			print_list(i); print_list(j); print_list(k); print_list(l);
			print_list(m); print_list(n); print_list(o); print_list(p);
			print_list(q); print_list(r); print_list(s); print_list(t);
			print_list(u); print_list(v); print_list(w); print_list(x);
			print_list(y); print_list(z); print_dic(total);
			printf("\n");
		}
		if (choice == 5){
			voc = fopen("voc.txt", "wb");
			nptr temp = total->head;
			printf("[단어 목록]을 [단어장 파일]에 저장합니다!!\n");
			char* temp_word;
				
			while (temp != NULL)
			{
					temp_word = malloc(sizeof(temp->word));
					strcpy(temp_word, temp->word);
					strcat(temp_word, " ");
					fwrite(temp_word, strlen(temp_word), 1, voc);
					temp = temp->next;
			}
			free(temp_word);
		}
	} while (choice != 6);
	
	free(a); free(b); free(c); free(d); free(e); free(f); free(g); free(h); free(i); free(j); free(k);
	free(l); free(m); free(n); free(o); free(p); free(q); free(r); free(s); free(t); free(u); free(v);
	free(w); free(x); free(y); free(z); free(dic); free(total); fclose(fp);	fclose(voc);

	return 0;
}
