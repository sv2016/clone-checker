#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct node{

	struct node *link;
	char word[40];

}Node;

typedef Node* nptr;

typedef struct list{
	int count;
	nptr head;
}List;

void init(List* lptr)
{
	lptr->count = 0;
	lptr->head=NULL;
}

void insert(List* lptr, char value[40])
{
	nptr new_nptr = (Node*)malloc(sizeof(Node));
	strcpy(new_nptr->word, value);
	if(lptr->head == NULL)
	{
		new_nptr->link = lptr->head;
		lptr->head = new_nptr;
	}
	else
	{
		nptr temp = lptr->head;
		while(temp->link != NULL)
		{
			temp = temp->link;		
		}
		new_nptr->link = temp->link;
		temp->link=new_nptr;
	}
	lptr->count++;
}

int search(List* lptr, char value[40])
{
	nptr temp = lptr->head;
	while(temp != NULL)
	{
		if(strcmp(value, temp->word) == 0)
		{
			return -1;
		}//있다.
		temp= temp->link;
	}
	return 0;//없다.
}

int delete(List *lptr, char value[40])
{
	if(search(lptr,value) == 0)
	{
		printf("삭제할 단어가 단어장에 존재하지 않습니다.\n");
		return -1;
	}
	else
	{
		if(lptr->count == 1)
		{
			lptr->head = NULL;
			lptr->count--;
		}
		else
		{
			nptr temp = lptr->head;
			nptr prev = NULL;
			while(temp!= NULL)
			{
				if(strcmp(value, temp->word) == 0)
				{		
					prev->link = temp->link;
					free(temp);
					lptr->count--;
					return 0;
				}
				prev = temp;
				temp = temp->link;
			}
		}
	}
}

void print_list(List* lptr)
{
	nptr temp = lptr->head;
	while(temp != NULL)
	{
		printf("%s ", temp->word);
		temp = temp->link;
	}
	printf("\n");
}

char* checkword(char* div_word)
{

	if( (isalpha(div_word[0]) || isdigit(div_word[0]) ) && (isalpha(div_word[strlen(div_word)-1]) || isdigit(div_word[strlen(div_word)-1]) ) )
	{

		return div_word;
	}
	else		
	{
		int j;
		if(!isalpha(div_word[0]) && !isdigit(div_word[0]))
		{
			if(strlen(div_word) == 2)
			{
				div_word[0] = div_word[1];
				div_word[1] = '\0';
			}
			else if(strlen(div_word) == 3)
			{
				div_word[0] = div_word[1];
				div_word[1] = div_word[2];
				div_word[2] = '\0';
			}
			else
			{
				for(j = 0; j<strlen(div_word)-1; j++)
				{
					div_word[j] = div_word[j+1];
				}
				div_word[j] = '\0';
			}
		}//첫 문자가 문장부호인 경우.
		if(!isalpha(div_word[strlen(div_word)-1]) && !isdigit(strlen(div_word)-1))
		{
			div_word[strlen(div_word)-1] = '\0';
		}//마지막 문자가 문장부호인 경우.

		return checkword(div_word);
	}
}

void clearBuffer()
{
	while(getchar() != '\n');
}

int main(void)
{
	int i = 0, j = 0, len=0;
	List* mylist[26];
	List* sublist[26];

	for(i=0; i<26; i++)
	{
		mylist[i] = (List*)malloc(sizeof(List));
		sublist[i] = (List*)malloc(sizeof(sublist));
	}
	
	char BUFFER[1024] = {0, };
	char *div_word = NULL;
	int key = 0;

	FILE *file = fopen("sample.txt", "r");

	if(file == NULL)
	{
		printf("file open failed!\n");

		return -1;
	}

	FILE *voc = fopen("voc.txt", "rb");

	if(voc == NULL)
	{
		printf("voc open failed!\n");

		return -1;
	}

	while(fgets(BUFFER, 1024, file) != NULL)
	{	
		len = strlen(BUFFER);
		if(len>1)
		{
			BUFFER[len-1] = '\0';
			BUFFER[len-2] = '\0';
			
		}//개행문자 ㅂ2
		if(strlen(BUFFER)>3)
		{
			for(i=0; i<strlen(BUFFER) -2 ; i++)
			{
				if(BUFFER[i] == '-' && BUFFER[i+1] == '-')
					BUFFER[i] = ' ';
			}
		}
		
		if(isdigit(BUFFER[0]))
		{	
			div_word = strtok(BUFFER, " ");
			div_word = strtok(NULL," ");//맨앞에 몇장 몇절은 단어가 아니랍니다.
		}
		else
			div_word = strtok(BUFFER," ");

		while(div_word != NULL)
		{
			div_word = checkword(div_word);
			int index = 0;
			if(isupper(div_word[0]))
			{
				index = div_word[0] - 65;
			}
			else if(islower(div_word[0]))
			{
				index = div_word[0] - 97;
			}
			else
			{
				div_word = NULL;
				div_word = strtok(NULL, " ");
				continue;
			}

			if(search(mylist[index], div_word) == -1)
			{
				
			}//해당 알파벳에 이미 존재.
			else if(search(mylist[index], div_word) == 0)
			{
				insert(mylist[index], div_word);
			}//알파벳에 없는 경우
			div_word = NULL;
			div_word = strtok(NULL, " ");
		}		
		strcpy(BUFFER,"\0");
	}
	fclose(file);

	div_word = NULL;
	strcpy(BUFFER, "\0");

	while(fgets(BUFFER, 1024, voc) != NULL)
	{	
		len = strlen(BUFFER);
		if(len>1)
		{
			BUFFER[len-1] = '\0';
			
		}//개행문자 ㅂ2
		
		if(BUFFER[0] == '[')
		{	
			div_word = strtok(BUFFER, " ");
			div_word = strtok(NULL," ");//맨앞에 단어 표시자는 제껴야지
		}
		else
			div_word = strtok(BUFFER," ");

		while(div_word != NULL)
		{
			div_word = checkword(div_word);
			int index = 0;
			if(isupper(div_word[0]))
			{
				index = div_word[0] - 65;
			}
			else if(islower(div_word[0]))
			{
				index = div_word[0] - 97;
			}
			else
			{
				div_word = NULL;
				div_word = strtok(NULL, " ");
				continue;
			}

			if(search(sublist[index], div_word) == -1)
			{
				
			}//해당 알파벳에 이미 존재.
			else if(search(sublist[index], div_word) == 0)
			{
				insert(sublist[index], div_word);
			}//알파벳에 없는 경우
			div_word = NULL;
			div_word = strtok(NULL, " ");
		}		
		strcpy(BUFFER,"\0");
	}
	fclose(voc);

	while(1)
	{
		printf("기능을 선택하세요 \n");
		printf("[1]Search [2]Insert\n[3]Delete [4]Store in ASCII\n[5]Store in binary [6]QUIT\n");
		scanf("%d", &key);
		if(key ==1)
		{
			int index = 0;
			char target_word[40] = {0, };
			printf("찾을 단어를 입력하세요\n");
			clearBuffer();
			scanf("%s", target_word);
			clearBuffer();
			
			if(isalpha(target_word[0]) && isupper(target_word[0]))
			{
				index = target_word[0] - 65;
			}
			else if(isalpha(target_word[0]) && islower(target_word[0]))
			{
				index = target_word[0] - 97;
			}
			else
			{
				printf("단어는 알파벳으로 시작해야 합니다.\n");
				continue;
			}

			if(search(mylist[index], target_word) == -1)
			{
				printf("단어 %s 는 사전에 존재합니다.\n", target_word);
				if(search(sublist[index], target_word) == 0)
				{
					insert(sublist[index], target_word);
					printf("단어 %s를 [%c]에 추가했습니다.\n", target_word, index+65);
				}//알파벳에 없으면 추가
				else
					printf("사전의 %s 는 이미 단어장에 존재합니다.\n", target_word);
			}
			else
				printf("단어 %s 는 사전에 존재하지 않습니다.\n", target_word);
			

		}

		else if(key ==2)
		{
			int index = 0;
			char target_word[40] = {0, };
			printf("추가할 단어를 입력하세요\n");
			clearBuffer();
			scanf("%s", target_word);
			clearBuffer();

			if(isalpha(target_word[0]) && isupper(target_word[0]))
			{
				index = target_word[0] - 65;
			}
			else if(isalpha(target_word[0]) && islower(target_word[0]))
			{
				index = target_word[0] - 97;
			}
			else
			{
				printf("단어는 영문자로 시작해야 합니다\n");
				continue;
			}
			if(search(sublist[index], target_word) == -1)
			{
				printf("단어 %s는 이미 단어장에 존재합니다.\n", target_word);
				continue;
			}

			else if(search(sublist[index], target_word) == 0)
			{
				insert(sublist[index], target_word);
				printf("단어 %s를 단어장 [%c]에 추가했습니다.\n", target_word, index+65);
			}//알파벳에 없으면 추가			

		}

		else if(key ==3)
		{
			char target_word[40] = {0, };
			int index = 0;
			printf("삭제할 단어를 입력하세요\n");
			clearBuffer();
			scanf("%s", target_word);
			clearBuffer();

			if(isalpha(target_word[0]) && isupper(target_word[0]))
			{
				index = target_word[0] - 65;
			}
			else if(isalpha(target_word[0]) && islower(target_word[0]))
			{
				index = target_word[0] - 97;
			}
			else
			{
				printf("단어는 영문자로 시작해야 합니다.\n");
				continue;
			}

			if(search(sublist[index], target_word) == 0)
			{
				printf("단어 %s는 단어장에 없습니다.\n", target_word);
				continue;
			}

			else if(search(sublist[index], target_word) == -1)
			{				
				delete(sublist[index], target_word);
				printf("단어 %s를 단어장에서 삭제했습니다.\n", target_word);
			}			
			
		}

		else if(key == 4)
		{
			FILE *voc2 = fopen("voc.txt", "w");
			for(i =0; i<26; i++)
			{
				nptr temp = sublist[i]->head;
				fprintf(voc2, "[%c] ", i+65);
				while(temp != NULL)
				{
					fprintf(voc2, " %s", temp->word);
					temp = temp->link;
				}
				fprintf(voc2, "%c", '\n');
			}
			fclose(voc2);
		}

		else if(key == 5)
		{
			FILE *voc2 = fopen("voc.txt", "wb");
			for(i =0; i<26; i++)
			{
				nptr temp = sublist[i]->head;
				fprintf(voc2, "[%c] ", i+65);
				while(temp != NULL)
				{
					fprintf(voc2, " %s", temp->word);
					temp = temp->link;
				}
				fprintf(voc2, "%c", '\n');
			}
			fclose(voc2);
		}

		else if(key == 6)
		{
			printf("bye!\n");
			break;
		}

		else if(key == 666)
		{
			for(i=0; i<26; i++)
			{
				printf("=======[%c]=======\n", i+65);
				print_list(mylist[i]);
				printf("\n");
			}
		}

		else if(key == 777)
		{
			for(i=0; i<26; i++)
			{
				printf("=======[%c]=======\n", i+65);
				print_list(sublist[i]);
				printf("\n");
			}
		}

		else
		{
			printf("Input again...wrong input\n");
			continue;
		}
			
				
		
	}//key 값 색인하여 메뉴 입장!

	return 0;
	
}
