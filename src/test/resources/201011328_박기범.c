#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <io.h>

typedef struct node
{
	char word[30];
	struct node* next;
} Node;

typedef struct linkedList
{
	Node* header; 
	Node* current; 
	Node* before; 
}LinkedList;

typedef LinkedList List;
void Init(List* nlist) //init
{
	nlist->header = (Node*) malloc(sizeof(Node));
	nlist->header->next = NULL; 
}


//	(plist->numOfData)++;


void Add(List* nlist, char* word) //add list
{

	Node* newNode = (Node*) malloc(sizeof(Node)); 
	Node* current = nlist->header; //first node is current node.
	strcpy(newNode->word, word); //copy.

	while(current->next != NULL) //til the end.
	{
		current = current->next; 
	}
	current->next = newNode; 
	newNode->next = NULL; // now new node is the last node.

}

int GetFirst(List* nlist, char* word) //get first data.
{
	if(nlist->header->next == NULL) //if is empty. exit.
		return 0;
	nlist->before = nlist->header; 
	nlist->current = nlist->header->next;
	strcpy(word, nlist->current->word);// word saved.
	return 1; //return 1  == complete.
}
int GetNext(List* nlist, char* word) //get next data. same as GetFirst
{

	if(nlist->current->next == NULL)
		return 0;
	nlist->before = nlist->current;
	nlist->current = nlist->current->next;	
	strcpy(word, nlist->current->word); 
	return 1;
}
void Delete(List* nlist)
{
	Node* rpos = nlist->current; 
	nlist->before->next = nlist->current->next; 
	nlist->current = nlist->before;  
	free(rpos); 
}
int WordDelete(List* nlist, char* word)
{
	char dword[30]; 
	if(GetFirst(nlist, dword))
	{
		if(strcmp(dword, word) == 0) //if it is exist
		{
			Delete(nlist);
			return 1;
		}
		while(GetNext(nlist, dword)) //get next data and do it again until the end.
		{
			if(strcmp(dword, word) == 0)
			{
				Delete(nlist);
				return 1;
			}
		}
	}
	return 0; //if fail return 0.
}
int Searchword(List* nlist, char* iword)//same as up there.
{
	char word[30];	
	if(GetFirst(nlist, word))
	{
		if(strcmp(word, iword) == 0)
		{
			return 1;
		}

		while(GetNext(nlist, word))
		{
			if(strcmp(word, iword) == 0)
			{
				return 1;
			}
		}
	}
	return 0;
}

void ReadFile(List* nlist) //read file
{
	int i;
	char buffer[256]; 
	char* result; //word after strtok
	char fword[256];
	int offset; //a to z offset
	FILE * fp = NULL;
	printf("File name?\n ");
	scanf("%s",fword);
	fp = fopen(fword, "r"); 
	if(fp == NULL)
	{
		printf("file open failed\n");
		exit(0);
	}
	while(!feof(fp))
	{
		fgets(buffer, 256, fp);
		result = strtok(buffer," \n");//cut by word.
		while(result != NULL) //until the end
		{
			int IsExist = 0; //is word exist in the vocab or not?
			if(!isalpha(result[0]))//is not alpha. skip.
			{
			//	printf("not alpha");
			}
			else
			{
				if(isupper(result[0]))
					offset = result[0] - 65;
				else
					offset = result[0] - 97;
				if(Searchword(nlist + offset, result))//if it is in the vocab a to z
					IsExist = 1; 		
				else
					IsExist = 0;	

				if(IsExist == 0) //if not exist
					Add(nlist + offset, result); 
			}
			result = strtok(NULL," \n"); //until the end.
		}		
	}
	fclose(fp);
}
/*
void ReadWriteFile(List* dlist)
{
	FILE * fp = NULL;
	char buffer[30] = {0};
	char a;
	char fword[30];
	int i;
	int k;
	int offset; //a to z offset
	printf("File name?\n ");
	scanf("%s",fword);
	fp = fopen(fword, "rb"); 
	if(fp == NULL)
	{
		printf("File open error\n");
		exit(0);
	}		
	while(1)
	{
		for(k=0; k< 30; k++)
		{
			i = fread(&a,sizeof(char),1,fp);
			if(i != 1)//when if fp cant get char, then break.
				break; 
			if(a != '\n')//if i is not a newline. store in the buffer.
				buffer[k] = a;
			else
				break;
		}
		
		if(isupper(buffer[0])) //same as up there. sorting
			offset = buffer[0] - 65;
		else	
			offset = buffer[0] - 97;

		Add((dlist+offset),buffer); // add.
		
		memset(buffer,0,30);//buffer flush
		
		if(i != 1) //if file is ended. finish.
			break;
	}	
}*/
void SearchWord(List* nlist)
{
	char inputWord[30];
	int i;
	int offset;
	int size;
	
	printf("Word : ");
	scanf("%s", inputWord);
	if(!isalpha(inputWord[0]))
	{	
		printf("error,it is not alpha.\n");
		return;
	}
	if(isupper(inputWord[0]))
		offset = inputWord[0] - 65;
	else


		offset = inputWord[0] - 97;
	
	if(Searchword(nlist + offset, inputWord))//a to z
	{
		printf(" Already Exist.\n");
		return;
	}
	else 
	{
		if(Searchword(nlist + 26, inputWord)) // others 
		{
			printf("->Already Exist.\n");
			return;
		}
		else 
		{
			printf("->Thers is no words in vocab.\n");
			return;
		}			
	}
}
void AddWord(List* nlist)
{
	char inputWord[30];
	int i;
	int offset;
	int size;

	printf(" Word that you want to add : ");
	scanf("%s", inputWord);
	size = strlen(inputWord);
	for(i = 0; i < size; i++)
	{
		if(!isalpha(inputWord[i]))
		{
			printf("Error is not Alpha.\n");
			return;
		}
	}

	if(isupper(inputWord[0]))
		offset = inputWord[0] - 65;
	else
		offset = inputWord[0] - 97;

	if(Searchword(nlist + offset, inputWord))
	{
		printf("->Already Exist.\n");
		return;
	}
	else
	{
		Add(nlist + offset, inputWord);
		printf("%s is added\n",inputWord);
	}
	
}
void DeleteWord(List* nlist)
{
		char inputWord[30];
		int offset;
		printf("Word that you want to erase:");

		scanf("%s",inputWord);
		if(!isalpha(inputWord[0]))
					return;
		if(isupper(inputWord[0]))
						offset = inputWord[0]-65;
		else
						offset = inputWord[0] -97;
			
		if(WordDelete(nlist + offset, inputWord)) //
		{
			printf("->Deleted.\n");
			return;
		}
		else
		{
			printf("->Not exist.\n");
			return;
		}			
	}
void Dictionary(List* nlist,List* words)
{
	char word[30];
	int offset;


	printf("Type the words that you want to search");
	scanf("%s", word);
	if(!isalpha(word[0]))
	{
		puts("wrong input");
		return;
	}


	if(isupper(word[0])) //same as up there. sorting
		offset = word[0] - 65;
	else	
		offset = word[0] - 97;
	
	if(Searchword(words + offset, word))//search from the vocab
	{
		printf("->Already Exist.\n");
		return;
	}
	else//
	{
		Add(words + offset, word);
		printf("%s is added\n",word);
	}
	

}

void SaveA(List* words)
{
	int i;
	FILE* fp = NULL;

	fp = fopen("voca.txt", "w");
	if(fp == NULL)
	{
		printf("error\n");
		exit(0);
	}

	for(i = 0; i <26; i++)
	{
			char word[30];

			if(GetFirst(words+i, word))//get the first word
			{
				if((words+i)->current->next == NULL) 
					fprintf(fp, "%s", word);
				else//if next is exist print ->
					fprintf(fp, "%s -> ", word);
			}
	
			while(GetNext(words+i, word)) //until the last word.
			{		
				if((words+i)->current->next == NULL)
					fprintf(fp, "%s", word);
				else
					fprintf(fp, "%s -> ", word); //same as up there.
			}

			fprintf(fp, "\n"); //when starting alphabet need to change, put enter
	}		
		

	printf("Saved as vocab.txt\n");

	fclose(fp);
}


void SaveB(List* words)
{
	int i;
	FILE* fp = NULL;

	fp = fopen("vocab.dat", "wb");
	if(fp == NULL)
	{
		puts("error");
		exit(0);
	}

	for(i=0; i<26; i++)
	{
		char word[30];

		if(GetFirst(words+i, word))
		{
			fwrite(word,strlen(word), 1, fp);
			fwrite("\n",1 ,1 , fp);
		}
	}
	puts("saved as vocab.dat\n");
	fclose(fp);
}

int main()
{
	int menu;
	int i;
	List nlist[26]; //vocab
	List dlist[26]; //dic


	for(i = 0; i < 26; i++)
	{
		Init(nlist+i); 
		Init(dlist+i);
	}
	printf("dicionary\n");
	ReadFile(dlist); 

	printf("\nvocab\n");
//	ReadWriteFile(nlist); //not working.
	
	printf("Menu\n");
	do
	{
		printf("[0] Open Dictionary\n");
		printf("[1] Search  [2] Insert\n");
		printf("[3] Delete  [4] Quit\n");
		printf("[5] Save as ASCII\n");
		printf("[6] Save as Binary\n");
		printf(":");
		scanf("%d", &menu);

		switch(menu)
		{
			case 0:
				Dictionary(dlist,nlist);
				break;
			case 1:
				SearchWord(nlist);				
				break;
			case 2: 
				AddWord(nlist);
				break;
			case 3:
				DeleteWord(nlist);
				break;
			case 4:
				break;
			case 5:
				SaveA(nlist);
				break;
			case 6:
				SaveB(nlist);
				break;
			default:
				printf("wrong number \n");
				break;
		}
		puts("");
	}while(menu != 4);

	return 0;
}
