#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

#define SIZE 26
#define SIZES 30

typedef struct NODE{
	char *data;
	struct NODE* prev;
	struct NODE* next;
}NODE;

typedef struct LIST{
	struct NODE* header;
	struct NODE* trailer;
}LIST;

void init(LIST* list);
void init_voca(LIST* list);
void addLast(NODE* trailer,char* element);
void print_list(NODE* plist);
int get_size(NODE* plist);
void print_menu();
void print_menu_store();
void search_list(LIST* list,LIST* arr,FILE* fp1,FILE* fp2);
void insert_list(LIST* list);
void deleted_list(LIST* list);
void search_voca(LIST* list);
void insert_voca(LIST* list,FILE* fp);
void deleted_voca(LIST* list);
void store_in_ascii(FILE* fp,char* str);
void store_in_binary(FILE* fp,char* str);


int main(void)
{
	int i = 0;
	int size = 400;
	int menu = 0;
	int *count = (int*)malloc(sizeof(int)*1);
	char *line = (char*)malloc(sizeof(char)*size);
	char *token = (char*)malloc(sizeof(char));
	char seps[] = " ,.?!()[]:;-=\"\'\t\n";

	LIST *word = (LIST*)malloc(sizeof(LIST)*SIZE);
	init(word);

	LIST *voca = (LIST*)malloc(sizeof(LIST)*1);
	init_voca(voca);

	FILE *fp = fopen("sample.txt","r");
	FILE *fp1 = fopen("voc.txt","a+");
	FILE *fp2 = fopen("voc.bin","wb+");

	while(fgets(line,size,fp))
	{
		token = strtok(line, seps);
		while(token!=NULL)
		{
			if(isalpha(token[0]))
			{
				if(isupper(token[0]))
					token[0] = tolower(token[0]);
				addLast((word+(token[0]-97))->trailer,token);
			}
			token = strtok(NULL,seps);
		}
	}
	fclose(fp);

	while(menu!=5)
	{
		print_menu();
		printf("메뉴를 입력하세요 : ");
		scanf("%d",&menu);
		switch(menu)
		{
			case 1:
				search_list(word,voca,fp1,fp2);
				break;
			case 2:
				search_voca(voca);
				break;
			case 3:
				insert_voca(voca,fp2);
				break;
			case 4:
				deleted_voca(voca);	
				break;
			case 5:
				printf("단어장을 종료합니다.");
				break;
			default :
				printf("해당하는 메뉴가 없습니다. 다시 입력하세요.\n");
				break;
		}
	}
	
	fclose(fp1);
	fclose(fp2);

	return 0;
}

void init(LIST* list)
{
	int i = 0;
	for(i=0;i<SIZE;i++)
	{
		(list+i)->header = (NODE*)malloc(sizeof(NODE)*1);
		(list+i)->trailer = (NODE*)malloc(sizeof(NODE)*1);
		(list+i)->header->prev = NULL;
		(list+i)->trailer->next = NULL;
		(list+i)->header->next = (list+i)->trailer;
		(list+i)->trailer->prev = (list+i)->header;
		(list+i)->header->data = (char*)malloc(sizeof(char)*2);
		(list+i)->trailer->data = (char*)malloc(sizeof(char)*2);
		(list+i)->header->data = "";
		(list+i)->trailer->data = "";
	}
}
void init_voca(LIST* list)
{
	list->header = (NODE*)malloc(sizeof(NODE)*1);
	list->trailer = (NODE*)malloc(sizeof(NODE)*1);
	list->header->prev = NULL;
	list->trailer->next = NULL;
	list->header->next = list->trailer;
	list->trailer->prev = list->header;
	list->header->data = (char*)malloc(sizeof(char)*2);
	list->trailer->data = (char*)malloc(sizeof(char)*2);
	list->header->data = "";
	list->trailer->data = "";
}
void addLast(NODE* trailer,char* element)
{
	NODE* temp = (NODE*)malloc(sizeof(NODE)*1);
	temp->data = (char*)malloc(sizeof(char)*30);
	temp->prev = trailer->prev;
	temp->next = trailer;
	strcpy(temp->data,element);
	trailer->prev->next = temp;
	trailer->prev = temp;
}
void print_list(NODE* plist)
{
	NODE* temp = (NODE*)malloc(sizeof(NODE)*1);

	temp = plist;
	while(temp)
	{
		printf("%s ",temp->data);
		temp = temp->next;
	}
	printf("\n");
}
int get_size(NODE* plist)
{
	NODE *p;
	int size = 0;

	p = plist;

	while(p)
	{
		size++;
		p=p->next;
	}
	return size-2;
}

void print_menu()
{
	printf("########################\n");
	printf("#         기능         #\n");
	printf("########################\n");
	printf("#  [1] Search - list   #\n");
	printf("#  [2] Search - voca   #\n");
	printf("#  [3] Insert - voca   #\n");
	printf("#  [4] Delete - voca   #\n");
	printf("#  [5] Quit            #\n");
	printf("########################\n");

}

void print_menu_store()
{
	printf("@@@@@@@@@@@@@@@@@@@@@@@@\n");
	printf("@         기능         @\n");
	printf("@@@@@@@@@@@@@@@@@@@@@@@@\n");
	printf("@  [1] Store in ASCII  @\n");
	printf("@  [2] Store in binary @\n");
	printf("@@@@@@@@@@@@@@@@@@@@@@@@\n");
}

void search_list(LIST* list,LIST* arr,FILE *fp1, FILE *fp2)
{
	int i = 0;
	int menu = 0;
	NODE* temp = (NODE*)malloc(sizeof(NODE)*1);
	char* input_1 = (char*)malloc(sizeof(char)*SIZES);
	printf("찾을 단어를 입력하세요 : ");
	scanf("%s",input_1);
	for(i=0;i<strlen(input_1);i++)
	{
		if(isupper(input_1[i]))
			input_1[i] = tolower(input_1[i]);
	}
	temp = (list+(input_1[0]-97))->header->next;
	while(temp)
	{
		if(strcmp(input_1,temp->data)==0)
		{
			printf("단어장에 있습니다.\n");
			print_menu_store();
			printf("메뉴를 입력하세요 : ");
			scanf("%d",&menu);
			switch(menu)
			{
				case 1:
					store_in_ascii(fp1,input_1);
					break;
				case 2:
					store_in_binary(fp2,input_1);
					addLast(arr->trailer,input_1);
					break;
			}
			break;
		}
		temp = temp->next;
	}
	if(temp == NULL)
		printf("단어장에 없습니다.\n");
		
}


void insert_list(LIST* list)
{
	char* input_2 = (char*)malloc(sizeof(char)*SIZES);
	printf("삽입할 단어를 입력하세요 : ");
	scanf("%s",input_2);
	if(get_size((list+(input_2[0]-97))->header)<5)
		addLast((list+(input_2[0]-97))->trailer,input_2);
	else
		addLast((list+26)->trailer,input_2);
}

void deleted_list(LIST* list)
{
	int count = 0;
	NODE* temp = (NODE*)malloc(sizeof(NODE)*1);
	char* input_3 = (char*)malloc(sizeof(char)*SIZES);
	printf("삭제할 단어를 입력하세요 : ");
	scanf("%s",input_3);
	temp = (list+(input_3[0]-97))->header->next;
	while(temp)
	{
		if(strcmp(input_3,temp->data)==0)
		{
			temp->prev->next = temp->next;
			temp->next->prev = temp->prev;
			temp->prev = NULL;
			temp->next = NULL;
			printf("삭제되었습니다.\n");
			break;
		}
		temp = temp->next;
	}
	if(temp == NULL)
		printf("단어장에 없습니다.\n");
}

void search_voca(LIST* list)
{
	int i = 0;
	NODE* temp = (NODE*)malloc(sizeof(NODE)*1);
	char* input_4 = (char*)malloc(sizeof(char)*SIZES);
	printf("찾을 단어를 입력하세요 : ");
	scanf("%s",input_4);
	for(i=0;i<strlen(input_4);i++)
	{
		if(isupper(input_4[i]))
			input_4[i] = tolower(input_4[i]);
	}
	temp = list->header->next;
	while(temp)
	{
		if(strcmp(input_4,temp->data)==0)
		{
			printf("단어장에 있습니다.\n");
			break;
		}
		temp = temp->next;
	}
	if(temp==NULL)
		printf("단어장에 없습니다.\n");
}

void insert_voca(LIST* list,FILE* fp)
{
	int i = 0;
	char* input_5 = (char*)malloc(sizeof(char)*SIZES);
	printf("삽입할 단어를 입력하세요 : ");
	scanf("%s",input_5);
	for(i=0;i<strlen(input_5);i++)
	{
		if(isupper(input_5[i]))
			input_5[i] = tolower(input_5[i]);
	}
	fwrite(input_5,sizeof(char),20,fp);
	addLast(list->trailer,input_5);
}

void deleted_voca(LIST* list)
{
	int i = 0;
	NODE* temp = (NODE*)malloc(sizeof(NODE)*1);
	char* input_6 = (char*)malloc(sizeof(char)*SIZES);
	printf("삭제할 단어를 입력하세요 : ");
	scanf("%s",input_6);
	for(i=0;i<strlen(input_6);i++)
	{
		if(isupper(input_6[i]))
			input_6[i] = tolower(input_6[i]);
	}
	temp = list->header->next;
	while(temp)
	{
		if(strcmp(input_6,temp->data)==0)
		{
			temp->prev->next = temp->next;
			temp->next->prev = temp->prev;
			temp->prev = NULL;
			temp->next = NULL;
			printf("삭제되었습니다.\n");
			break;
		}
		temp = temp->next;
	}
	if(temp==NULL)
		printf("단어장에 없습니다.\n");
}

void store_in_ascii(FILE* fp,char* str)
{
	fprintf(fp,"%s\r\n",str);
}

void store_in_binary(FILE* fp,char* str)
{
	fwrite(str,sizeof(char),(strlen(str)+1),fp);
	
}

