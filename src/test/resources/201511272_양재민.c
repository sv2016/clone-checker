#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct node
{
   char str[30];
   struct node *next;
   struct node *prev;
}Node;
typedef struct List
{
   Node *header;
   Node *trailer;
}List;

void initList();
void addLast(char word[], List *alphabet);
void openFile();
void input(char word[]);
void insert();
void delet();
void dicsearch();
void vocsearch();
void printmenu();
void ASCII();
void binary();

List alphabet[26];
List voc;
int main(void)
{
   int menu;
   initList();
   openFile();
   while(1)
   {
      printmenu();
      printf("입력: ");
      scanf("%d", &menu);
      switch(menu)
      {
      case 1:
         dicsearch();
         break;
      case 2:
         vocsearch();
         break;
      case 3:
         insert();
         break;
      case 4:
         delet(1);
         break;
      case 5:
         ASCII();
         break;
      case 6:
         binary();
         break;
      case 7:
         exit(1);
      }
   }
   return 0;
}
void initList()
{
   int i = 0; 
   for(i = 0; i < 26; i++)
   {
      alphabet[i].header = (Node*)malloc(sizeof(Node));
      alphabet[i].trailer = (Node*)malloc(sizeof(Node));
      alphabet[i].header -> next = alphabet[i].trailer;
      alphabet[i].trailer -> prev = alphabet[i].header;
      alphabet[i].trailer -> next = NULL;
      alphabet[i].header -> prev = NULL; 
   }
   voc.header = (Node*)malloc(sizeof(Node));
   voc.trailer = (Node*)malloc(sizeof(Node));
   voc.header -> next = voc.trailer;
   voc.trailer -> prev = voc.header;
   voc.trailer -> next = NULL;
   voc.header -> prev = NULL;
}
void addLast(char word[], List *node)
{
   Node *nodenew = (Node*)malloc(sizeof(Node));
   nodenew -> prev = node -> trailer -> prev;
   nodenew -> next = node -> trailer;
   strcpy(nodenew -> str, word);
   node -> trailer -> prev -> next = nodenew;
   node -> trailer -> prev = nodenew;
}
void printmenu()
{
   printf("기능을 선택하세요\n");
   printf("[1] 사전 search\t");
   printf("[2] 단어장 search\n");
   printf("[3] 단어장 insert\t");
   printf("[4] 단어장 delete\n");
   printf("[5] store in ASCII\t");
   printf("[6] store in binary\n");
   printf("[7] 종료\n");
}
void openFile()
{
   char word[30];
   int count;
   FILE *fp = fopen("Sample.txt", "r+");
   
   while(fscanf(fp, "%s", word) != EOF)
   {
      if(isalpha(word[0]))
      {
         count = 0;
         while(word[count] != '0')
         {
            count++;
         }
         count--;
         if(!isalpha(word[count]))
         {
            word[count] = '\0';
            count--;
            input(word);
         }
         else
         {
            input(word);
         }
      }
   }
   fclose(fp);
}
void input(char word[])
{
   int index;
   if(isupper(word[0]))
   {
      addLast(word, &alphabet[word[0] - 65]);
   }
   else
   {
      addLast(word, &alphabet[word[0] - 97]);
   }
}
void insert()
{
   char inser[30];
   printf("insert할 문자 입력: ");
   scanf("%s", inser);
   
   addLast(inser, &voc);
}
void delet()
{
   char dele[30];
   Node *temp;
   
   printf("delete할 문자 입력: ");
   scanf("%s", dele);
   
   dele[0] = toupper(dele[0]);
   temp = voc.header->next;
   while(temp != voc.trailer)
   {
      if(strcmp(temp->str, dele) == 0)
      {
         temp -> next -> prev = temp ->prev;
         temp -> prev -> next = temp -> next;
         temp = temp -> next;
      }
      else
      {
         temp = temp -> next;
      }
   }
   dele[0] = tolower(dele[0]);
   temp = voc.header -> next;
   while(temp != voc.trailer)
   {
      if(strcmp(temp -> str, dele) == 0)
      {
         temp -> next -> prev = temp -> prev;
         temp -> prev -> next = temp -> next;
         temp = temp -> next;
      }
      else
      {
         temp  = temp ->next;
      }
   }
}

void dicsearch()
{
   char sear[30];
   Node *temp;
   
   printf("찾을문자 입력: ");
   scanf("%s", sear);
   
   sear[0] = toupper(sear[0]);
   
   temp = alphabet[sear[0] - 65].header -> next;
   while(temp != alphabet[sear[0] - 65].trailer)
   {
      if(strcmp(temp -> str, sear) == 0)
      {
         printf("%s", temp -> str);
         addLast(sear, &voc);
         return;
      }
      else
      {
         temp = temp -> next;
      }
   }
   sear[0] = tolower(sear[0]);
   temp = alphabet[sear[0] -97].header-> next;
   while(temp != alphabet[sear[0] - 97].trailer)
   {
      if(strcmp(temp -> str, sear) == 0)
      {
         printf("%s", temp ->str);
         addLast(sear, &voc);
         return;
      }
      else
      {
         temp = temp -> next;
      }
   }
   printf("없습니다.\n");
   return;
}
void vocsearch()
{
   char sear[30];
   Node *temp;
   
   printf("찾을 문자 입력: ");
   scanf("%s", sear);

   sear[0] = toupper(sear[0]);
   temp = voc.header->next;
   while(temp != voc.trailer)
   {
      if(strcmp(temp -> str, sear)== 0)
      {
         printf("%s\n", sear);
         return;
      }
      else
      {
         temp = temp -> next;
      }
   }
   sear[0] = tolower(sear[0]);
   temp = voc.header -> next;
   while(temp != voc.trailer)
   {
      if(strcmp(temp -> str, sear) == 0)
      {
         printf("%s\n", sear);
         return;
      }
      else
      {
         temp = temp -> next;
      }
   }
   printf("없습니다.\n");
   return;
}
void ASCII()
{
   FILE *fp = fopen("voc.txt", "a+");
   Node *temp;
   
   temp = voc.header -> next; 

   while(temp != voc.trailer)
   {
      fprintf(fp, "%s", temp -> str);
      temp = temp -> next;
   }
   fclose(fp);
}
void binary()
{
   FILE *fp = fopen("voc.dat", "ab+");
   Node *temp;
   
   temp = voc.header->next;
   while(temp != voc.trailer)
   {
      fwrite(temp, sizeof(temp), 1, fp);
      temp = temp->next;
   }
   fclose(fp);
}
