import java.util.*;
//Change Center 할 때, Calculate 에서 numOfFile 의 수를 모른다. 
//따라서 CalFile()의 매개변수에 그 값을 넣어줄지 아니면 Controller에서 file 수 만큼 돌 것인지 정해야함.
//centerNum을 fileNum으로 볼 것인가 결정. 
public class Calculate 
{
	private float SyncRate[][];// = new float[3][6]; //배열의 크기는 얼만큼  정할 것인가 (Line, Func, Variable, Pre, Ann)
	private int centerNum = 0;
	private int fileNum =0;
	private int numOfFile =0;
	ArrayList <Files> files = new ArrayList();
	
	public Calculate(ArrayList<Files> files)
	{
		this.files = files;
	}
	public void calFile() 
	{
		this.numOfFile = files.size();
		SyncRate = new float[numOfFile][6];
		
		for(fileNum=0;fileNum<this.numOfFile;fileNum++)
		{
			calLine();
			calFunction();
			calVariable();
			calPreprocessor();
			calAnnotation();
			SyncRate[fileNum][5] = (SyncRate[fileNum][0]+SyncRate[fileNum][1]+SyncRate[fileNum][2]+SyncRate[fileNum][3]+SyncRate[fileNum][4])/5;

		}
		
	}
	public void calLine()
	{
		/*double LineDiff = 0;
		double LineDiff1 =0;
		
		LineDiff = files.get(centerNum).getNumOfLine() * 0.1;
		LineDiff1 = files.get(centerNum).getNumOfLine() * 0.3;
		
		if(files.get(centerNum).getNumOfLine() == files.get(fileNum).getNumOfLine())//일치
		{
			SyncRate[fileNum][0] = 100;
		}
		else if(files.get(centerNum).getNumOfLine()-LineDiff <= files.get(fileNum).getNumOfLine() && files.get(fileNum).getNumOfLine()<= files.get(centerNum).getNumOfLine()+LineDiff )
		{
			SyncRate[fileNum][0] = 40;
		}
		else if(files.get(centerNum).getNumOfLine()-LineDiff1 <= files.get(fileNum).getNumOfLine() && files.get(fileNum).getNumOfLine()<= files.get(centerNum).getNumOfLine()+LineDiff1 )
		{
			SyncRate[fileNum][0] = 30;
		}
		else
		{
			SyncRate[fileNum][0] = 10;
		}*/
		if(files.get(centerNum).getNumOfLine() == 0 || files.get(fileNum).getNumOfLine() == 0){
			SyncRate[fileNum][0] = 0;
		}
		else if(files.get(centerNum).getNumOfLine() >= files.get(fileNum).getNumOfLine()){
			SyncRate[fileNum][0] = (float)(files.get(fileNum).getNumOfLine() / (float)files.get(centerNum).getNumOfLine()) * 100;
		}
		else{
			SyncRate[fileNum][0] = (float)(files.get(centerNum).getNumOfLine() / (float)files.get(fileNum).getNumOfLine()) * 100;
		}
	}
	public void calFunction()
	{
		int i = 0;
		int j = 0;
		int numSame = 0;
		
		/* 함수 이름 비교 */
		if(files.get(centerNum).getListFunction().isEmpty()|| files.get(fileNum).getListFunction().isEmpty()) 
		{
			//center의 함수를 저장하는 List가 비어있다면
			SyncRate[fileNum][1] = 0;
		}
		else
		{
			//기준 파일과 비교 파일의 함수를 저장하는 List가 안비어있을 때
			while(i<files.get(centerNum).getListFunction().size())
			{	
				String str1 = new String(files.get(centerNum).getListFunction().get(i));
					
				for(j=0;j<files.get(fileNum).getListFunction().size();j++)
				{
					String str2 = new String(files.get(fileNum).getListFunction().get(j));
					if(str1.equals(str2))
					{
						numSame++;
						break;
					}
				}
				i++;
			}
			
			if(files.get(centerNum).getNumOfFunction() >= files.get(fileNum).getNumOfFunction()){
				SyncRate[fileNum][1] = (float) ( (( (float)numSame / (float)files.get(centerNum).getNumOfFunction() ) * 100 * 0.6) + ( (float)files.get(fileNum).getNumOfFunction() / (float)files.get(centerNum).getNumOfFunction() ) * 100 * 0.4 );
			}
			else{
				SyncRate[fileNum][1] = (float) ( (( (float)numSame / (float)files.get(fileNum).getNumOfFunction()) * 100 * 0.6) + ( (float)files.get(centerNum).getNumOfFunction() / (float)files.get(fileNum).getNumOfFunction() ) * 100 * 0.4 );
			}
		}
		/*함수 개수 비교 */
		/*double FuncDiff = 0;
		double FuncDiff1 =0;
		
		FuncDiff = files.get(centerNum).getNumOfFunction() * 0.1;
		FuncDiff1 = files.get(centerNum).getNumOfFunction() * 0.3;
		
		if(files.get(centerNum).getNumOfFunction() == files.get(fileNum).getNumOfFunction())//일치
		{
			SyncRate[fileNum][1] = 100;
		}
		else if(files.get(centerNum).getNumOfFunction()-FuncDiff <= files.get(fileNum).getNumOfFunction() && files.get(fileNum).getNumOfFunction()<= files.get(centerNum).getNumOfFunction()+FuncDiff )
		{
			SyncRate[fileNum][1] = 40;
		}
		else if(files.get(centerNum).getNumOfFunction()-FuncDiff1 <= files.get(fileNum).getNumOfFunction() && files.get(fileNum).getNumOfFunction()<= files.get(centerNum).getNumOfFunction()+FuncDiff1 )
		{
			SyncRate[fileNum][1] = 30;
		}
		else
		{
			SyncRate[fileNum][1] = 10;
		}*/
	}
	public void calVariable()
	{
		int i = 0;
		int j = 0;
		int numSame = 0;
		
		/* 변수 이름 비교 */
		if(files.get(centerNum).getListVariable().isEmpty()|| files.get(fileNum).getListVariable().isEmpty()) 
		{
			//center의 함수를 저장하는 List가 비어있다면
			SyncRate[fileNum][2] = 0;
		}
		else 
		{
			//center와 파일의 변수가 둘 다 있을 때 
			while(i<files.get(centerNum).getListVariable().size())
			{	
				String str1 = new String(files.get(centerNum).getListVariable().get(i));
					
				for(j=0;j<files.get(fileNum).getListVariable().size();j++)
				{
					String str2 = new String(files.get(fileNum).getListVariable().get(j));
					if(str1.equals(str2))
					{
						numSame++;
						break;
					}
				}
				i++;
			}
			/*
			if(numSame > files.get(centerNum).getListVariable().size()){
				numSame = files.get(centerNum).getListVariable().size();
			}
			else if(numSame > files.get(fileNum).getListVariable().size()){
				numSame = files.get(fileNum).getListVariable().size();
			}
			*/
			
			if(files.get(centerNum).getNumOfVariable() >= files.get(fileNum).getNumOfVariable()){
				SyncRate[fileNum][2] = (float) ((( (float)numSame / (float)files.get(centerNum).getNumOfVariable() ) * 100 * 0.6) + ( (float)files.get(fileNum).getNumOfVariable() / (float)files.get(centerNum).getNumOfVariable() ) * 100 * 0.4);
			}
			else{
				SyncRate[fileNum][2] = (float) ((( (float)numSame / (float)files.get(fileNum).getNumOfVariable()) * 100 * 0.6) + ( (float)files.get(centerNum).getNumOfVariable() / (float)files.get(fileNum).getNumOfVariable() ) * 100 * 0.4);
			}
		}
		
		/*변수 개수 비교 */
		/*double VariableDiff = 0;//
		double VariableDiff1 =0;
		
		VariableDiff = files.get(centerNum).getNumOfVariable() * 0.1;
		VariableDiff1 = files.get(centerNum).getNumOfVariable() * 0.3;
		
		if(files.get(centerNum).getNumOfVariable() == files.get(fileNum).getNumOfVariable())//일치
		{
			SyncRate[fileNum][2] = 100;
		}
		else if(files.get(centerNum).getNumOfVariable()-VariableDiff <= files.get(fileNum).getNumOfVariable() && files.get(fileNum).getNumOfVariable()<= files.get(centerNum).getNumOfVariable()+VariableDiff )
		{
			SyncRate[fileNum][2] = 40;
		}
		else if(files.get(centerNum).getNumOfVariable()-VariableDiff1 <= files.get(fileNum).getNumOfVariable() && files.get(fileNum).getNumOfVariable()<= files.get(centerNum).getNumOfVariable()+VariableDiff1 )
		{
			SyncRate[fileNum][2] = 30;
		}
		else
		{
			SyncRate[fileNum][2] = 10;
		}*/
	}
	public void calPreprocessor()
	{
		double PreDiff = 0;
		double PreDiff1 = 0;
		
		int i = 0;
		int j = 0;
		int numSame = 0;
		
		/* 전처리기 이름 비교 */
		if(files.get(centerNum).getListPreprocessor().isEmpty()|| files.get(fileNum).getListPreprocessor().isEmpty()) 
		{
			//center의 함수를 저장하는 List가 비어있다면
			SyncRate[fileNum][3] = 0;
		}
		else 
		{
			//center와 파일의 변수가 둘 다 있을 때 
			while(i<files.get(centerNum).getListPreprocessor().size())
			{	
				String str1 = new String(files.get(centerNum).getListPreprocessor().get(i));
					
				for(j=0;j<files.get(fileNum).getListPreprocessor().size();j++)
				{
					String str2 = new String(files.get(fileNum).getListPreprocessor().get(j));
					if(str1.equals(str2))
					{
						numSame++;
						break;
					}
				}
				i++;
			}
			
			if(files.get(centerNum).getNumOfPreprocessor() >= files.get(fileNum).getNumOfPreprocessor()){
				SyncRate[fileNum][3] = (float) ((( (float)numSame / (float)files.get(centerNum).getNumOfPreprocessor()) * 100 * 0.6) + ( (float)files.get(fileNum).getNumOfPreprocessor() / (float)files.get(centerNum).getNumOfPreprocessor() ) * 100 * 0.4);
			}
			else{
				SyncRate[fileNum][3] = (float) ((( (float)numSame / (float)files.get(fileNum).getNumOfPreprocessor()) * 100 * 0.6) + ( (float)files.get(centerNum).getNumOfPreprocessor() / (float)files.get(fileNum).getNumOfPreprocessor() ) * 100 * 0.4);
			}
		}
		
		/*PreDiff = files.get(centerNum).getNumOfPreprocessor() * 0.1;
		PreDiff1 = files.get(centerNum).getNumOfPreprocessor() * 0.3;
		
		if(files.get(centerNum).getNumOfPreprocessor() == files.get(fileNum).getNumOfPreprocessor())//일치
		{
			SyncRate[fileNum][3] = 100;
		}
		else if(files.get(centerNum).getNumOfPreprocessor()-PreDiff <= files.get(fileNum).getNumOfPreprocessor() && files.get(fileNum).getNumOfPreprocessor()<= files.get(centerNum).getNumOfPreprocessor()+PreDiff )
		{
			SyncRate[fileNum][3] = 40;
		}
		else if(files.get(centerNum).getNumOfPreprocessor()-PreDiff1 <= files.get(fileNum).getNumOfPreprocessor() && files.get(fileNum).getNumOfPreprocessor()<= files.get(centerNum).getNumOfPreprocessor()+PreDiff1 )
		{
			SyncRate[fileNum][3] = 30;
		}
		else
		{
			SyncRate[fileNum][3] = 10;
		}*/
	}
	public void calAnnotation()
	{
		/*double AnnoDiff = 0;
		double AnnoDiff1 = 0;
		
		AnnoDiff = files.get(centerNum).getNumOfPreprocessor() * 0.1;
		AnnoDiff1 = files.get(centerNum).getNumOfPreprocessor() * 0.3;
		
		if(files.get(centerNum).getNumOfAnnotation() == files.get(fileNum).getNumOfAnnotation())//일치
		{
			SyncRate[fileNum][4] = 100;
		}
		else if(files.get(centerNum).getNumOfAnnotation()-AnnoDiff <= files.get(fileNum).getNumOfAnnotation() && files.get(fileNum).getNumOfAnnotation()<= files.get(centerNum).getNumOfAnnotation()+AnnoDiff )
		{
			SyncRate[fileNum][4] = 40;
		}
		else if(files.get(centerNum).getNumOfAnnotation()-AnnoDiff1 <= files.get(fileNum).getNumOfAnnotation() && files.get(fileNum).getNumOfAnnotation()<= files.get(centerNum).getNumOfAnnotation()+AnnoDiff1 )
		{
			SyncRate[fileNum][4] = 30;
		}
		else
		{
			SyncRate[fileNum][4] = 10;
		}*/
		
		if(files.get(centerNum).getNumOfAnnotation() == 0 || files.get(fileNum).getNumOfAnnotation() == 0){
			SyncRate[fileNum][4] = 0;
		}
		else if(files.get(centerNum).getNumOfAnnotation() >= files.get(fileNum).getNumOfAnnotation()){
			SyncRate[fileNum][4] = ((float)files.get(fileNum).getNumOfAnnotation() / (float)files.get(centerNum).getNumOfAnnotation()) * 100;
		}
		else{
			SyncRate[fileNum][4] = ((float)files.get(centerNum).getNumOfAnnotation() / (float)files.get(fileNum).getNumOfAnnotation()) * 100;
		}
	}
	public void setNumCenter(int pointerNum)
	{
		this.centerNum = pointerNum;
	}
	public float getTotalSync(int pointerNum)//새로 생성했음
	{
		return SyncRate[pointerNum][5];
	}
	public float getLineSync(int pointerNum)
	{
		return SyncRate[pointerNum][0];
	}
	public float getFunctionSync(int pointerNum)
	{
		return SyncRate[pointerNum][1];
	}
	public float getVariableSync(int pointerNum)
	{
		return SyncRate[pointerNum][2];
	}
	public float getPreprocessorSync(int pointerNum)
	{
		return SyncRate[pointerNum][3];
	}
	public float getAnnotationSync(int pointerNum)
	{
		return SyncRate[pointerNum][4];
	}	
}
