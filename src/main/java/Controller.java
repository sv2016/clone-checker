import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Area;
import java.awt.geom.RoundRectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Exception;
import javax.swing.*;
import javax.swing.border.AbstractBorder;
import javax.swing.border.EmptyBorder;

public class Controller
{
	private String folderPath;
	private String fileName = null;
	private ArrayList<Files> files = new ArrayList();
	ArrayList<String> source = new ArrayList();
	private int pointerNum = 0;
	private int fileNum = 0;
	private int numOfFile = 0;
	
	//GUI-----------------------------------------------------------------------
	JFrame frame = new JFrame();
	JFileChooser chooser = new JFileChooser();
	Container ct;
	JPanel pnMain, pnResult, pnCloud;
	JLabel lbLogo, lbTip, lbLogoSmall, lbList, lbChooseFile, lbCenterCircle;
	JButton btFolder, btStart;
	JList ltFile;
	JScrollPane pnFileList;
	ArrayList<JLabel> lbCloud = new ArrayList();
	//--------------------------------------------------------------------------
	
	public Controller() {
		DisplayMain();
	}
	
	public void mkFileInstance() throws FileNotFoundException,IOException ,Exception 
	{	
		File dir = new File(folderPath);
		File [] fileList = dir.listFiles();

		for(File f : fileList)
		{
			if(!f.isHidden() && f.getName().endsWith(".c"))//.c 확장자가 아니면 count를 하지 않는다.
			{
				if(f.length() ==0 )
				{
					numOfFile = -1;//크기가 0byte인 소스 코드 파일
					break;
				}
				else
				{
					numOfFile++;
				}
			}
		}
		if(numOfFile == 0)// .c 확장자가 없다.
		{
			throw new Exception("1");
		}
		else if(numOfFile >100)//파일 수 100개 초과 
		{
			numOfFile = 0;
			throw new Exception("2");
		}
		else if(numOfFile == -1)// 0 byte
		{
			throw new Exception("3");
		}
		else
		{	
			while(fileNum < numOfFile)
			{
				for(File f : fileList)
				{
					if(!f.isHidden()&& f.getName().endsWith(".c"))
					{
						fileName = f.getName();
						Files file = new Files(fileName);
						files.add(file);
						fileNum++;
					}	
				}
			}
		}
			
		for(fileNum = 0; fileNum<numOfFile ;fileNum++)
		{
			fileName = fileList[fileNum].getName();
			
			BufferedReader br = new BufferedReader(new FileReader(folderPath+fileName));
			
			while(br.ready() !=false)
			{
				source.add(br.readLine());
			}
			br.close();
			
			Analyze analyze = new Analyze(source);
			analyze.analyzeFile(files.get(fileNum));
			
			System.out.println();
		}
	}
	
	
	public String getFolderPath() {
		return folderPath;
	}
	
	public void DisplayMain() {
		//창 속성
		frame.setLocation(200, 200); //창 위치
		frame.setSize(1150, 600); //창 크기
		frame.setTitle("Pleasant Clone Checker"); //창 이름
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //닫기 버튼 누를 경우 종료
		frame.setResizable(false); //창 크기 조절 불가
		
		//컨테이너 속성
		ct = frame.getContentPane();
		
		//패널 설정
		pnMain = new JPanel();
		pnMain.setBounds(0, 0, 1150, 600);
		pnMain.setLayout(null);
		pnMain.setBackground(new Color(255, 255, 255));
		
		//로고 출력
		lbLogo = new JLabel(new ImageIcon(getClass().getResource("logo.png")));
		lbLogo.setBounds(327, 107, 502, 133);
		pnMain.add(lbLogo);
		
		//팁 출력
		lbTip = new JLabel("<html>비교할 파일들이 들어있는 폴더를 지정하신 후, <b>시작하기</b> 버튼을 누르세요.<html>");
		lbTip.setBounds(0, 325, 1150, 30);
		lbTip.setHorizontalAlignment(JLabel.CENTER); //가운데 정렬
		lbTip.setFont(new Font("Malgun Gothic", Font.PLAIN, 12));
		lbTip.setForeground(new Color(89, 89, 89));
		pnMain.add(lbTip);
		
		//폴더 선택 버튼
		btFolder = new JButton("폴더 선택");
		btFolder.setBounds(334, 414, 482, 30);
		btFolder.setBackground(Color.white);
		btFolder.setBorder(null);
		btFolder.setFont(new Font("Malgun Gothic", Font.BOLD, 12));
		btFolder.setFocusable(false);
		ActionListener alFolder = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectFolder();
			}
		};
		btFolder.addActionListener(alFolder);
		pnMain.add(btFolder);
		
		//시작하기 버튼
		btStart = new JButton("시작하기");
		btStart.setBounds(515, 460, 120, 35);
		btStart.setFont(new Font("Malgun Gothic", Font.BOLD, 14));
		btStart.setBackground(new Color(230, 230, 230));
		btStart.setBorder(null);
		btStart.setFocusable(false);
		btStart.setVisible(false);
		ActionListener alStart = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					start();
					DisplayResult();
				}
				catch(Exception e1) {
					DisplayMain();
				}
				
			}
		};
		btStart.addActionListener(alStart);
		pnMain.add(btStart);
		
		ct.add(pnMain);
		
		ct.setVisible(true);
		frame.setVisible(true);
	}
	
	public void selectFolder() {
		if (showDialog() == JFileChooser.APPROVE_OPTION) { 
			showFolderPath();
		}
	}
	
	public int showDialog() {
		chooser.setCurrentDirectory(new java.io.File("."));
		chooser.setDialogTitle("폴더 선택");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setAcceptAllFileFilterUsed(false);
		
		return chooser.showOpenDialog(chooser);
	}
	
	public void showFolderPath() {
		btFolder.setText("" + chooser.getSelectedFile());
		btStart.setVisible(true);
	}
	
	public void DisplayResult() {
		//패널 설정
		pnResult = new JPanel();
		pnResult.setBounds(0, 0, 1150, 600);
		pnResult.setLayout(null);
		pnResult.setBackground(new Color(255, 255, 255));
		
		//로고 출력
		lbLogoSmall = new JLabel(new ImageIcon(new ImageIcon(getClass().getResource("logo.png")).getImage().getScaledInstance(265, 70, Image.SCALE_DEFAULT)));
		lbLogoSmall.setBounds(30, 30, 265, 70);
		pnResult.add(lbLogoSmall);
		
		//리스트 출력
		lbList = new JLabel("LIST");
		lbList.setBounds(1000, 0, 145, 50);
		lbList.setFont(new Font("Malgun Gothic", Font.BOLD, 18));
		lbList.setHorizontalAlignment(JLabel.CENTER); //가운데 정렬
		lbList.setBackground(new Color(247, 247, 247));
		lbList.setOpaque(true);
		pnResult.add(lbList);
		
		pnFileList = new JScrollPane();
		pnFileList.setBounds(1000, 50, 145, 521);
		pnFileList.setBorder(null);
		Vector fileList = new Vector();
		for(int i = 0; i < files.size(); i++) {
			String temp = files.get(i).getName();
			fileList.addElement("<html><center>" + temp.substring(0, 9) + "<br>" + temp.substring(10, temp.indexOf(".")) + "</center></html>");
		}
		
		UIManager.put("List.focusCellHighlightBorder", BorderFactory.createEmptyBorder());
		ltFile = new JList(); //JList 생성
		ltFile.setFixedCellWidth(98); //셀 너비 설정
		ltFile.setFixedCellHeight(40); //셀 높이 설정
		ltFile.setListData(fileList); //리스트 데이터 삽입
		ltFile.setBackground(new Color(247, 247, 247)); //배경색 설정
		ltFile.setSelectionBackground(new Color(225, 225, 225)); //선택된 셀 배경색 설정
		ltFile.setFont(new Font("Malgun Gothic", Font.PLAIN, 12)); //폰트 설정
		ltFile.setDragEnabled(true); //드래그 가능 여부 설정
		ltFile.resetKeyboardActions();
		ltFile.setBorder(new EmptyBorder(0, 15, 15, 15)); //간격 설정
		DefaultListCellRenderer renderer = (DefaultListCellRenderer)ltFile.getCellRenderer(); //셀 내용 가운데 정렬
		renderer.setHorizontalAlignment(JLabel.CENTER); //셀 내용 가운데 정렬
		MouseListener mlFile = new MouseAdapter() {
		    public void mouseClicked(MouseEvent e) {
		    	if (e.getClickCount() == 1) {
			    	if (SwingUtilities.isLeftMouseButton(e) == true) {
			    		changeCenter(ltFile.getSelectedIndex());
			    	}
		    	}
		    }
		};
		KeyListener klFile = new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_UP) {
					ltFile.setSelectedIndex(ltFile.getSelectedIndex() + 1);
				}
				else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
					ltFile.setSelectedIndex(ltFile.getSelectedIndex() - 1);
				}
			}
		};
		ltFile.addMouseListener(mlFile);
		ltFile.addKeyListener(klFile);
		pnFileList.setViewportView(ltFile); //뷰포트 설정
		pnResult.add(pnFileList);
		
		//파일 미선택시 화면
		lbChooseFile = new JLabel("<html>오른쪽 LIST에서 <b><font color=\"#92a8d1\">기준 소스 코드 파일<font></b>을 선택하세요.</html>");
		lbChooseFile.setBounds(0, 100, 1000, 470);
		lbChooseFile.setFont(new Font("Malgun Gothic", Font.PLAIN, 18));
		lbChooseFile.setHorizontalAlignment(JLabel.CENTER);
		pnResult.add(lbChooseFile);
		
		showCloud();
		
		ct.add(pnResult);
		pnResult.setVisible(true);
	}
	
	public void showCloud() {
		//파일 선택시 화면(태그 클라우드 출력)
		pnCloud = new JPanel();
		pnCloud.setBounds(0, 100, 1000, 470);
		pnCloud.setVisible(false);
		pnCloud.setBackground(Color.white);
		pnCloud.setOpaque(true);
		pnCloud.setLayout(null);
		pnResult.add(pnCloud);
		
		//기준 서클 이미지 출력
		lbCenterCircle = new JLabel("", new ImageIcon(getClass().getResource("centercircle.png")), JLabel.RIGHT);
		lbCenterCircle.setBounds(437, 172, 126, 126);
		lbCenterCircle.setFont(new Font("Malgun Gothic", Font.BOLD, 21));
		lbCenterCircle.setHorizontalTextPosition(JLabel.CENTER);
		lbCenterCircle.setForeground(new Color(125, 125, 125));
		pnCloud.add(lbCenterCircle);
		pnCloud.setComponentZOrder(lbCenterCircle, 0);
	}
	
	public void start() throws Exception {
		folderPath = chooser.getSelectedFile() + "\\";
		pnMain.setVisible(false);
		
		try {
			mkFileInstance();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1)
		{
			if(e1.getMessage() == "1") {
				JOptionPane.showMessageDialog(null, ".c 확장자 를 가진 파일이 없는 폴더입니다.", "오류", JOptionPane.ERROR_MESSAGE);
				throw new Exception();
			}
			else if(e1.getMessage() == "2") {
				JOptionPane.showMessageDialog(null, "파일이 100개 초과인 폴더입니다.", "오류", JOptionPane.ERROR_MESSAGE);
				throw new Exception();
			}
			else if(e1.getMessage() == "3") {
				JOptionPane.showMessageDialog(null, "0Byte 파일이 있는 폴더입니다.", "오류", JOptionPane.ERROR_MESSAGE);
				throw new Exception();
			}
			JOptionPane.showMessageDialog(null, "폴더 경로가 올바르지 않습니다.", "오류", JOptionPane.ERROR_MESSAGE);
			throw new Exception();
		}
		
	}
	
	public void changeCenter(int tempPointerNum) {
    	if(ltFile.isSelectionEmpty() == false && ltFile.getSelectedIndex() != pointerNum)
    	{
    		lbChooseFile.setVisible(false);
    		
    		pointerNum = tempPointerNum;
    		final Calculate cal = new Calculate(files);//Calcshowulate에 file 객체 넘기기 
    		cal.setNumCenter(pointerNum);//center를 정해주고 
    		cal.calFile();//계산 반복 
    		
    		lbCloud.clear();
    		pnCloud.removeAll();
    		String temp = "" + ltFile.getSelectedValue();
    		lbCenterCircle.setText(temp.substring(14, 23)); //기준 서클 내용 수정
    		pnCloud.add(lbCenterCircle);
    		pnCloud.setComponentZOrder(lbCenterCircle, 0);
    		pnCloud.setVisible(true);
    		
    		for(int i = 0; i < files.size(); i++) 
    		{
		    	lbCloud.add(new JLabel());
		    	final JLabel lbTempCloud = lbCloud.get(i);

	    		if(i != pointerNum && (int)cal.getTotalSync(i) > 52) { //기준 퍼센트 이상이고 기준이 아닐 경우
	    			temp = "" + files.get(i).getName();
		    		final JLabel lbTempName = new JLabel(temp.substring(0, 9));
		    		JLabel lbTempPercent = new JLabel((int)cal.getTotalSync(i) + "%");//일치율 전체
		    		
		    		lbTempName.setFont(new Font("Malgun Gothic", Font.BOLD, (int)Math.ceil((22 * (cal.getTotalSync(i) / 100)))));
		    		lbTempName.setForeground(new Color(146, 168, 209));
		    		Dimension sizeTempName = lbTempName.getPreferredSize();
		    		lbTempName.setBounds(0, 0, (int)sizeTempName.getWidth(), (int)sizeTempName.getHeight());
		    		
		    		lbTempPercent.setFont(new Font("Tahoma", Font.PLAIN, 13));
		    		lbTempPercent.setForeground(new Color(141, 141, 141));
		    		Dimension sizeTempPercent = lbTempPercent.getPreferredSize();
		    		lbTempPercent.setBounds(((int)sizeTempName.getWidth() / 2) - (((int)sizeTempPercent.getWidth() + 6) / 2), (int)sizeTempName.getHeight() + 4,
		    				(int)sizeTempPercent.getWidth() + 6, (int)sizeTempPercent.getHeight() - 1);
		    		lbTempPercent.setHorizontalAlignment(JLabel.CENTER);
		    		lbTempPercent.setBorder(new RoundedBorder(new Color(141, 141, 141), 6));
		    		
		    		lbTempCloud.add(lbTempName);
		    		lbTempCloud.add(lbTempPercent);
		    		
		    		boolean sw;
		    		do 
		    		{
		    			sw = true;
		    			lbTempCloud.setBounds(new Random().nextInt(800 - (int)sizeTempName.getWidth()) + 100, new Random().nextInt(350 - ((int)sizeTempName.getHeight() + 12)) + 70, 
		    					(int)sizeTempName.getWidth(), (int)sizeTempName.getHeight() + (int)sizeTempPercent.getHeight() + 3);
		    			
		    			if(intersects(lbTempCloud, lbCenterCircle) == true) sw = false;
		    			
		    			for(int j = 0; j < i; j++) 
		    			{
		    				JLabel lbAnotherCloud = lbCloud.get(j);
		    				if(intersects(lbTempCloud, lbAnotherCloud) == true) { //겹치면, false가 안 겹친다는 의미
		    					sw = false;
		    					break;
		    				}
		    			}
		    		}while(sw == false);
		    		
		    		MouseListener mlTempName = new MouseAdapter() {
		    			int pointerNum;
	    				JLabel lbTempSync = new JLabel();
	    				JLabel lbTempTotalSyncRate;
	    				JLabel lbTempSyncRate[] = new JLabel[5];
	    				
	    				public void mouseClicked(MouseEvent e) {
	    			    	if (SwingUtilities.isLeftMouseButton(e) == true) {
		    					for(int i = 0; i < files.size(); i++) {
		    			    		if(files.get(i).getName().substring(0, 9).equals(lbTempName.getText())) {
		    			    			pointerNum = i;
		    			    			break;
		    			    		}
		    			    	}
	
				    			ltFile.setSelectedIndex(pointerNum);
		    					changeCenter(pointerNum);
	    			    	}
	    				}
	    				
	    				public void mouseEntered(MouseEvent e) {
	    					for(int i = 0; i < files.size(); i++) {
	    			    		if(files.get(i).getName().substring(0, 9).equals(lbTempName.getText())) {
	    			    			pointerNum = i;
	    			    			break;
	    			    		}
	    			    	}
	    					
	    					lbTempSync = new JLabel("", new ImageIcon(getClass().getResource("sync.png")), JLabel.CENTER);
	    			    	lbTempSync.setBounds((int)lbTempCloud.getX() + (lbTempCloud.getWidth() / 2) - 142, (int)lbTempCloud.getY() - 55, 284, 56);
	    			    	
	    			    	displaySync(pointerNum, cal, lbTempSync, lbTempTotalSyncRate, lbTempSyncRate);
	    			    }
	    			    
	    			    public void mouseExited(MouseEvent e) {
	    			    	lbTempSync.removeAll();
	    			    	pnCloud.remove(lbTempSync);
	    			    	pnCloud.updateUI();
	    			    }
		    		};
		    		
		    		lbTempName.addMouseListener(mlTempName);
		    		
		    		pnCloud.add(lbTempCloud);
    			}
    		}
    		
    		pnCloud.updateUI();
    	}
	}
	
	public void displaySync(int pointerNum, Calculate cal, JLabel lbTempSync, JLabel lbTempTotalSyncRate, JLabel[] lbTempSyncRate) {
		lbTempTotalSyncRate = new JLabel((int)cal.getTotalSync(pointerNum) + "%");
    	lbTempTotalSyncRate.setBounds(10, 0, 63, 45);
    	lbTempTotalSyncRate.setForeground(new Color(115, 115, 115));
		lbTempTotalSyncRate.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lbTempTotalSyncRate.setHorizontalAlignment(JLabel.CENTER);
		lbTempSync.add(lbTempTotalSyncRate);
    	
		lbTempSyncRate[0] = new JLabel((int)cal.getLineSync(pointerNum) + "%");
		lbTempSyncRate[1] = new JLabel((int)cal.getFunctionSync(pointerNum) + "%");
		lbTempSyncRate[2] = new JLabel((int)cal.getVariableSync(pointerNum) + "%");
		lbTempSyncRate[3] = new JLabel((int)cal.getPreprocessorSync(pointerNum) + "%");
		lbTempSyncRate[4] = new JLabel((int)cal.getAnnotationSync(pointerNum) + "%");
		lbTempSyncRate[0].setBounds(117, 8, 40, 13);
		lbTempSyncRate[1].setBounds(181, 8, 40, 13);
		lbTempSyncRate[2].setBounds(244, 8, 40, 13);
		lbTempSyncRate[3].setBounds(123, 25, 40, 13);
		lbTempSyncRate[4].setBounds(187, 25, 40, 13);
		
    	for(int i = 0; i < 5; i++) {
    		lbTempSyncRate[i].setForeground(new Color(115, 115, 115));
    		lbTempSyncRate[i].setFont(new Font("Tahoma", Font.PLAIN, 13));
    		lbTempSync.add(lbTempSyncRate[i]);
    	}
		
		pnCloud.add(lbTempSync);
    	pnCloud.setComponentZOrder(lbTempSync, 0);
    	pnCloud.updateUI();
	}
	
	public boolean intersects(JLabel lbTempA, JLabel lbTempB){ //충돌처리
		JLabel lbTempC = new JLabel();
		lbTempC.setBounds(lbTempB.getX() - 10, lbTempB.getY() - 10, lbTempB.getWidth() + 20, lbTempB.getHeight() + 20);
		
	    Area areaA = new Area(lbTempA.getBounds());
	    Area areaB = new Area(lbTempC.getBounds());

	    return areaA.intersects(areaB.getBounds2D());
	}
	
	public class RoundedBorder extends AbstractBorder { //라벨을 둥근 모서리로
        private final Color color;
        private final int gap;

        public RoundedBorder(Color c, int g) {
            color = c;
            gap = g;
        }

        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2d = (Graphics2D) g.create();
            g2d.setColor(color);
            g2d.draw(new RoundRectangle2D.Double(x, y, width - 1, height - 1, gap, gap));
            g2d.dispose();
        }

        public Insets getBorderInsets(Component c) {
            return (getBorderInsets(c, new Insets(gap, gap, gap, gap)));
        }

        public Insets getBorderInsets(Component c, Insets insets) {
            insets.left = insets.top = insets.right = insets.bottom = gap / 2;
            return insets;
        }

        public boolean isBorderOpaque() {
            return false;
        }
    }
}
