import java.util.*;

public class Files 
{
	private String name;
	private int numOfLine;
	private int numOfFunction;
	private int numOfVariable;
	private int numOfPreprocessor;
	private int numOfAnnotation;
	private ArrayList<String> listFunction = new ArrayList<String>();
	private ArrayList<String> listVariable = new ArrayList<String>();
	private ArrayList<String> listPreprocessor = new ArrayList<String>();
	
	public Files(String fileName)
	{
		name = fileName;
		
	}
	public String getName()
	{
		return name;
	}
	public int getNumOfLine()
	{
		return numOfLine;
	}
	public int getNumOfFunction()
	{
		return numOfFunction;
	}
	public int getNumOfVariable()
	{
		return numOfVariable;
	}
	public int getNumOfPreprocessor()
	{
		return numOfPreprocessor;
	}
	public int getNumOfAnnotation()
	{
		return numOfAnnotation;
	}
	public ArrayList<String> getListFunction()
	{
		return listFunction;
	}
	public ArrayList<String> getListVariable()
	{
		return listVariable;
	}
	public ArrayList<String> getListPreprocessor()
	{
		return listPreprocessor;
	}
	
	public void setName(String tempName)
	{
		name = tempName;
	}
	public void setNumOfLine(int tempNumOfLine)
	{
		numOfLine = tempNumOfLine;
	}
	public void setNumOfFunction(int tempNumOfFunction)
	{
		numOfFunction = tempNumOfFunction;
	}
	public void setNumOfVariable(int tempNumOfVariable)
	{
		numOfVariable = tempNumOfVariable;
	}
	public void setNumOfPreprocessor(int tempNumOfPreprocessor)
	{
		numOfPreprocessor = tempNumOfPreprocessor;
	}
	public void setNumOfAnnotation(int tempNumOfAnnotation)
	{
		numOfAnnotation = tempNumOfAnnotation;
	}
	public void setListFunction(ArrayList<String> tempFunctionName)
	{
		listFunction = tempFunctionName;
	}
	public void setListVariable(ArrayList<String> tempVariableName)
	{
		listVariable = tempVariableName;
	}
	public void setListPreprocessor(ArrayList<String> tempPreprocessorName)
	{
		listPreprocessor = tempPreprocessorName;
	}
}
