import java.util.ArrayList;

public class Analyze {
	ArrayList<String> source = new ArrayList();
	String tempFolderPath;
	int numOfLine=0;
	int numOfFunction = 0;
	int numOfVariable = 0;
	int numOfPreprocessor = 0;
	int numOfAnnotation=0;
	ArrayList<String> listFunction = new ArrayList();
	ArrayList<String> listVariable = new ArrayList();
	ArrayList<String> listPreprocessor = new ArrayList();
	ArrayList<String> listType = new ArrayList();
	
	public Analyze(ArrayList<String> source){
		this.source = source;
		this.listType.add("void");
		this.listType.add("boolean");
		this.listType.add("char");
		this.listType.add("short");
		this.listType.add("int");
		this.listType.add("long");
		this.listType.add("double");
		this.listType.add("float");
	}
	
	public void analyzeFile(Files file){
		this.changeAnnotation();
		this.analyzeAnnotation();
		this.analyzeLine();
		this.deleteAnnotation();
		this.divideCodeLine();
		this.deletePrintf();
		this.findType();
		this.analyzeFunction();
		this.analyzeVariable();
		this.analyzePreprocessor();
		
		file.setNumOfAnnotation(numOfAnnotation);
		file.setNumOfLine(numOfLine);
		file.setNumOfFunction(numOfFunction);
		file.setListFunction(listFunction);
		file.setNumOfVariable(numOfVariable);
		file.setListVariable(listVariable);
		file.setNumOfPreprocessor(numOfPreprocessor);
		file.setListPreprocessor(listPreprocessor);
		source.clear();
	}
	
	void analyzeLine(){
		numOfLine = source.size();
	}
	
	void analyzeFunction(){
		int i,j;
		
		for(i=0 ; i<source.size() ; i++){
			for(j=0 ; j<listType.size(); j++){
				if(source.get(i).contains(listType.get(j))){
					if(source.get(i).contains("(") && source.get(i).indexOf("(") > source.get(i).indexOf(listType.get(j))){
						if(!source.get(i).contains(";")){
							numOfFunction++;
							listFunction.add(source.get(i).substring(source.get(i).indexOf(listType.get(j)), source.get(i).indexOf(")")+1));
						}
					}
				}
			}
		}
	}
	
	void analyzeVariable(){
		int i,j;
		int flag;
		String tempString;
		
		
		for(i=0 ; i<source.size() ; i++){
			tempString = source.get(i).trim();
			for(j=1 ; j<listType.size(); j++){
				if(tempString.startsWith(listType.get(j)) && !(tempString.contains("(")) && !tempString.contains("printf")){
					do{
						if(tempString.contains(",")){
							listVariable.add( (tempString.substring(0, tempString.indexOf(","))).trim() );
							tempString = tempString.substring(tempString.indexOf(",")+1);
							numOfVariable++;
						}
						else{
							listVariable.add(tempString.trim());
							numOfVariable++;
							break;
						}
					} while(true);
				}
			}
		}
			
		for(i=0 ; i<listVariable.size(); i++){	//다듬기
			if(listVariable.get(i).contains("=")){	// = 지우기
				listVariable.add(i,listVariable.get(i).substring(0, listVariable.get(i).indexOf("=")));
				listVariable.remove(i+1);
			}
			if(listVariable.get(i).contains(";")){	// ; 지우기
				listVariable.add(i,listVariable.get(i).substring(0, listVariable.get(i).indexOf(";")));
				listVariable.remove(i+1);
			}
			
			flag = 0;
			for(j=0 ; j<listType.size(); j++){
				if(!(listVariable.get(i).contains(listType.get(j)))){
					flag++;
				}
			}
			if(flag == listType.size()){
				listVariable.add(i, listVariable.get(i-1).substring(0, listVariable.get(i-1).indexOf(" ")+1) + listVariable.get(i) );
				listVariable.remove(i+1);
			}

		}
	}
	
	void analyzePreprocessor(){
		int i;
		
		for(i=0 ; i<source.size() ; i++){ 
			if(source.get(i).trim().startsWith("#")){	//#로 시작하는 경우
				numOfPreprocessor++;
				listPreprocessor.add(source.get(i).trim());
			}
		}
	}
	
	void analyzeAnnotation(){
		int i;
		
		for(i=0 ; i<source.size() ; i++){
			if(source.get(i).contains("//") || source.get(i).contains("*/")){
				numOfAnnotation++;
			}
		}
	}
	
	void changeAnnotation(){	// /**/을 //로 바꿔주는 함수
		int i;
		int start = 0;	// 주석이 시작되는 줄
		int end = -1;	// 주석이 끝나는 줄
		boolean isExist;
		
		do{
			isExist = false;
			for(i=end+1 ; i<source.size() ; i++){
				if(source.get(i).contains("/*")){
					start = i;
					isExist = true;
					break;
				}
			}
			for(i=start ; i<source.size() ; i++){
				if(source.get(i).contains("*/")){
					end = i;
					break;
				}
			}
			if(isExist){	
				source.add(start, source.get(start).replace("/*", "//"));//주석이 시작하는 /*을 //로 바꿈
				source.remove(start+1);
				for(i=start+1 ; i<end ; i++){
					source.add(i, "//" + source.get(i));	//중간 코드들 맨 앞 // 추가
					source.remove(i+1);
				}
			}
			
		} while(isExist);		
	}
	
	void deleteAnnotation(){
		int i;
		
		for(i=0 ; i<source.size() ; i++){
			if(source.get(i).contains("//") && !source.get(i).contains("*/")){
				source.add(i, source.get(i).substring(0, source.get(i).indexOf("//")).trim());
				source.remove(i+1);
			}
			else if(source.get(i).contains("//") && source.get(i).contains("*/")){
				source.add(i, source.get(i).substring(0, source.get(i).indexOf("//"))+source.get(i).substring(source.get(i).indexOf("*/")+2).trim());
				source.remove(i+1);
			}
			else if(source.get(i).contains("*/")){
				source.add(i, source.get(i).substring(source.get(i).indexOf("*/")+2).trim());
				source.remove(i+1);
			}
		}
		for(i=source.size()-1 ; i>0; i--){
			if(source.get(i).length() == 0){
				source.remove(i);
			}
		}
	}
	
	void divideCodeLine(){
		int i=0;
		int count;
		String tempString1, tempString2;
		
		do{
			count = 0;
			tempString1 = source.get(i);
			
			if(tempString1.contains(";") && !tempString1.contains("for"))
			{
				if(tempString1.contains("\""))
				{
					break;
				}
				else
				{
					do
					{
						tempString2 = tempString1.substring(0, tempString1.indexOf(";")+1);
						source.add(i+1+count, tempString2);
						count++;
						tempString1 = tempString1.substring(tempString1.indexOf(";")+1).trim();
						
						if(tempString1.length() == 1)
						{
							break;
						}
						
					}while(tempString1.substring(tempString1.indexOf(";")+1).length()>1);
					if(!tempString1.isEmpty()){
						source.add(i+1+count, tempString1);
					}
					source.remove(i);
				}
			}
			
			i++;
		}while(i<source.size());
	}
	
	void deletePrintf(){
		int i;
		
		for(i=0 ; i<source.size() ; i++){
			if(source.get(i).contains("printf")){
				source.add( i, source.get(i).substring(0, source.get(i).indexOf("(")+1) + source.get(i).substring(source.get(i).lastIndexOf(")")));
				source.remove(i+1);
			}
		}
	}
	
	void findType(){
		int i,j;
		
		for(i=0 ; i<source.size() ; i++){
			if(source.get(i).contains("typedef") && source.get(i).contains("struct") && !source.get(i).contains(";")){	//typedef struct 추가
				j=0;
				while(true){
					if(source.get(i+j).contains("}"))
						break;
					j++;
				}
				listType.add(source.get(i+j).substring(source.get(i+j).indexOf("}")+1, source.get(i+j).indexOf(";")).trim());
			}
			else if(source.get(i).contains("typedef") && !source.get(i).contains("struct") && !source.get(i).contains(";")){	//typedef 변수 추가
				for(j=0 ; j<listType.size(); j++){
					if(source.get(i).contains(listType.get(j))){
						listType.add(source.get(i).substring( source.get(j).indexOf(listType.get(j))+listType.get(j).length(), source.get(i).indexOf(";") ).trim());
					}
				}
			}
			else if(!source.get(i).contains("typedef") && source.get(i).trim().startsWith("struct") && !source.get(i).contains(";")){	//struct 추가
				if(source.get(i).contains("{")){
					listType.add(source.get(i).substring(source.get(i).indexOf("struct"), source.get(i).indexOf("{")).trim());
				}
				else{
					listType.add(source.get(i).substring(source.get(i).indexOf("struct")).trim());
				}
			}
		}
	}
}
